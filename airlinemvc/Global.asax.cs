﻿using airlinemvc.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace airlinemvc
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            Database.SetInitializer(
                new MigrateDatabaseToLatestVersion<Models.AirlineMVCContext, Migrations.Configuration>());
            ApplicationDbContext db = new ApplicationDbContext();
            CreateRoles(db);
            CreateSuperUser(db);
            AddPermissionsToSuperUser(db);
            db.Dispose();
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        private void AddPermissionsToSuperUser(ApplicationDbContext db)
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));

            var user = userManager.FindByName("antonio.franco.frazao@formandos.cinel.pt");

            if (!userManager.IsInRole(user.Id, "View"))
            {
                userManager.AddToRole(user.Id, "View");
            }

            if (!userManager.IsInRole(user.Id, "Create"))
            {
                userManager.AddToRole(user.Id, "Create");
            }

            if (!userManager.IsInRole(user.Id, "Edit"))
            {
                userManager.AddToRole(user.Id, "Edit");
            }

            if (!userManager.IsInRole(user.Id, "Delete"))
            {
                userManager.AddToRole(user.Id, "Delete");
            }
        }

        private void CreateSuperUser(ApplicationDbContext db)
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));

            var user = userManager.FindByName("antonio.franco.frazao@formandos.cinel.pt");

            if (user == null)
            {
                user = new ApplicationUser
                {
                    UserName = "antonio.franco.frazao@formandos.cinel.pt",
                    Email = "antonio.franco.frazao@formandos.cinel.pt"
                };

                userManager.Create(user, "Manolo21!");
            }
        }

        private void CreateRoles(ApplicationDbContext db)
        {
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));

            /*Para apagar um role mal colocado:
             var role = roleManager.FindByName("Creat");
             roleManager.Delete(role);
             */

            if (!roleManager.RoleExists("View"))
            {
                roleManager.Create(new IdentityRole("View"));
            }


            if (!roleManager.RoleExists("Edit"))
            {
                roleManager.Create(new IdentityRole("Edit"));
            }


            if (!roleManager.RoleExists("Create"))
            {
                roleManager.Create(new IdentityRole("Create"));
            }


            if (!roleManager.RoleExists("Delete"))
            {
                roleManager.Create(new IdentityRole("Delete"));
            }

            if (!roleManager.RoleExists("EmployeeView"))
            {
                roleManager.Create(new IdentityRole("EmployeeView"));
            }


            if (!roleManager.RoleExists("EmployeeEdit"))
            {
                roleManager.Create(new IdentityRole("EmployeeEdit"));
            }


            if (!roleManager.RoleExists("EmployeeCreate"))
            {
                roleManager.Create(new IdentityRole("EmployeeCreate"));
            }


            if (!roleManager.RoleExists("EmployeeDelete"))
            {
                roleManager.Create(new IdentityRole("EmployeeDelete"));
            }

            if (!roleManager.RoleExists("ClientView"))
            {
                roleManager.Create(new IdentityRole("ClientView"));
            }


            if (!roleManager.RoleExists("ClientEdit"))
            {
                roleManager.Create(new IdentityRole("ClientEdit"));
            }


            if (!roleManager.RoleExists("ClientCreate"))
            {
                roleManager.Create(new IdentityRole("ClientCreate"));
            }
        }
    }
}
