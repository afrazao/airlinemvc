﻿using airlinemvc.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace airlinemvc.Services
{
    public static class DataService
    {
        public static async Task<Response> GetAPIData(string UrlBase, string controller)
        {
            try
            {
                var client = new HttpClient();

                client.BaseAddress = new Uri(UrlBase);

                var answer = await client.GetAsync(controller);

                var outcome = await answer.Content.ReadAsStringAsync();

                //if (!NetworkService.CheckConnection().IsSuccess)
                //{
                //    return new Response
                //    {
                //        IsSuccess = false,
                //        Message = outcome
                //    };
                //}

                var countries = JsonConvert.DeserializeObject<List<APIModel>>(outcome);

                return new Response
                {
                    IsSuccess = true,
                    Result = countries
                };
            }
            catch (Exception ex)
            {
                return new Response
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }

        public static async Task<Response> GetAPICitiesData(string UrlBase, string controller)
        {
            try
            {
                var client = new HttpClient();

                client.BaseAddress = new Uri(UrlBase);

                var answer = await client.GetAsync(controller);

                var outcome = await answer.Content.ReadAsStringAsync();

                //if (!NetworkService.CheckConnection().IsSuccess)
                //{
                //    return new Response
                //    {
                //        IsSuccess = false,
                //        Message = outcome
                //    };
                //}

                var cities = JsonConvert.DeserializeObject<List<APICityModel>>(outcome);

                return new Response
                {
                    IsSuccess = true,
                    Result = cities
                };
            }
            catch (Exception ex)
            {
                return new Response
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }

    }
}