﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(airlinemvc.Startup))]
namespace airlinemvc
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
