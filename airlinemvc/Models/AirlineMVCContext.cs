﻿using airlinemvc.Services;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;

namespace airlinemvc.Models
{
    public class AirlineMVCContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx
    
        public AirlineMVCContext() : base("name=AirlineMVCContext")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }

        public System.Data.Entity.DbSet<airlinemvc.Models.Airplane> Airplanes { get; set; }

        public System.Data.Entity.DbSet<airlinemvc.Models.AirplaneRange> AirplaneRanges { get; set; }

        public System.Data.Entity.DbSet<airlinemvc.Models.Country> Countries { get; set; }

        public System.Data.Entity.DbSet<airlinemvc.Models.City> Cities { get; set; }

        public System.Data.Entity.DbSet<airlinemvc.Models.Flight> Flights { get; set; }

        public System.Data.Entity.DbSet<airlinemvc.Models.Client> Clients { get; set; }

        public System.Data.Entity.DbSet<airlinemvc.Models.Ticket> Tickets { get; set; }

        public System.Data.Entity.DbSet<airlinemvc.Models.TicketType> TicketTypes { get; set; }

        public System.Data.Entity.DbSet<airlinemvc.Models.TicketPrice> TicketPrices { get; set; }

        public System.Data.Entity.DbSet<airlinemvc.Models.TicketClass> TicketClasses { get; set; }

        public System.Data.Entity.DbSet<airlinemvc.Models.Employee> Employees { get; set; }
    }
}
