﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace airlinemvc.Models
{
    public class Employee
    {
        [Key]
        public int EmployeeID { get; set; }

        [Required(ErrorMessage = "You have to insert a First Name.")]
        [StringLength(50, ErrorMessage = "The {0} has to be {1} characters long.")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "You have to insert a Last Name.")]
        [StringLength(50, ErrorMessage = "The {0} has to be {1} characters long.")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "You have to have an email associated.")]
        public string Email { get; set; }
    }
}