﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace airlinemvc.Models
{
    public class AirplaneRange
    {
        [Key]
        public int RangeID { get; set; }

        [Display(Name = "Range")]
        [Required(ErrorMessage = "You must insert an Airplane Range.")]
        public string RangeDistance { get; set; }

        public virtual ICollection<Airplane> Airplanes { get; set; }
    }
}