﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace airlinemvc.Models
{
    public class City
    {
        [Key]
        public int CityID { get; set; }

        [Display(Name = "City")]
        //[Required(ErrorMessage ="You have to insert a City Name.")]
        //[StringLength(30, ErrorMessage ="The City name has to be at least {2} characters long.", MinimumLength = 2)]
        public string CityName { get; set; }

        [Display(Name = "Latitude")]
        public string Lat { get; set; }

        [Display(Name = "Longitude")]
        public string Long { get; set; }

        [Display(Name = "Time Zone")]
        //[Required(ErrorMessage ="You have to insert a Time value.")]
        //[DataType(DataType.Time)]
        public string TimeZone { get; set; }

        [Display(Name = "GMT")]
        public string GMT { get; set; }

        //[Required(ErrorMessage = "You have to insert a Country.")]
        public int CountryID { get; set; }

        public virtual Country Country { get; set; }

        public virtual ICollection<Flight> OriginLocationCities { get; set; }

        public virtual ICollection<Flight> DestinationLocationCities { get; set; }
    }
}