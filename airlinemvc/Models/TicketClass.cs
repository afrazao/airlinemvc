﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace airlinemvc.Models
{
    public class TicketClass
    {
        [Key]
        public int TicketClassID { get; set; }

        [Required(ErrorMessage = "You must insert a {0}")]
        [Display(Name = "Class")]
        public string TicketClassName { get; set; }

        public virtual ICollection<TicketPrice> TicketPrices { get; set; }
    }
}