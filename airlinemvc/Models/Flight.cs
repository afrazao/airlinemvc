﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace airlinemvc.Models
{

    public class Flight
    {
        private AirlineMVCContext db = new AirlineMVCContext();

        [Key]
        public int FlightID { get; set; }

        [Display(Name = "Origin Country")]
        [Required(ErrorMessage = "You have to choose a Country Name.")]
        public int CountryOriginID { get; set; }

        [Display(Name ="Origin City")]
        [Required(ErrorMessage = "You have to choose a City Name.")]
        public int OriginID { get; set; }

        [Display(Name = "Destination Country")]
        [Required(ErrorMessage = "You have to choose a Country Name.")]
        public int CountryDestinationID { get; set; }

        [Display(Name = "Destination City")]
        [Required(ErrorMessage = "You have to choose a City Name.")]
        public int DestinationID { get; set; }

        [Display(Name = "Airplane")]
        [Required(ErrorMessage = "You have to choose an Airplane.")]
        public int AirplaneID { get; set; }

        [Display(Name = "Departure")]
        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy HH:mm}", ApplyFormatInEditMode = true)]
        [DataType(DataType.DateTime)]
        public DateTime DepartureTime { get; set; }

        [Display(Name = "Arrival")]
        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy HH:mm}")]
        public DateTime ArrivalTime { get; set; }

        [Display(Name = "Total of Flight Seats")]
        [Required(ErrorMessage = "You have to insert a value.")]
        public int FlightTotalSeats { get; set; }

        [Display(Name = "Available Flight Seats")]
        [Required(ErrorMessage = "You have to insert a value.")]
        public int FlightAvailableSeats { get; set; }

        [ForeignKey("CountryOriginID")]
        [InverseProperty("OriginLocationCountries")]
        public virtual Country CountryOrigin { get; set; }

        [ForeignKey("CountryDestinationID")]
        [InverseProperty("DestinationLocationCountries")]
        public virtual Country CountryDestination { get; set; }

        [ForeignKey("OriginID")]
        [InverseProperty("OriginLocationCities")]
        public virtual City Origin { get; set; }

        [ForeignKey("DestinationID")]
        [InverseProperty("DestinationLocationCities")]
        public virtual City Destination { get; set; }

        public virtual Airplane Airplane { get; set; }

        public virtual ICollection<Ticket> Tickets { get; set; }

        public string FromTo
        {
            get
            {
                var origin = db.Cities.Find(CountryOriginID);
                var destination = db.Cities.Find(CountryDestinationID);
                return $"{origin.CityName} - {destination.CityName} - {DepartureTime}";
            }
        }
    }
}