﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace airlinemvc.Models
{
    public class TicketType
    {
        [Key]
        public int TicketTypeID { get; set; }

        [Required(ErrorMessage = "You must insert a {0}")]
        [Display(Name = "Type")]
        public string TicketTypeName { get; set; }

        public virtual ICollection<TicketPrice> TicketPrices { get; set; }
    }
}