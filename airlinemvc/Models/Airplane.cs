﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace airlinemvc.Models
{
    public class Airplane
    {
        [Key]
        public int AirplaneID { get; set; }

        [Required(ErrorMessage = "You must insert an Airplane {0}.")]
        [Display(Name ="Brand")]
        public string Brand { get; set; }

        [Display(Name = "Model")]
        [Required(ErrorMessage = "You must insert an Airplane {0}.")]
        public string Model { get; set; }

        [Display(Name = "Range")]
        [Required(ErrorMessage = "You must insert an Airplane Range.")]
        public int RangeID { get; set; }

        [Display(Name = "Cruise Speed")]
        [Required(ErrorMessage ="You must insert an Airplane Cruising Speed between 400mph and 900mph.")]
        //[Range(400, 900, ErrorMessage = "You must insert an Airplane Cruising Speed between {1}mph and {2}mph.")]
        public int CruiseSpeed { get; set; }

        [Display(Name = "Seats")]
        [Required(ErrorMessage = "The value must be between 100 seats and 300 seats.")]
        //[Range(100, 300, ErrorMessage = "The value must be between {1} seats and {2} seats.")]
        public int Seats { get; set; }

        public string AirplaneBrandModel
        {
            get
            {
                return Brand + " " + Model;
            }
        }

        public virtual AirplaneRange AirplaneRange { get; set; }

        public virtual ICollection<Flight> FlightAirplane { get; set; }
    }
}