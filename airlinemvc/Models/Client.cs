﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace airlinemvc.Models
{
    public class Client
    {
        [Key]
        public int ClientID { get; set; }

        [Display(Name = "First Name")]
        [Required(ErrorMessage ="You have to insert a First Name.")]
        [StringLength(50, ErrorMessage = "The {0} has to be {1} characters long.")]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        [Required(ErrorMessage = "You have to insert a Last Name.")]
        [StringLength(50, ErrorMessage = "The {0} has to be {1} characters long.")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "You have to insert a Date of Birth.")]
        [Display(Name = "Date of Birth")]
        [DataType(DataType.Date)]
        public DateTime DateOfBirth { get; set; }

        [Required(ErrorMessage ="You must enter a TIN Number.")]
        public int TIN { get; set; }

        [Required(ErrorMessage = "You have to have an email associated.")]
        public string Email { get; set; }

        public virtual ICollection<Ticket> Tickets { get; set; }
    }
}