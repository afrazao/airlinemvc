﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace airlinemvc.Models
{
    public class Country
    {
        [Key]
        public int CountryID { get; set; }

        [Display(Name ="Country")]
        //[Required(ErrorMessage ="You must enter a Country.")]
        //[StringLength(50, ErrorMessage ="The Country name must be at least characters long.")]
        public string CountryName { get; set; }

        public virtual ICollection<City> Cities { get; set; }

        public virtual ICollection<Flight> OriginLocationCountries { get; set; }

        public virtual ICollection<Flight> DestinationLocationCountries { get; set; }
    }
}