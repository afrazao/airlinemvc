﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace airlinemvc.Models
{
    public class Response
    {
        public bool IsSuccess { get; set; }

        public String Message { get; set; }

        public Object Result { get; set; }
    }
}