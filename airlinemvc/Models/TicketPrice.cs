﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace airlinemvc.Models
{
    public class TicketPrice
    { 
        [Key]
        public int TicketPriceID { get; set; }

        [Required(ErrorMessage = "You must insert an {0}")]
        [Display(Name = "ID Name")]
        public string TicketPriceName { get; set; }

        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString = "{0:C2}", ApplyFormatInEditMode = false)]
        [Required(ErrorMessage = "You must insert a {0}")]
        [Display(Name = "Price")]
        public double TicketPriceValue { get; set; }

        [Required(ErrorMessage = "You must insert a Ticket Class {0}")]
        public int TicketClassID { get; set; }

        [Required(ErrorMessage = "You must insert a Ticket Type {0}")]
        public int TicketTypeID { get; set; }

        public virtual TicketClass TicketClass { get; set; }

        public virtual TicketType TicketType { get; set; }

        public virtual ICollection<Ticket> Tickets { get; set; }
    }
}