﻿using airlinemvc.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace airlinemvc.Models
{
    public class Ticket
    {
        [Key]
        public int TicketID { get; set; }

        [Required(ErrorMessage = "You must insert a {0}")]
        [Display(Name = "Name")]
        [StringLength(50, ErrorMessage = "The {0} has to be {1} characters long.")]
        public string Name { get; set; }

        [Required(ErrorMessage = "You must insert a {0}")]
        [Display(Name = "Date of Birth")]
        [DataType(DataType.Date)]
        public DateTime DOB { get; set; }

        [Display(Name = "Class")]
        public string Class { get; set; }

        [Display(Name = "Type")]
        public string Type { get; set; }

        [Required(ErrorMessage = "You must insert a {0}")]
        [Display(Name = "Flight")]
        public int FlightID { get; set; }

        [Required(ErrorMessage = "You must insert a Seat Number")]
        [Display(Name = "Seat Number")]
        public string Seat { get; set; }

        [Required(ErrorMessage = "You must insert a Price")]
        [Display(Name = "Price")]
        public int TicketPriceID { get; set; }

        [Required(ErrorMessage = "You must insert a {0}")]
        [Display(Name = "Client")]
        public int ClientID { get; set; }

        public virtual Flight Flight { get; set; }

        public virtual Client Client { get; set; }

        public virtual TicketPrice TicketPrice { get; set; }
    }
}