namespace airlinemvc.Migrations
{
    using airlinemvc.Models;
    using airlinemvc.Services;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Threading.Tasks;

    internal sealed class Configuration : DbMigrationsConfiguration<airlinemvc.Models.AirlineMVCContext>
    {

        public List<APIModel> CountriesList { get; set; }

        public List<APICityModel> CitiesList { get; set; }

        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
            ContextKey = "airlinemvc.Models.AirlineMVCContext";
            LoadData();
            LoadAPICitiesData();
        }

        protected override void Seed(airlinemvc.Models.AirlineMVCContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.

            if (!context.AirplaneRanges.Any())
            {
                context.AirplaneRanges.Add(new AirplaneRange { RangeDistance = "Long-Distance" });
                context.AirplaneRanges.Add(new AirplaneRange { RangeDistance = "Medium-Distance" });
                context.AirplaneRanges.Add(new AirplaneRange { RangeDistance = "Short-Distance" });

                try
                {
                    context.SaveChanges();
                }
                catch (Exception e)
                {
                    string exc = e.Message;
                }
            }

            var LongRange = (from r in context.AirplaneRanges where r.RangeDistance == "Long-Distance" select r).FirstOrDefault();
            var MediumRange = (from r in context.AirplaneRanges where r.RangeDistance == "Medium-Distance" select r).FirstOrDefault();
            var ShortRange = (from r in context.AirplaneRanges where r.RangeDistance == "Short-Distance" select r).FirstOrDefault();

            if (!context.Airplanes.Any())
            {
                context.Airplanes.Add(new Airplane { Brand = "Boeing", Model = "747 SP", RangeID = LongRange.RangeID, CruiseSpeed = 903, Seats = 276});
                context.Airplanes.Add(new Airplane { Brand = "Boeing", Model = "320", RangeID = MediumRange.RangeID, CruiseSpeed = 750, Seats = 200 });
                context.Airplanes.Add(new Airplane { Brand = "ATR-72", Model = "600", RangeID = ShortRange.RangeID, CruiseSpeed = 500, Seats = 70 });

                try
                {
                    context.SaveChanges();
                }
                catch (Exception e)
                {
                    string exc = e.Message;
                }
            }

            if (!context.TicketClasses.Any())
            {
                context.TicketClasses.Add(new TicketClass { TicketClassName = "Executive" });
                context.TicketClasses.Add(new TicketClass { TicketClassName = "Economic" });

                try
                {
                    context.SaveChanges();
                }
                catch (Exception e)
                {
                    string exc = e.Message;
                }
            }

            if (!context.TicketTypes.Any())
            {
                context.TicketTypes.Add(new TicketType { TicketTypeName = "Adult" });
                context.TicketTypes.Add(new TicketType { TicketTypeName = "Child" });
                context.TicketTypes.Add(new TicketType { TicketTypeName = "Infant" });

                try
                {
                    context.SaveChanges();
                }
                catch (Exception e)
                {
                    string exc = e.Message;
                }
            }

            var classPriceExecutive = (from c in context.TicketClasses where c.TicketClassName == "Executive" select c).FirstOrDefault();
            var classPriceEconomic = (from c in context.TicketClasses where c.TicketClassName == "Economic" select c).FirstOrDefault();
            var classTypeAdult = (from c in context.TicketTypes where c.TicketTypeName == "Adult" select c).FirstOrDefault();
            var classTypeChild = (from c in context.TicketTypes where c.TicketTypeName == "Child" select c).FirstOrDefault();
            var classTypeInfant = (from c in context.TicketTypes where c.TicketTypeName == "Infant" select c).FirstOrDefault();

            if (!context.TicketPrices.Any())
            {
                context.TicketPrices.Add(new TicketPrice { TicketPriceName = "AA", TicketPriceValue = 1000, TicketClassID = classPriceExecutive.TicketClassID, TicketTypeID = classTypeAdult.TicketTypeID });
                context.TicketPrices.Add(new TicketPrice { TicketPriceName = "AB", TicketPriceValue = 600, TicketClassID = classPriceEconomic.TicketClassID, TicketTypeID = classTypeAdult.TicketTypeID });
                context.TicketPrices.Add(new TicketPrice { TicketPriceName = "CA", TicketPriceValue = 700, TicketClassID = classPriceExecutive.TicketClassID, TicketTypeID = classTypeChild.TicketTypeID });
                context.TicketPrices.Add(new TicketPrice { TicketPriceName = "CB", TicketPriceValue = 350, TicketClassID = classPriceEconomic.TicketClassID, TicketTypeID = classTypeChild.TicketTypeID });
                context.TicketPrices.Add(new TicketPrice { TicketPriceName = "IA", TicketPriceValue = 500, TicketClassID = classPriceExecutive.TicketClassID, TicketTypeID = classTypeInfant.TicketTypeID });
                context.TicketPrices.Add(new TicketPrice { TicketPriceName = "IB", TicketPriceValue = 150, TicketClassID = classPriceEconomic.TicketClassID, TicketTypeID = classTypeInfant.TicketTypeID });

                try
                {
                    context.SaveChanges();
                }
                catch (Exception e)
                {
                    string exc = e.Message;
                }
            }

            if (!context.Countries.Any())
            {
                if (CountriesList != null)
                {
                    foreach (var country in CountriesList)
                    {
                        if ((country.nameCountry != "Antarctica") || (country.nameCountry != "Bonaire, Saint Eustatius and Saba") || (country.nameCountry != "Heard Island and McDonald Islands")
                                                    || (country.nameCountry != "Tokelau") || (country.nameCountry != "United States Minor Outlying Islands") || (country.nameCountry != "Andorra")
                                                     || (country.nameCountry != "Anguilla") || (country.nameCountry != "Aruba") || (country.nameCountry != "Benin") || (country.nameCountry != "Bouvet Island")
                                                      || (country.nameCountry != "Cocos Islands") || (country.nameCountry != "Cook Islands") || (country.nameCountry != "Cura�ao") || (country.nameCountry != "Christmas Island") || (country.nameCountry != "Western Sahara")
                                                       || (country.nameCountry != "Falkland Islands") || (country.nameCountry != "Micronesia") || (country.nameCountry != "Faroe Islands") || (country.nameCountry != "Grenada") || (country.nameCountry != "Guernsey")
                                                        || (country.nameCountry != "Guadeloupe") || (country.nameCountry != "South Georgia and the South Sandwich Islands") || (country.nameCountry != "Haiti") || (country.nameCountry != "Isle of Man") || (country.nameCountry != "British Indian Ocean Territory")
                                                         || (country.nameCountry != "Jersey") || (country.nameCountry != "Liechtenstein") || (country.nameCountry != "Libya") || (country.nameCountry != "Monaco")
                                                          || (country.nameCountry != "Saint Martin") || (country.nameCountry != "Mongolia") || (country.nameCountry != "Macao") || (country.nameCountry != "Martinique") || (country.nameCountry != "Malta")
                                                           || (country.nameCountry != "Mauritius") || (country.nameCountry != "Nauru") || (country.nameCountry != "Niue") || (country.nameCountry != "Saint Pierre and Miquelon") || (country.nameCountry != "Pitcairn")
                                                            || (country.nameCountry != "Palestinian Territory") || (country.nameCountry != "Palau") || (country.nameCountry != "Reunion") || (country.nameCountry != "Singapore") || (country.nameCountry != "San Marino")
                                                             || (country.nameCountry != "Sao Tome and Principe") || (country.nameCountry != "Swaziland") || (country.nameCountry != "Turks and Caicos Islands") || (country.nameCountry != "French Southern Territories")
                                                              || (country.nameCountry != "Trinidad and Tobago") || (country.nameCountry != "Tuvalu") || (country.nameCountry != "Uganda") || (country.nameCountry != "Vatican") || (country.nameCountry != "Saint Vincent and the Grenadines") || (country.nameCountry != "British Virgin Islands")
                                                               || (country.nameCountry != "Wallis and Futuna") || (country.nameCountry != "Mayotte"))
                        {
                            context.Countries.Add(new Country { CountryName = country.nameCountry });

                            try
                            {
                                context.SaveChanges();
                            }
                            catch (Exception e)
                            {
                                string exc = e.Message;
                            }
                        }
                    }
                }
            }

            if (!context.Cities.Any())
            {
                if(CountriesList != null)
                {
                    foreach (var country in CountriesList)
                    {
                        if ((country.nameCountry != "Antarctica") || (country.nameCountry != "Bonaire, Saint Eustatius and Saba") || (country.nameCountry != "Heard Island and McDonald Islands") 
                            || (country.nameCountry != "Tokelau") || (country.nameCountry != "United States Minor Outlying Islands") || (country.nameCountry != "Andorra")
                             || (country.nameCountry != "Anguilla") || (country.nameCountry != "Aruba") || (country.nameCountry != "Benin") || (country.nameCountry != "Bouvet Island")
                              || (country.nameCountry != "Cocos Islands") || (country.nameCountry != "Cook Islands") || (country.nameCountry != "Cura�ao") || (country.nameCountry != "Christmas Island") || (country.nameCountry != "Western Sahara")
                               || (country.nameCountry != "Falkland Islands") || (country.nameCountry != "Micronesia") || (country.nameCountry != "Faroe Islands") || (country.nameCountry != "Grenada") || (country.nameCountry != "Guernsey")
                                || (country.nameCountry != "Guadeloupe") || (country.nameCountry != "South Georgia and the South Sandwich Islands") || (country.nameCountry != "Haiti") || (country.nameCountry != "Isle of Man") || (country.nameCountry != "British Indian Ocean Territory")
                                 || (country.nameCountry != "Jersey") || (country.nameCountry != "Liechtenstein") || (country.nameCountry != "Libya") || (country.nameCountry != "Monaco")
                                  || (country.nameCountry != "Saint Martin") || (country.nameCountry != "Mongolia") || (country.nameCountry != "Macao") || (country.nameCountry != "Martinique") || (country.nameCountry != "Malta")
                                   || (country.nameCountry != "Mauritius") || (country.nameCountry != "Nauru") || (country.nameCountry != "Niue") || (country.nameCountry != "Saint Pierre and Miquelon") || (country.nameCountry != "Pitcairn")
                                    || (country.nameCountry != "Palestinian Territory") || (country.nameCountry != "Palau") || (country.nameCountry != "Reunion") || (country.nameCountry != "Singapore") || (country.nameCountry != "San Marino")
                                     || (country.nameCountry != "Sao Tome and Principe") || (country.nameCountry != "Swaziland") || (country.nameCountry != "Turks and Caicos Islands") || (country.nameCountry != "French Southern Territories")
                                      || (country.nameCountry != "Trinidad and Tobago") || (country.nameCountry != "Tuvalu") || (country.nameCountry != "Uganda") || (country.nameCountry != "Vatican") || (country.nameCountry != "Saint Vincent and the Grenadines") || (country.nameCountry != "British Virgin Islands")
                                       || (country.nameCountry != "Wallis and Futuna") || (country.nameCountry != "Mayotte"))
                        {
                            var countryName = country.nameCountry;

                            int id = (from countryid in context.Countries
                                      where countryid.CountryName == countryName
                                      select countryid.CountryID).FirstOrDefault();

                            int idcountry = id;

                            string cityLat = (from city in CitiesList
                                              where city.nameCity == country.capital
                                              select city.latitudeCity).FirstOrDefault();

                            string cityLong = (from city in CitiesList
                                               where city.nameCity == country.capital
                                               select city.longitudeCity).FirstOrDefault();

                            string timezone = (from city in CitiesList
                                               where city.nameCity == country.capital
                                               select city.timezone).FirstOrDefault();

                            string gmt = (from city in CitiesList
                                          where city.nameCity == country.capital
                                          select city.GMT).FirstOrDefault();

                            context.Cities.Add(new City { CityName = country.capital, CountryID = idcountry, Lat = cityLat, Long = cityLong, TimeZone = timezone, GMT = gmt });

                            try
                            {
                                context.SaveChanges();
                            }
                            catch (Exception e)
                            {
                                string exc = e.Message;
                            }
                        }
                    }
                }
            }
        }

        private async Task LoadAPIData()
        {
            try
            {
                var responseCountries = await DataService.GetAPIData("https://aviation-edge.com", "/v2/public/countryDatabase?key=dcad8e-4b8595");

                CountriesList = (List<APIModel>)responseCountries.Result;
            }
            catch (Exception e)
            {
                
            }
        }

        private async Task LoadData()
        {
            await LoadAPIData();
        }

        private async Task LoadAPICitiesData()
        {
            try
            {
                var responseCities = await DataService.GetAPICitiesData("https://aviation-edge.com", "/v2/public/cityDatabase?key=dcad8e-4b8595");

                CitiesList = (List<APICityModel>)responseCities.Result;
            }
            catch (Exception e)
            {
                
            }
        }

        private async Task LoadCitiesData()
        {
            await LoadAPICitiesData();
        }
    }
}
