﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using airlinemvc.Models;

namespace airlinemvc.Controllers
{
    public class CitiesController : Controller
    {
        private AirlineMVCContext db = new AirlineMVCContext();

        [Authorize(Roles = "View")]
        // GET: Cities
        public ActionResult Index()
        {
            var cities = db.Cities.Include(c => c.Country);
            return View(cities.ToList());
        }

        [AllowAnonymous]
        // GET: Cities
        public ActionResult IndexAnonymous()
        {
            var cities = db.Cities.Include(c => c.Country);
            return View(cities.ToList());
        }

        [Authorize(Roles = "EmployeeView")]
        // GET: Cities
        public ActionResult IndexEmployee()
        {
            var cities = db.Cities.Include(c => c.Country);
            return View(cities.ToList());
        }

        [Authorize(Users = "antonio.franco.frazao@formandos.cinel.pt")]
        // GET: Cities/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("BadRequest", "Requests");
            }
            City city = db.Cities.Find(id);
            if (city == null)
            {
                return RedirectToAction("PageNotFound", "Requests");
            }
            return View(city);
        }

        [Authorize(Roles = "Create")]
        // GET: Cities/Create
        public ActionResult Create()
        {
            ViewBag.CountryID = new SelectList(db.Countries, "CountryID", "CountryName");
            return View();
        }

        // POST: Cities/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CityID,CityName,Lat,Long,TimeZone,GMT,CountryID")] City city)
        {
            if (ModelState.IsValid)
            {
                db.Cities.Add(city);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CountryID = new SelectList(db.Countries, "CountryID", "CountryName", city.CountryID);
            return View(city);
        }

        [Authorize(Roles = "EmployeeCreate")]
        // GET: Cities/Create
        public ActionResult CreateEmployee()
        {
            ViewBag.CountryID = new SelectList(db.Countries, "CountryID", "CountryName");
            return View();
        }

        // POST: Cities/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateEmployee([Bind(Include = "CityID,CityName,Lat,Long,TimeZone,GMT,CountryID")] City city)
        {
            if (ModelState.IsValid)
            {
                db.Cities.Add(city);
                db.SaveChanges();
                return RedirectToAction("IndexEmployee");
            }

            ViewBag.CountryID = new SelectList(db.Countries, "CountryID", "CountryName", city.CountryID);
            return View(city);
        }

        [Authorize(Roles = "Edit")]
        // GET: Cities/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("BadRequest", "Requests");
            }
            City city = db.Cities.Find(id);
            if (city == null)
            {
                return RedirectToAction("PageNotFound", "Requests");
            }
            ViewBag.CountryID = new SelectList(db.Countries, "CountryID", "CountryName", city.CountryID);
            return View(city);
        }

        // POST: Cities/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CityID,CityName,Lat,Long,TimeZone,GMT,CountryID")] City city)
        {
            if (ModelState.IsValid)
            {
                db.Entry(city).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CountryID = new SelectList(db.Countries, "CountryID", "CountryName", city.CountryID);
            return View(city);
        }

        [Authorize(Roles = "EmployeeEdit")]
        // GET: Cities/Edit/5
        public ActionResult EditEmployee(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("BadRequest", "Requests");
            }
            City city = db.Cities.Find(id);
            if (city == null)
            {
                return RedirectToAction("PageNotFound", "Requests");
            }
            ViewBag.CountryID = new SelectList(db.Countries, "CountryID", "CountryName", city.CountryID);
            return View(city);
        }

        // POST: Cities/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditEmployee([Bind(Include = "CityID,CityName,Lat,Long,TimeZone,GMT,CountryID")] City city)
        {
            if (ModelState.IsValid)
            {
                db.Entry(city).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("IndexEmployee");
            }
            ViewBag.CountryID = new SelectList(db.Countries, "CountryID", "CountryName", city.CountryID);
            return View(city);
        }

        [Authorize(Roles = "Delete")]
        // GET: Cities/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("BadRequest", "Requests");
            }
            City city = db.Cities.Find(id);
            if (city == null)
            {
                return RedirectToAction("PageNotFound", "Requests");
            }
            return View(city);
        }

        // POST: Cities/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            City city = db.Cities.Find(id);
            db.Cities.Remove(city);
            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                RedirectToAction("NotPossible", "Requests");
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
