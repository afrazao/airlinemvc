﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using airlinemvc.Models;

namespace airlinemvc.Controllers
{
    
    public class TicketsController : Controller
    {
        private AirlineMVCContext db = new AirlineMVCContext();

        [Authorize(Roles = "View")]
        // GET: Tickets
        public ActionResult Index()
        {
            var tickets = db.Tickets.Include(t => t.Client).Include(t => t.Flight).Include(t => t.TicketPrice);
            return View(tickets.ToList());
        }

        [Authorize(Roles = "ClientView")]
        // GET: Tickets
        public ActionResult IndexClient()
        {
            List<Ticket> ticketList = new List<Ticket>();

            var user = User.Identity.Name.ToString();

            int clients = (from c in db.Clients where c.Email == user select c.ClientID).FirstOrDefault();

            if (clients != 0)
            {
                ticketList = (from t in db.Tickets.Include(t => t.Client).Include(t => t.Flight).Include(t => t.TicketPrice) where t.ClientID == clients select t).ToList();
            }
            else
            {
                return RedirectToAction("CreateClient", "Clients");
            }

            return View(ticketList.ToList());
        }

        [Authorize(Roles = "EmployeeView")]
        // GET: Tickets
        public ActionResult IndexEmployee(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("BadRequest", "Requests");
            }
            Flight flight = db.Flights.Find(id);
            if (flight == null)
            {
                return RedirectToAction("PageNotFound", "Requests");
            }
            List<Ticket> ticketList = new List<Ticket>();

            ticketList = (from t in db.Tickets.Include(t => t.Client).Include(t => t.Flight).Include(t => t.TicketPrice) where t.FlightID== id select t).ToList();

            string getCityOrigin = (from c in db.Cities where c.CityID == flight.OriginID select c.CityName).FirstOrDefault();

            string getCityDestination = (from c in db.Cities where c.CityID == flight.DestinationID select c.CityName).FirstOrDefault();

            string info = $"Flight Number {flight.FlightID} from {getCityOrigin} to {getCityDestination} - {flight.DepartureTime}";

            ViewBag.info = info;

            return View(ticketList.ToList());
        }

        [Authorize(Roles = "View")]
        // GET: Tickets
        public ActionResult IndexAdmin(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("BadRequest", "Requests");
            }
            Flight flight = db.Flights.Find(id);
            if (flight == null)
            {
                return RedirectToAction("PageNotFound", "Requests");
            }
            List<Ticket> ticketList = new List<Ticket>();

            ticketList = (from t in db.Tickets.Include(t => t.Client).Include(t => t.Flight).Include(t => t.TicketPrice) where t.FlightID == id select t).ToList();

            string getCityOrigin = (from c in db.Cities where c.CityID == flight.OriginID select c.CityName).FirstOrDefault();

            string getCityDestination = (from c in db.Cities where c.CityID == flight.DestinationID select c.CityName).FirstOrDefault();

            string info = $"Flight Number {flight.FlightID} from {getCityOrigin} to {getCityDestination} - {flight.DepartureTime}";

            ViewBag.info = info;

            return View(ticketList.ToList());
        }

        [Authorize(Roles = "EmployeeView")]
        // GET: Flights
        public ActionResult IndexTicketsEmployee()
        {
            var tickets = db.Tickets.Include(t => t.Client).Include(t => t.Flight).Include(t => t.TicketPrice);
            return View(tickets.ToList());
        }

        [Authorize(Users ="antonio.franco.frazao@formandos.cinel.pt")]
        // GET: Tickets/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("BadRequest", "Requests");
            }
            Ticket ticket = db.Tickets.Find(id);
            if (ticket == null)
            {
                return RedirectToAction("PageNotFound", "Requests");
            }
            return View(ticket);
        }

        [Authorize(Roles = "Create")]
        // GET: Tickets/Create
        public ActionResult Create(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("BadRequest", "Requests");
            }
            Flight flight = db.Flights.Find(id);
            if (flight == null)
            {
                return RedirectToAction("PageNotFound", "Requests");
            }

            string getCityOrigin = (from c in db.Cities where c.CityID == flight.OriginID select c.CityName).FirstOrDefault();

            string getCityDestination = (from c in db.Cities where c.CityID == flight.DestinationID select c.CityName).FirstOrDefault();

            string info = $"Flight Number {flight.FlightID} from {getCityOrigin} to {getCityDestination} - {flight.DepartureTime}";

            ViewBag.info = info;

            ViewBag.seats = flight.FlightTotalSeats;

            List<Ticket> TicketsList = (from t in db.Tickets where t.FlightID == flight.FlightID select t).ToList();

            List<string> TicketSeat = (from ts in TicketsList select ts.Seat).ToList();

            ViewBag.tickets = TicketSeat;

            ViewBag.Type = new SelectList(db.TicketTypes, "TicketTypeID", "TicketTypeName");
            ViewBag.Class = new SelectList(db.TicketClasses, "TicketClassID", "TicketClassName");
            ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "FirstName");
            ViewBag.TicketPriceID = new SelectList(db.TicketPrices, "TicketPriceID", "TicketPriceName");
            return View();
        }

        // POST: Tickets/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "TicketID,Name,DOB,Class,Type,FlightID,Seat,TicketPriceID,ClientID")] Ticket ticket, int? id)
        {
            if (id == null)
            {
                return RedirectToAction("BadRequest", "Requests");
            }
            Flight flight = db.Flights.Find(id);
            if (flight == null)
            {
                return RedirectToAction("PageNotFound", "Requests");
            }

            var dateDOB = Request["DOB"];

            DateTime dateCheck;

            if (dateDOB == "")
            {
                Flight flightPostClass = (from f in db.Flights where f.FlightID == id select f).FirstOrDefault();

                string getCityOriginClass = (from c in db.Cities where c.CityID == flight.OriginID select c.CityName).FirstOrDefault();

                string getCityDestinationClass = (from c in db.Cities where c.CityID == flight.DestinationID select c.CityName).FirstOrDefault();

                string infoClass = $"Flight Number {flight.FlightID} from {getCityOriginClass } to {getCityDestinationClass } - {flight.DepartureTime}";

                ViewBag.info = infoClass;

                ViewBag.seats = flight.FlightTotalSeats;

                List<Ticket> TicketsListClass = (from t in db.Tickets where t.FlightID == flight.FlightID select t).ToList();

                List<string> TicketSeatClass = (from ts in TicketsListClass select ts.Seat).ToList();

                ViewBag.tickets = TicketSeatClass;

                ViewBag.Type = new SelectList(db.TicketTypes, "TicketTypeID", "TicketTypeName");
                ViewBag.Class = new SelectList(db.TicketClasses, "TicketClassID", "TicketClassName");
                ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "FirstName");
                ViewBag.TicketPriceID = new SelectList(db.TicketPrices, "TicketPriceID", "TicketPriceName");

                ModelState.AddModelError("DOB", "You have to choose a valid Date of Birth");
                return View();
            }
            else
            {
                dateCheck = DateTime.Parse(Request["DOB"]);

                if (dateCheck >= DateTime.Now.Date)
                {
                    Flight flightPostClass = (from f in db.Flights where f.FlightID == id select f).FirstOrDefault();

                    string getCityOriginClass = (from c in db.Cities where c.CityID == flight.OriginID select c.CityName).FirstOrDefault();

                    string getCityDestinationClass = (from c in db.Cities where c.CityID == flight.DestinationID select c.CityName).FirstOrDefault();

                    string infoClass = $"Flight Number {flight.FlightID} from {getCityOriginClass } to {getCityDestinationClass } - {flight.DepartureTime}";

                    ViewBag.info = infoClass;

                    ViewBag.seats = flight.FlightTotalSeats;

                    List<Ticket> TicketsListClass = (from t in db.Tickets where t.FlightID == flight.FlightID select t).ToList();

                    List<string> TicketSeatClass = (from ts in TicketsListClass select ts.Seat).ToList();

                    ViewBag.tickets = TicketSeatClass;

                    ViewBag.Type = new SelectList(db.TicketTypes, "TicketTypeID", "TicketTypeName");
                    ViewBag.Class = new SelectList(db.TicketClasses, "TicketClassID", "TicketClassName");
                    ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "FirstName");
                    ViewBag.TicketPriceID = new SelectList(db.TicketPrices, "TicketPriceID", "TicketPriceName");

                    ModelState.AddModelError("DOB", "You have to choose a valid Date of Birth");
                    return View();
                }
            }

            var classCheck = Request["Class"];

            if (string.IsNullOrEmpty(classCheck))
            {
                Flight flightPostClass = (from f in db.Flights where f.FlightID == id select f).FirstOrDefault();

                string getCityOriginClass = (from c in db.Cities where c.CityID == flight.OriginID select c.CityName).FirstOrDefault();

                string getCityDestinationClass = (from c in db.Cities where c.CityID == flight.DestinationID select c.CityName).FirstOrDefault();

                string infoClass = $"Flight Number {flight.FlightID} from {getCityOriginClass } to {getCityDestinationClass } - {flight.DepartureTime}";

                ViewBag.info = infoClass;

                ViewBag.seats = flight.FlightTotalSeats;

                List<Ticket> TicketsListClass = (from t in db.Tickets where t.FlightID == flight.FlightID select t).ToList();

                List<string> TicketSeatClass = (from ts in TicketsListClass select ts.Seat).ToList();

                ViewBag.tickets = TicketSeatClass;

                ViewBag.Type = new SelectList(db.TicketTypes, "TicketTypeID", "TicketTypeName");
                ViewBag.Class = new SelectList(db.TicketClasses, "TicketClassID", "TicketClassName");
                ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "FirstName");
                ViewBag.TicketPriceID = new SelectList(db.TicketPrices, "TicketPriceID", "TicketPriceName");

                ModelState.AddModelError("Class", "You have to choose a Class");
                return View();
            }

            var typeCheck = Request["Type"];

            if (string.IsNullOrEmpty(typeCheck))
            {
                Flight flightPostType = (from f in db.Flights where f.FlightID == id select f).FirstOrDefault();

                string getCityOriginType = (from c in db.Cities where c.CityID == flight.OriginID select c.CityName).FirstOrDefault();

                string getCityDestinationType = (from c in db.Cities where c.CityID == flight.DestinationID select c.CityName).FirstOrDefault();

                string infoType = $"Flight Number {flight.FlightID} from {getCityOriginType } to {getCityDestinationType } - {flight.DepartureTime}";

                ViewBag.info = infoType;

                ViewBag.seats = flight.FlightTotalSeats;

                List<Ticket> TicketsListType = (from t in db.Tickets where t.FlightID == flight.FlightID select t).ToList();

                List<string> TicketSeatType = (from ts in TicketsListType select ts.Seat).ToList();

                ViewBag.tickets = TicketSeatType;

                ViewBag.Type = new SelectList(db.TicketTypes, "TicketTypeID", "TicketTypeName");
                ViewBag.Class = new SelectList(db.TicketClasses, "TicketClassID", "TicketClassName");
                ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "FirstName");
                ViewBag.TicketPriceID = new SelectList(db.TicketPrices, "TicketPriceID", "TicketPriceName");

                ModelState.AddModelError("Type", "You have to choose a Type");
                return View();
            }

            int ticketc = int.Parse(Request["Class"]);

            string ticketclass = (from c in db.TicketClasses where c.TicketClassID == ticketc select c.TicketClassName).FirstOrDefault();

            int tickett = int.Parse(Request["Type"]);

            string tickettype = (from c in db.TicketTypes where c.TicketTypeID == tickett select c.TicketTypeName).FirstOrDefault();

            DateTime checkDate = DateTime.Today.Date.AddYears(-2);

            if (tickettype == "Infant" && dateCheck < checkDate)
            {
                Flight flightPostClass = (from f in db.Flights where f.FlightID == id select f).FirstOrDefault();

                string getCityOriginClass = (from c in db.Cities where c.CityID == flight.OriginID select c.CityName).FirstOrDefault();

                string getCityDestinationClass = (from c in db.Cities where c.CityID == flight.DestinationID select c.CityName).FirstOrDefault();

                string infoClass = $"Flight Number {flight.FlightID} from {getCityOriginClass } to {getCityDestinationClass } - {flight.DepartureTime}";

                ViewBag.info = infoClass;

                ViewBag.seats = flight.FlightTotalSeats;

                List<Ticket> TicketsListClass = (from t in db.Tickets where t.FlightID == flight.FlightID select t).ToList();

                List<string> TicketSeatClass = (from ts in TicketsListClass select ts.Seat).ToList();

                ViewBag.tickets = TicketSeatClass;

                ViewBag.Type = new SelectList(db.TicketTypes, "TicketTypeID", "TicketTypeName");
                ViewBag.Class = new SelectList(db.TicketClasses, "TicketClassID", "TicketClassName");
                ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "FirstName");
                ViewBag.TicketPriceID = new SelectList(db.TicketPrices, "TicketPriceID", "TicketPriceName");

                ModelState.AddModelError("DOB", "The Date you introduced is not valid for an Infant");
                return View();
            }

            DateTime checkDateChild = DateTime.Today.Date.AddYears(-18);

            if (tickettype == "Child" && dateCheck > checkDate)
            {
                Flight flightPostClass = (from f in db.Flights where f.FlightID == id select f).FirstOrDefault();

                string getCityOriginClass = (from c in db.Cities where c.CityID == flight.OriginID select c.CityName).FirstOrDefault();

                string getCityDestinationClass = (from c in db.Cities where c.CityID == flight.DestinationID select c.CityName).FirstOrDefault();

                string infoClass = $"Flight Number {flight.FlightID} from {getCityOriginClass } to {getCityDestinationClass } - {flight.DepartureTime}";

                ViewBag.info = infoClass;

                ViewBag.seats = flight.FlightTotalSeats;

                List<Ticket> TicketsListClass = (from t in db.Tickets where t.FlightID == flight.FlightID select t).ToList();

                List<string> TicketSeatClass = (from ts in TicketsListClass select ts.Seat).ToList();

                ViewBag.tickets = TicketSeatClass;

                ViewBag.Type = new SelectList(db.TicketTypes, "TicketTypeID", "TicketTypeName");
                ViewBag.Class = new SelectList(db.TicketClasses, "TicketClassID", "TicketClassName");
                ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "FirstName");
                ViewBag.TicketPriceID = new SelectList(db.TicketPrices, "TicketPriceID", "TicketPriceName");

                ModelState.AddModelError("DOB", "The Date you introduced is not valid for a Child");
                return View();
            }

            if (tickettype == "Child" && dateCheck < checkDateChild)
            {
                Flight flightPostClass = (from f in db.Flights where f.FlightID == id select f).FirstOrDefault();

                string getCityOriginClass = (from c in db.Cities where c.CityID == flight.OriginID select c.CityName).FirstOrDefault();

                string getCityDestinationClass = (from c in db.Cities where c.CityID == flight.DestinationID select c.CityName).FirstOrDefault();

                string infoClass = $"Flight Number {flight.FlightID} from {getCityOriginClass } to {getCityDestinationClass } - {flight.DepartureTime}";

                ViewBag.info = infoClass;

                ViewBag.seats = flight.FlightTotalSeats;

                List<Ticket> TicketsListClass = (from t in db.Tickets where t.FlightID == flight.FlightID select t).ToList();

                List<string> TicketSeatClass = (from ts in TicketsListClass select ts.Seat).ToList();

                ViewBag.tickets = TicketSeatClass;

                ViewBag.Type = new SelectList(db.TicketTypes, "TicketTypeID", "TicketTypeName");
                ViewBag.Class = new SelectList(db.TicketClasses, "TicketClassID", "TicketClassName");
                ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "FirstName");
                ViewBag.TicketPriceID = new SelectList(db.TicketPrices, "TicketPriceID", "TicketPriceName");

                ModelState.AddModelError("DOB", "The Date you introduced is not valid for a Child");
                return View();
            }

            if (tickettype == "Adult" && dateCheck > checkDateChild)
            {
                Flight flightPostClass = (from f in db.Flights where f.FlightID == id select f).FirstOrDefault();

                string getCityOriginClass = (from c in db.Cities where c.CityID == flight.OriginID select c.CityName).FirstOrDefault();

                string getCityDestinationClass = (from c in db.Cities where c.CityID == flight.DestinationID select c.CityName).FirstOrDefault();

                string infoClass = $"Flight Number {flight.FlightID} from {getCityOriginClass } to {getCityDestinationClass } - {flight.DepartureTime}";

                ViewBag.info = infoClass;

                ViewBag.seats = flight.FlightTotalSeats;

                List<Ticket> TicketsListClass = (from t in db.Tickets where t.FlightID == flight.FlightID select t).ToList();

                List<string> TicketSeatClass = (from ts in TicketsListClass select ts.Seat).ToList();

                ViewBag.tickets = TicketSeatClass;

                ViewBag.Type = new SelectList(db.TicketTypes, "TicketTypeID", "TicketTypeName");
                ViewBag.Class = new SelectList(db.TicketClasses, "TicketClassID", "TicketClassName");
                ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "FirstName");
                ViewBag.TicketPriceID = new SelectList(db.TicketPrices, "TicketPriceID", "TicketPriceName");

                ModelState.AddModelError("DOB", "The Date you introduced is not valid for an Adult");
                return View();
            }

            ticket.TicketPriceID = (from tp in db.TicketPrices where tp.TicketClassID == ticketc && tp.TicketTypeID == tickett select tp.TicketPriceID).FirstOrDefault();

            if (ModelState.IsValid)
            {
                ticket.Class = ticketclass;
                ticket.Type = tickettype;
                ticket.FlightID = flight.FlightID;
                flight.FlightAvailableSeats = flight.FlightAvailableSeats - 1;
                db.Entry(flight).State = EntityState.Modified;
                db.Tickets.Add(ticket);
                db.SaveChanges();
                return RedirectToAction("Index", "Flights");
            }

            Flight flightPost = (from f in db.Flights where f.FlightID == id select f).FirstOrDefault();

            string getCityOrigin = (from c in db.Cities where c.CityID == flight.OriginID select c.CityName).FirstOrDefault();

            string getCityDestination = (from c in db.Cities where c.CityID == flight.DestinationID select c.CityName).FirstOrDefault();

            string info = $"Flight Number {flight.FlightID} from {getCityOrigin} to {getCityDestination} - {flight.DepartureTime}";

            ViewBag.info = info;

            ViewBag.seats = flight.FlightTotalSeats;

            List<Ticket> TicketsList = (from t in db.Tickets where t.FlightID == flight.FlightID select t).ToList();

            List<string> TicketSeat = (from ts in TicketsList select ts.Seat).ToList();

            ViewBag.tickets = TicketSeat;

            ViewBag.Type = new SelectList(db.TicketTypes, "TicketTypeID", "TicketTypeName");
            ViewBag.Class = new SelectList(db.TicketClasses, "TicketClassID", "TicketClassName");
            ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "FirstName");
            ViewBag.TicketPriceID = new SelectList(db.TicketPrices, "TicketPriceID", "TicketPriceName");
            return View();
        }

        //JSON RESULT
        [HttpPost]
        public ActionResult PostTicketPrice(int TicketClassID, int TicketTypeID)
        {
            var price = db.TicketPrices.Where(tp => tp.TicketClassID == TicketClassID && tp.TicketTypeID == TicketTypeID).Select(tp => tp).FirstOrDefault();

            double ticketprice = price.TicketPriceValue;

            return Json(ticketprice, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "ClientCreate")]
        // GET: Tickets/Create
        public ActionResult CreateClient(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("BadRequest", "Requests");
            }
            Flight flight = db.Flights.Find(id);
            if (flight == null)
            {
                return RedirectToAction("PageNotFound", "Requests");
            }

            string getCityOrigin = (from c in db.Cities where c.CityID == flight.OriginID select c.CityName).FirstOrDefault();

            string getCityDestination = (from c in db.Cities where c.CityID == flight.DestinationID select c.CityName).FirstOrDefault();

            string info = $"Flight Number {flight.FlightID} from {getCityOrigin} to {getCityDestination} - {flight.DepartureTime}";

            ViewBag.info = info;

            ViewBag.seats = flight.FlightTotalSeats;

            List<Ticket> TicketsList = (from t in db.Tickets where t.FlightID == flight.FlightID select t).ToList();

            List<string> TicketSeat = (from ts in TicketsList select ts.Seat).ToList();

            ViewBag.tickets = TicketSeat;

            var user = User.Identity.Name.ToString();

            int clients = (from c in db.Clients where c.Email == user select c.ClientID).FirstOrDefault();

            if (clients == 0)
            {
                return RedirectToAction("CreateClient", "Clients");
            }

            ViewBag.Type = new SelectList(db.TicketTypes, "TicketTypeID", "TicketTypeName");
            ViewBag.Class = new SelectList(db.TicketClasses, "TicketClassID", "TicketClassName");
            ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "FirstName");
            //ViewBag.FlightID = new SelectList(db.Flights, "FlightID", "FlightID");
            ViewBag.TicketPriceID = new SelectList(db.TicketPrices, "TicketPriceID", "TicketPriceName");
            return View();
        }

        // POST: Tickets/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateClient([Bind(Include = "TicketID,Name,DOB,Class,Type,Seat,TicketPriceID,ClientID")] Ticket ticket, int? id)
        {
            if (id == null)
            {
                return RedirectToAction("BadRequest", "Requests");
            }
            Flight flight = db.Flights.Find(id);
            if (flight == null)
            {
                return RedirectToAction("PageNotFound", "Requests");
            }
            var dateDOB = Request["DOB"];

            DateTime dateCheck;

            if (dateDOB == "")
            {
                Flight flightPostClass = (from f in db.Flights where f.FlightID == id select f).FirstOrDefault();

                string getCityOriginClass = (from c in db.Cities where c.CityID == flight.OriginID select c.CityName).FirstOrDefault();

                string getCityDestinationClass = (from c in db.Cities where c.CityID == flight.DestinationID select c.CityName).FirstOrDefault();

                string infoClass = $"Flight Number {flight.FlightID} from {getCityOriginClass } to {getCityDestinationClass } - {flight.DepartureTime}";

                ViewBag.info = infoClass;

                ViewBag.seats = flight.FlightTotalSeats;

                List<Ticket> TicketsListClass = (from t in db.Tickets where t.FlightID == flight.FlightID select t).ToList();

                List<string> TicketSeatClass = (from ts in TicketsListClass select ts.Seat).ToList();

                ViewBag.tickets = TicketSeatClass;

                ViewBag.Type = new SelectList(db.TicketTypes, "TicketTypeID", "TicketTypeName");
                ViewBag.Class = new SelectList(db.TicketClasses, "TicketClassID", "TicketClassName");
                ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "FirstName");
                ViewBag.TicketPriceID = new SelectList(db.TicketPrices, "TicketPriceID", "TicketPriceName");

                ModelState.AddModelError("DOB", "You have to choose a valid Date of Birth");
                return View();
            }
            else
            {
                dateCheck = DateTime.Parse(Request["DOB"]);

                if (dateCheck >= DateTime.Now.Date)
                {
                    Flight flightPostClass = (from f in db.Flights where f.FlightID == id select f).FirstOrDefault();

                    string getCityOriginClass = (from c in db.Cities where c.CityID == flight.OriginID select c.CityName).FirstOrDefault();

                    string getCityDestinationClass = (from c in db.Cities where c.CityID == flight.DestinationID select c.CityName).FirstOrDefault();

                    string infoClass = $"Flight Number {flight.FlightID} from {getCityOriginClass } to {getCityDestinationClass } - {flight.DepartureTime}";

                    ViewBag.info = infoClass;

                    ViewBag.seats = flight.FlightTotalSeats;

                    List<Ticket> TicketsListClass = (from t in db.Tickets where t.FlightID == flight.FlightID select t).ToList();

                    List<string> TicketSeatClass = (from ts in TicketsListClass select ts.Seat).ToList();

                    ViewBag.tickets = TicketSeatClass;

                    ViewBag.Type = new SelectList(db.TicketTypes, "TicketTypeID", "TicketTypeName");
                    ViewBag.Class = new SelectList(db.TicketClasses, "TicketClassID", "TicketClassName");
                    ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "FirstName");
                    ViewBag.TicketPriceID = new SelectList(db.TicketPrices, "TicketPriceID", "TicketPriceName");

                    ModelState.AddModelError("DOB", "You have to choose a valid Date of Birth");
                    return View();
                }
            }
            var classCheck = Request["Class"];

            if (string.IsNullOrEmpty(classCheck))
            {
                Flight flightPostClass = (from f in db.Flights where f.FlightID == id select f).FirstOrDefault();

                string getCityOriginClass = (from c in db.Cities where c.CityID == flight.OriginID select c.CityName).FirstOrDefault();

                string getCityDestinationClass = (from c in db.Cities where c.CityID == flight.DestinationID select c.CityName).FirstOrDefault();

                string infoClass = $"Flight Number {flight.FlightID} from {getCityOriginClass } to {getCityDestinationClass } - {flight.DepartureTime}";

                ViewBag.info = infoClass;

                ViewBag.seats = flight.FlightTotalSeats;

                List<Ticket> TicketsListClass = (from t in db.Tickets where t.FlightID == flight.FlightID select t).ToList();

                List<string> TicketSeatClass = (from ts in TicketsListClass select ts.Seat).ToList();

                ViewBag.tickets = TicketSeatClass;

                var userClass = User.Identity.Name.ToString();

                int clientsClass = (from c in db.Clients where c.Email == userClass select c.ClientID).FirstOrDefault();

                if (clientsClass == 0)
                {
                    return RedirectToAction("CreateClient", "Clients");
                }

                ViewBag.Type = new SelectList(db.TicketTypes, "TicketTypeID", "TicketTypeName");
                ViewBag.Class = new SelectList(db.TicketClasses, "TicketClassID", "TicketClassName");
                ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "FirstName");
                //ViewBag.FlightID = new SelectList(db.Flights, "FlightID", "FlightID");
                ViewBag.TicketPriceID = new SelectList(db.TicketPrices, "TicketPriceID", "TicketPriceName");

                ModelState.AddModelError("Class", "You have to choose a Class");
                return View();
            }

            var typeCheck = Request["Type"];

            if (string.IsNullOrEmpty(typeCheck))
            {
                Flight flightPostType = (from f in db.Flights where f.FlightID == id select f).FirstOrDefault();

                string getCityOriginType = (from c in db.Cities where c.CityID == flight.OriginID select c.CityName).FirstOrDefault();

                string getCityDestinationType = (from c in db.Cities where c.CityID == flight.DestinationID select c.CityName).FirstOrDefault();

                string infoType = $"Flight Number {flight.FlightID} from {getCityOriginType } to {getCityDestinationType } - {flight.DepartureTime}";

                ViewBag.info = infoType;

                ViewBag.seats = flight.FlightTotalSeats;

                List<Ticket> TicketsListType = (from t in db.Tickets where t.FlightID == flight.FlightID select t).ToList();

                List<string> TicketSeatType = (from ts in TicketsListType select ts.Seat).ToList();

                ViewBag.tickets = TicketSeatType;

                var userType = User.Identity.Name.ToString();

                int clientsType = (from c in db.Clients where c.Email == userType select c.ClientID).FirstOrDefault();

                if (clientsType == 0)
                {
                    return RedirectToAction("CreateClient", "Clients");
                }

                ViewBag.Type = new SelectList(db.TicketTypes, "TicketTypeID", "TicketTypeName");
                ViewBag.Class = new SelectList(db.TicketClasses, "TicketClassID", "TicketClassName");
                ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "FirstName");
                //ViewBag.FlightID = new SelectList(db.Flights, "FlightID", "FlightID");
                ViewBag.TicketPriceID = new SelectList(db.TicketPrices, "TicketPriceID", "TicketPriceName");

                ModelState.AddModelError("Class", "You have to choose a Class");
                return View();
            }

            int idt = (from c in db.Clients where c.Email == User.Identity.Name select c.ClientID).FirstOrDefault();

            int ticketc = int.Parse(Request["Class"]);

            string ticketclass = (from c in db.TicketClasses where c.TicketClassID == ticketc select c.TicketClassName).FirstOrDefault();

            int tickett = int.Parse(Request["Type"]);

            string tickettype = (from c in db.TicketTypes where c.TicketTypeID == tickett select c.TicketTypeName).FirstOrDefault();

            DateTime checkDate = DateTime.Today.Date.AddYears(-2);

            if (tickettype == "Infant" && dateCheck < checkDate)
            {
                Flight flightPostClass = (from f in db.Flights where f.FlightID == id select f).FirstOrDefault();

                string getCityOriginClass = (from c in db.Cities where c.CityID == flight.OriginID select c.CityName).FirstOrDefault();

                string getCityDestinationClass = (from c in db.Cities where c.CityID == flight.DestinationID select c.CityName).FirstOrDefault();

                string infoClass = $"Flight Number {flight.FlightID} from {getCityOriginClass } to {getCityDestinationClass } - {flight.DepartureTime}";

                ViewBag.info = infoClass;

                ViewBag.seats = flight.FlightTotalSeats;

                List<Ticket> TicketsListClass = (from t in db.Tickets where t.FlightID == flight.FlightID select t).ToList();

                List<string> TicketSeatClass = (from ts in TicketsListClass select ts.Seat).ToList();

                ViewBag.tickets = TicketSeatClass;

                ViewBag.Type = new SelectList(db.TicketTypes, "TicketTypeID", "TicketTypeName");
                ViewBag.Class = new SelectList(db.TicketClasses, "TicketClassID", "TicketClassName");
                ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "FirstName");
                ViewBag.TicketPriceID = new SelectList(db.TicketPrices, "TicketPriceID", "TicketPriceName");

                ModelState.AddModelError("DOB", "The Date you introduced is not valid for an Infant");
                return View();
            }

            DateTime checkDateChild = DateTime.Today.Date.AddYears(-18);

            if (tickettype == "Child" && dateCheck > checkDate)
            {
                Flight flightPostClass = (from f in db.Flights where f.FlightID == id select f).FirstOrDefault();

                string getCityOriginClass = (from c in db.Cities where c.CityID == flight.OriginID select c.CityName).FirstOrDefault();

                string getCityDestinationClass = (from c in db.Cities where c.CityID == flight.DestinationID select c.CityName).FirstOrDefault();

                string infoClass = $"Flight Number {flight.FlightID} from {getCityOriginClass } to {getCityDestinationClass } - {flight.DepartureTime}";

                ViewBag.info = infoClass;

                ViewBag.seats = flight.FlightTotalSeats;

                List<Ticket> TicketsListClass = (from t in db.Tickets where t.FlightID == flight.FlightID select t).ToList();

                List<string> TicketSeatClass = (from ts in TicketsListClass select ts.Seat).ToList();

                ViewBag.tickets = TicketSeatClass;

                ViewBag.Type = new SelectList(db.TicketTypes, "TicketTypeID", "TicketTypeName");
                ViewBag.Class = new SelectList(db.TicketClasses, "TicketClassID", "TicketClassName");
                ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "FirstName");
                ViewBag.TicketPriceID = new SelectList(db.TicketPrices, "TicketPriceID", "TicketPriceName");

                ModelState.AddModelError("DOB", "The Date you introduced is not valid for a Child");
                return View();
            }

            if (tickettype == "Child" && dateCheck < checkDateChild)
            {
                Flight flightPostClass = (from f in db.Flights where f.FlightID == id select f).FirstOrDefault();

                string getCityOriginClass = (from c in db.Cities where c.CityID == flight.OriginID select c.CityName).FirstOrDefault();

                string getCityDestinationClass = (from c in db.Cities where c.CityID == flight.DestinationID select c.CityName).FirstOrDefault();

                string infoClass = $"Flight Number {flight.FlightID} from {getCityOriginClass } to {getCityDestinationClass } - {flight.DepartureTime}";

                ViewBag.info = infoClass;

                ViewBag.seats = flight.FlightTotalSeats;

                List<Ticket> TicketsListClass = (from t in db.Tickets where t.FlightID == flight.FlightID select t).ToList();

                List<string> TicketSeatClass = (from ts in TicketsListClass select ts.Seat).ToList();

                ViewBag.tickets = TicketSeatClass;

                ViewBag.Type = new SelectList(db.TicketTypes, "TicketTypeID", "TicketTypeName");
                ViewBag.Class = new SelectList(db.TicketClasses, "TicketClassID", "TicketClassName");
                ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "FirstName");
                ViewBag.TicketPriceID = new SelectList(db.TicketPrices, "TicketPriceID", "TicketPriceName");

                ModelState.AddModelError("DOB", "The Date you introduced is not valid for a Child");
                return View();
            }

            if (tickettype == "Adult" && dateCheck > checkDateChild)
            {
                Flight flightPostClass = (from f in db.Flights where f.FlightID == id select f).FirstOrDefault();

                string getCityOriginClass = (from c in db.Cities where c.CityID == flight.OriginID select c.CityName).FirstOrDefault();

                string getCityDestinationClass = (from c in db.Cities where c.CityID == flight.DestinationID select c.CityName).FirstOrDefault();

                string infoClass = $"Flight Number {flight.FlightID} from {getCityOriginClass } to {getCityDestinationClass } - {flight.DepartureTime}";

                ViewBag.info = infoClass;

                ViewBag.seats = flight.FlightTotalSeats;

                List<Ticket> TicketsListClass = (from t in db.Tickets where t.FlightID == flight.FlightID select t).ToList();

                List<string> TicketSeatClass = (from ts in TicketsListClass select ts.Seat).ToList();

                ViewBag.tickets = TicketSeatClass;

                ViewBag.Type = new SelectList(db.TicketTypes, "TicketTypeID", "TicketTypeName");
                ViewBag.Class = new SelectList(db.TicketClasses, "TicketClassID", "TicketClassName");
                ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "FirstName");
                ViewBag.TicketPriceID = new SelectList(db.TicketPrices, "TicketPriceID", "TicketPriceName");

                ModelState.AddModelError("DOB", "The Date you introduced is not valid for an Adult");
                return View();
            }

            ticket.TicketPriceID = (from tp in db.TicketPrices where tp.TicketClassID == ticketc && tp.TicketTypeID == tickett select tp.TicketPriceID).FirstOrDefault();

            if (ModelState.IsValid)
            {
                ticket.Class = ticketclass;
                ticket.Type = tickettype;
                ticket.ClientID = idt;
                ticket.FlightID = flight.FlightID;
                flight.FlightAvailableSeats = flight.FlightAvailableSeats - 1;
                db.Entry(flight).State = EntityState.Modified;
                db.Tickets.Add(ticket);
                db.SaveChanges();
                return RedirectToAction("IndexClient");
            }

            Flight flightPost = (from f in db.Flights where f.FlightID == id select f).FirstOrDefault();

            string getCityOrigin = (from c in db.Cities where c.CityID == flight.OriginID select c.CityName).FirstOrDefault();

            string getCityDestination = (from c in db.Cities where c.CityID == flight.DestinationID select c.CityName).FirstOrDefault();

            string info = $"Flight Number {flight.FlightID} from {getCityOrigin} to {getCityDestination} - {flight.DepartureTime}";

            ViewBag.info = info;

            ViewBag.seats = flight.FlightTotalSeats;

            List<Ticket> TicketsList = (from t in db.Tickets where t.FlightID == flight.FlightID select t).ToList();

            List<string> TicketSeat = (from ts in TicketsList select ts.Seat).ToList();

            ViewBag.tickets = TicketSeat;

            var user = User.Identity.Name.ToString();

            int clients = (from c in db.Clients where c.Email == user select c.ClientID).FirstOrDefault();

            if (clients == 0)
            {
                return RedirectToAction("CreateClient", "Clients");
            }

            ViewBag.Type = new SelectList(db.TicketTypes, "TicketTypeID", "TicketTypeName");
            ViewBag.Class = new SelectList(db.TicketClasses, "TicketClassID", "TicketClassName");
            ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "FirstName");
            //ViewBag.FlightID = new SelectList(db.Flights, "FlightID", "FlightID");
            ViewBag.TicketPriceID = new SelectList(db.TicketPrices, "TicketPriceID", "TicketPriceName");
            return View();
        }

        [Authorize(Roles = "EmployeeCreate")]
        // GET: Tickets/Create
        public ActionResult CreateEmployee(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("BadRequest", "Requests");
            }
            Flight flight = db.Flights.Find(id);
            if (flight == null)
            {
                return RedirectToAction("PageNotFound", "Requests");
            }

            string getCityOrigin = (from c in db.Cities where c.CityID == flight.OriginID select c.CityName).FirstOrDefault();

            string getCityDestination = (from c in db.Cities where c.CityID == flight.DestinationID select c.CityName).FirstOrDefault();

            string info = $"Flight Number {flight.FlightID} from {getCityOrigin} to {getCityDestination} - {flight.DepartureTime}";

            ViewBag.info = info;

            ViewBag.seats = flight.FlightTotalSeats;

            List<Ticket> TicketsList = (from t in db.Tickets where t.FlightID == flight.FlightID select t).ToList();

            List<string> TicketSeat = (from ts in TicketsList select ts.Seat).ToList();

            ViewBag.tickets = TicketSeat;

            ViewBag.Type = new SelectList(db.TicketTypes, "TicketTypeID", "TicketTypeName");
            ViewBag.Class = new SelectList(db.TicketClasses, "TicketClassID", "TicketClassName");
            ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "FirstName");
            ViewBag.TicketPriceID = new SelectList(db.TicketPrices, "TicketPriceID", "TicketPriceName");
            return View();
        }

        // POST: Tickets/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateEmployee([Bind(Include = "TicketID,Name,DOB,Class,Type,Seat,TicketPriceID,ClientID")] Ticket ticket, int? id)
        {
            if (id == null)
            {
                return RedirectToAction("BadRequest", "Requests");
            }
            Flight flight = db.Flights.Find(id);
            if (flight == null)
            {
                return RedirectToAction("PageNotFound", "Requests");
            }
            var dateDOB = Request["DOB"];

            DateTime dateCheck;

            if (dateDOB == "")
            {
                Flight flightPostClass = (from f in db.Flights where f.FlightID == id select f).FirstOrDefault();

                string getCityOriginClass = (from c in db.Cities where c.CityID == flight.OriginID select c.CityName).FirstOrDefault();

                string getCityDestinationClass = (from c in db.Cities where c.CityID == flight.DestinationID select c.CityName).FirstOrDefault();

                string infoClass = $"Flight Number {flight.FlightID} from {getCityOriginClass } to {getCityDestinationClass } - {flight.DepartureTime}";

                ViewBag.info = infoClass;

                ViewBag.seats = flight.FlightTotalSeats;

                List<Ticket> TicketsListClass = (from t in db.Tickets where t.FlightID == flight.FlightID select t).ToList();

                List<string> TicketSeatClass = (from ts in TicketsListClass select ts.Seat).ToList();

                ViewBag.tickets = TicketSeatClass;

                ViewBag.Type = new SelectList(db.TicketTypes, "TicketTypeID", "TicketTypeName");
                ViewBag.Class = new SelectList(db.TicketClasses, "TicketClassID", "TicketClassName");
                ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "FirstName");
                ViewBag.TicketPriceID = new SelectList(db.TicketPrices, "TicketPriceID", "TicketPriceName");

                ModelState.AddModelError("DOB", "You have to choose a valid Date of Birth");
                return View();
            }
            else
            {
                dateCheck = DateTime.Parse(Request["DOB"]);

                if (dateCheck >= DateTime.Now.Date)
                {
                    Flight flightPostClass = (from f in db.Flights where f.FlightID == id select f).FirstOrDefault();

                    string getCityOriginClass = (from c in db.Cities where c.CityID == flight.OriginID select c.CityName).FirstOrDefault();

                    string getCityDestinationClass = (from c in db.Cities where c.CityID == flight.DestinationID select c.CityName).FirstOrDefault();

                    string infoClass = $"Flight Number {flight.FlightID} from {getCityOriginClass } to {getCityDestinationClass } - {flight.DepartureTime}";

                    ViewBag.info = infoClass;

                    ViewBag.seats = flight.FlightTotalSeats;

                    List<Ticket> TicketsListClass = (from t in db.Tickets where t.FlightID == flight.FlightID select t).ToList();

                    List<string> TicketSeatClass = (from ts in TicketsListClass select ts.Seat).ToList();

                    ViewBag.tickets = TicketSeatClass;

                    ViewBag.Type = new SelectList(db.TicketTypes, "TicketTypeID", "TicketTypeName");
                    ViewBag.Class = new SelectList(db.TicketClasses, "TicketClassID", "TicketClassName");
                    ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "FirstName");
                    ViewBag.TicketPriceID = new SelectList(db.TicketPrices, "TicketPriceID", "TicketPriceName");

                    ModelState.AddModelError("DOB", "You have to choose a valid Date of Birth");
                    return View();
                }
            }
            var classCheck = Request["Class"];

            if (string.IsNullOrEmpty(classCheck))
            {
                Flight flightPostClass = (from f in db.Flights where f.FlightID == id select f).FirstOrDefault();

                string getCityOriginClass = (from c in db.Cities where c.CityID == flight.OriginID select c.CityName).FirstOrDefault();

                string getCityDestinationClass = (from c in db.Cities where c.CityID == flight.DestinationID select c.CityName).FirstOrDefault();

                string infoClass = $"Flight Number {flight.FlightID} from {getCityOriginClass } to {getCityDestinationClass } - {flight.DepartureTime}";

                ViewBag.info = infoClass;

                ViewBag.seats = flight.FlightTotalSeats;

                List<Ticket> TicketsListClass = (from t in db.Tickets where t.FlightID == flight.FlightID select t).ToList();

                List<string> TicketSeatClass = (from ts in TicketsListClass select ts.Seat).ToList();

                ViewBag.tickets = TicketSeatClass;

                ViewBag.Type = new SelectList(db.TicketTypes, "TicketTypeID", "TicketTypeName");
                ViewBag.Class = new SelectList(db.TicketClasses, "TicketClassID", "TicketClassName");
                ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "FirstName");
                ViewBag.TicketPriceID = new SelectList(db.TicketPrices, "TicketPriceID", "TicketPriceName");

                ModelState.AddModelError("Class", "You have to choose a Class");
                return View();
            }

            var typeCheck = Request["Type"];

            if (string.IsNullOrEmpty(typeCheck))
            {
                Flight flightPostType = (from f in db.Flights where f.FlightID == id select f).FirstOrDefault();

                string getCityOriginType = (from c in db.Cities where c.CityID == flightPostType.OriginID select c.CityName).FirstOrDefault();

                string getCityDestinationType = (from c in db.Cities where c.CityID == flightPostType.DestinationID select c.CityName).FirstOrDefault();

                string infoType = $"Flight Number {flight.FlightID} from {getCityOriginType } to {getCityDestinationType } - {flightPostType.DepartureTime}";

                ViewBag.info = infoType;

                ViewBag.seats = flightPostType.FlightTotalSeats;

                List<Ticket> TicketsListType = (from t in db.Tickets where t.FlightID == flightPostType.FlightID select t).ToList();

                List<string> TicketSeatType = (from ts in TicketsListType select ts.Seat).ToList();

                ViewBag.tickets = TicketSeatType;

                ViewBag.Type = new SelectList(db.TicketTypes, "TicketTypeID", "TicketTypeName");
                ViewBag.Class = new SelectList(db.TicketClasses, "TicketClassID", "TicketClassName");
                ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "FirstName");
                ViewBag.TicketPriceID = new SelectList(db.TicketPrices, "TicketPriceID", "TicketPriceName");

                ModelState.AddModelError("Type", "You have to choose a Type");
                return View();
            }

            int ticketc = int.Parse(Request["Class"]);

            string ticketclass = (from c in db.TicketClasses where c.TicketClassID == ticketc select c.TicketClassName).FirstOrDefault();

            int tickett = int.Parse(Request["Type"]);

            string tickettype = (from c in db.TicketTypes where c.TicketTypeID == tickett select c.TicketTypeName).FirstOrDefault();

            DateTime checkDate = DateTime.Today.Date.AddYears(-2);

            if (tickettype == "Infant" && dateCheck < checkDate)
            {
                Flight flightPostClass = (from f in db.Flights where f.FlightID == id select f).FirstOrDefault();

                string getCityOriginClass = (from c in db.Cities where c.CityID == flight.OriginID select c.CityName).FirstOrDefault();

                string getCityDestinationClass = (from c in db.Cities where c.CityID == flight.DestinationID select c.CityName).FirstOrDefault();

                string infoClass = $"Flight Number {flight.FlightID} from {getCityOriginClass } to {getCityDestinationClass } - {flight.DepartureTime}";

                ViewBag.info = infoClass;

                ViewBag.seats = flight.FlightTotalSeats;

                List<Ticket> TicketsListClass = (from t in db.Tickets where t.FlightID == flight.FlightID select t).ToList();

                List<string> TicketSeatClass = (from ts in TicketsListClass select ts.Seat).ToList();

                ViewBag.tickets = TicketSeatClass;

                ViewBag.Type = new SelectList(db.TicketTypes, "TicketTypeID", "TicketTypeName");
                ViewBag.Class = new SelectList(db.TicketClasses, "TicketClassID", "TicketClassName");
                ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "FirstName");
                ViewBag.TicketPriceID = new SelectList(db.TicketPrices, "TicketPriceID", "TicketPriceName");

                ModelState.AddModelError("DOB", "The Date you introduced is not valid for an Infant");
                return View();
            }

            DateTime checkDateChild = DateTime.Today.Date.AddYears(-18);

            if (tickettype == "Child" && dateCheck > checkDate)
            {
                Flight flightPostClass = (from f in db.Flights where f.FlightID == id select f).FirstOrDefault();

                string getCityOriginClass = (from c in db.Cities where c.CityID == flight.OriginID select c.CityName).FirstOrDefault();

                string getCityDestinationClass = (from c in db.Cities where c.CityID == flight.DestinationID select c.CityName).FirstOrDefault();

                string infoClass = $"Flight Number {flight.FlightID} from {getCityOriginClass } to {getCityDestinationClass } - {flight.DepartureTime}";

                ViewBag.info = infoClass;

                ViewBag.seats = flight.FlightTotalSeats;

                List<Ticket> TicketsListClass = (from t in db.Tickets where t.FlightID == flight.FlightID select t).ToList();

                List<string> TicketSeatClass = (from ts in TicketsListClass select ts.Seat).ToList();

                ViewBag.tickets = TicketSeatClass;

                ViewBag.Type = new SelectList(db.TicketTypes, "TicketTypeID", "TicketTypeName");
                ViewBag.Class = new SelectList(db.TicketClasses, "TicketClassID", "TicketClassName");
                ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "FirstName");
                ViewBag.TicketPriceID = new SelectList(db.TicketPrices, "TicketPriceID", "TicketPriceName");

                ModelState.AddModelError("DOB", "The Date you introduced is not valid for a Child");
                return View();
            }

            if (tickettype == "Child" && dateCheck < checkDateChild)
            {
                Flight flightPostClass = (from f in db.Flights where f.FlightID == id select f).FirstOrDefault();

                string getCityOriginClass = (from c in db.Cities where c.CityID == flight.OriginID select c.CityName).FirstOrDefault();

                string getCityDestinationClass = (from c in db.Cities where c.CityID == flight.DestinationID select c.CityName).FirstOrDefault();

                string infoClass = $"Flight Number {flight.FlightID} from {getCityOriginClass } to {getCityDestinationClass } - {flight.DepartureTime}";

                ViewBag.info = infoClass;

                ViewBag.seats = flight.FlightTotalSeats;

                List<Ticket> TicketsListClass = (from t in db.Tickets where t.FlightID == flight.FlightID select t).ToList();

                List<string> TicketSeatClass = (from ts in TicketsListClass select ts.Seat).ToList();

                ViewBag.tickets = TicketSeatClass;

                ViewBag.Type = new SelectList(db.TicketTypes, "TicketTypeID", "TicketTypeName");
                ViewBag.Class = new SelectList(db.TicketClasses, "TicketClassID", "TicketClassName");
                ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "FirstName");
                ViewBag.TicketPriceID = new SelectList(db.TicketPrices, "TicketPriceID", "TicketPriceName");

                ModelState.AddModelError("DOB", "The Date you introduced is not valid for a Child");
                return View();
            }

            if (tickettype == "Adult" && dateCheck > checkDateChild)
            {
                Flight flightPostClass = (from f in db.Flights where f.FlightID == id select f).FirstOrDefault();

                string getCityOriginClass = (from c in db.Cities where c.CityID == flight.OriginID select c.CityName).FirstOrDefault();

                string getCityDestinationClass = (from c in db.Cities where c.CityID == flight.DestinationID select c.CityName).FirstOrDefault();

                string infoClass = $"Flight Number {flight.FlightID} from {getCityOriginClass } to {getCityDestinationClass } - {flight.DepartureTime}";

                ViewBag.info = infoClass;

                ViewBag.seats = flight.FlightTotalSeats;

                List<Ticket> TicketsListClass = (from t in db.Tickets where t.FlightID == flight.FlightID select t).ToList();

                List<string> TicketSeatClass = (from ts in TicketsListClass select ts.Seat).ToList();

                ViewBag.tickets = TicketSeatClass;

                ViewBag.Type = new SelectList(db.TicketTypes, "TicketTypeID", "TicketTypeName");
                ViewBag.Class = new SelectList(db.TicketClasses, "TicketClassID", "TicketClassName");
                ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "FirstName");
                ViewBag.TicketPriceID = new SelectList(db.TicketPrices, "TicketPriceID", "TicketPriceName");

                ModelState.AddModelError("DOB", "The Date you introduced is not valid for an Adult");
                return View();
            }

            ticket.TicketPriceID = (from tp in db.TicketPrices where tp.TicketClassID == ticketc && tp.TicketTypeID == tickett select tp.TicketPriceID).FirstOrDefault();

            if (ModelState.IsValid)
            {
                ticket.Class = ticketclass;
                ticket.Type = tickettype;
                ticket.FlightID = flight.FlightID;
                flight.FlightAvailableSeats = flight.FlightAvailableSeats - 1;
                db.Entry(flight).State = EntityState.Modified;
                db.Tickets.Add(ticket);
                db.SaveChanges();
                return RedirectToAction("IndexEmployee", "Flights");
            }

            Flight flightPost = (from f in db.Flights where f.FlightID == id select f).FirstOrDefault();

            string getCityOrigin = (from c in db.Cities where c.CityID == flight.OriginID select c.CityName).FirstOrDefault();

            string getCityDestination = (from c in db.Cities where c.CityID == flight.DestinationID select c.CityName).FirstOrDefault();

            string info = $"Flight Number {flight.FlightID} from {getCityOrigin} to {getCityDestination} - {flight.DepartureTime}";

            ViewBag.info = info;

            ViewBag.seats = flight.FlightTotalSeats;

            List<Ticket> TicketsList = (from t in db.Tickets where t.FlightID == flight.FlightID select t).ToList();

            List<string> TicketSeat = (from ts in TicketsList select ts.Seat).ToList();

            ViewBag.tickets = TicketSeat;

            ViewBag.Type = new SelectList(db.TicketTypes, "TicketTypeID", "TicketTypeName");
            ViewBag.Class = new SelectList(db.TicketClasses, "TicketClassID", "TicketClassName");
            ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "FirstName");
            ViewBag.TicketPriceID = new SelectList(db.TicketPrices, "TicketPriceID", "TicketPriceName");
            return View();
        }

        [Authorize(Roles = "Edit")]
        // GET: Tickets/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("BadRequest", "Requests");
            }
            Ticket ticket = db.Tickets.Find(id);
            if (ticket == null)
            {
                return RedirectToAction("PageNotFound", "Requests");
            }

            Flight flight = (from f in db.Flights where f.FlightID == ticket.FlightID select f).FirstOrDefault();

            string getCityOrigin = (from c in db.Cities where c.CityID == flight.OriginID select c.CityName).FirstOrDefault();

            string getCityDestination = (from c in db.Cities where c.CityID == flight.DestinationID select c.CityName).FirstOrDefault();

            string info = $"Flight Number {flight.FlightID} from {getCityOrigin} to {getCityDestination} - {flight.DepartureTime}";

            ViewBag.info = info;

            ViewBag.seats = flight.FlightTotalSeats;

            List<Ticket> TicketsList = (from t in db.Tickets where t.FlightID == flight.FlightID select t).ToList();

            List<string> TicketSeat = (from ts in TicketsList select ts.Seat).ToList();

            ViewBag.tickets = TicketSeat;

            ViewBag.Type = new SelectList(db.TicketTypes, "TicketTypeID", "TicketTypeName");
            ViewBag.Class = new SelectList(db.TicketClasses, "TicketClassID", "TicketClassName");
            ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "FirstName");
            ViewBag.TicketPriceID = new SelectList(db.TicketPrices, "TicketPriceID", "TicketPriceValue");
            return View(ticket);
        }

        // POST: Tickets/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "TicketID,Name,DOB,Class,Type,FlightID,Seat,TicketPriceID,ClientID")] Ticket ticket, int? id)
        {
            if (id == null)
            {
                return RedirectToAction("BadRequest", "Requests");
            }
            Ticket ticketCheck = db.Tickets.Find(id);
            if (ticket == null)
            {
                return RedirectToAction("PageNotFound", "Requests");
            }

            Flight flight = (from f in db.Flights where f.FlightID == ticketCheck.FlightID select f).FirstOrDefault();
                
            var dateDOB = Request["DOB"];

            DateTime dateCheck;

            if (dateDOB == "")
            {
                Flight flightPostClass = (from f in db.Flights where f.FlightID == id select f).FirstOrDefault();

                string getCityOriginClass = (from c in db.Cities where c.CityID == flight.OriginID select c.CityName).FirstOrDefault();

                string getCityDestinationClass = (from c in db.Cities where c.CityID == flight.DestinationID select c.CityName).FirstOrDefault();

                string infoClass = $"Flight Number {flight.FlightID} from {getCityOriginClass } to {getCityDestinationClass } - {flight.DepartureTime}";

                ViewBag.info = infoClass;

                ViewBag.seats = flight.FlightTotalSeats;

                List<Ticket> TicketsListClass = (from t in db.Tickets where t.FlightID == flight.FlightID select t).ToList();

                List<string> TicketSeatClass = (from ts in TicketsListClass select ts.Seat).ToList();

                ViewBag.tickets = TicketSeatClass;

                ViewBag.Type = new SelectList(db.TicketTypes, "TicketTypeID", "TicketTypeName");
                ViewBag.Class = new SelectList(db.TicketClasses, "TicketClassID", "TicketClassName");
                ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "FirstName");
                ViewBag.TicketPriceID = new SelectList(db.TicketPrices, "TicketPriceID", "TicketPriceName");

                ModelState.AddModelError("DOB", "You have to choose a valid Date of Birth");
                return View();
            }
            else
            {
                dateCheck = DateTime.Parse(Request["DOB"]);

                if (dateCheck >= DateTime.Now.Date)
                {
                    Flight flightPostClass = (from f in db.Flights where f.FlightID == id select f).FirstOrDefault();

                    string getCityOriginClass = (from c in db.Cities where c.CityID == flight.OriginID select c.CityName).FirstOrDefault();

                    string getCityDestinationClass = (from c in db.Cities where c.CityID == flight.DestinationID select c.CityName).FirstOrDefault();

                    string infoClass = $"Flight Number {flight.FlightID} from {getCityOriginClass } to {getCityDestinationClass } - {flight.DepartureTime}";

                    ViewBag.info = infoClass;

                    ViewBag.seats = flight.FlightTotalSeats;

                    List<Ticket> TicketsListClass = (from t in db.Tickets where t.FlightID == flight.FlightID select t).ToList();

                    List<string> TicketSeatClass = (from ts in TicketsListClass select ts.Seat).ToList();

                    ViewBag.tickets = TicketSeatClass;

                    ViewBag.Type = new SelectList(db.TicketTypes, "TicketTypeID", "TicketTypeName");
                    ViewBag.Class = new SelectList(db.TicketClasses, "TicketClassID", "TicketClassName");
                    ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "FirstName");
                    ViewBag.TicketPriceID = new SelectList(db.TicketPrices, "TicketPriceID", "TicketPriceName");

                    ModelState.AddModelError("DOB", "You have to choose a valid Date of Birth");
                    return View();
                }
            }
            var classCheck = Request["Class"];

            if (string.IsNullOrEmpty(classCheck))
            {
                Flight flightPostClass = (from f in db.Flights where f.FlightID == ticketCheck.FlightID select f).FirstOrDefault();

                string getCityOriginClass = (from c in db.Cities where c.CityID == flightPostClass.OriginID select c.CityName).FirstOrDefault();

                string getCityDestinationClass = (from c in db.Cities where c.CityID == flightPostClass.DestinationID select c.CityName).FirstOrDefault();

                string infoClass = $"Flight Number {flightPostClass.FlightID} from {getCityOriginClass } to {getCityDestinationClass } - {flightPostClass.DepartureTime}";

                ViewBag.info = infoClass;

                ViewBag.seats = flightPostClass.FlightTotalSeats;

                List<Ticket> TicketsListClass = (from t in db.Tickets where t.FlightID == flightPostClass.FlightID select t).ToList();

                List<string> TicketSeatClass = (from ts in TicketsListClass select ts.Seat).ToList();

                ViewBag.tickets = TicketSeatClass;

                ViewBag.Type = new SelectList(db.TicketTypes, "TicketTypeID", "TicketTypeName");
                ViewBag.Class = new SelectList(db.TicketClasses, "TicketClassID", "TicketClassName");
                ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "FirstName");
                ViewBag.TicketPriceID = new SelectList(db.TicketPrices, "TicketPriceID", "TicketPriceName");

                ModelState.AddModelError("Class", "You have to choose a Class");
                return View();
            }

            var typeCheck = Request["Type"];

            if (string.IsNullOrEmpty(typeCheck))
            {
                Flight flightPostType = (from f in db.Flights where f.FlightID == ticketCheck.FlightID select f).FirstOrDefault();

                string getCityOriginType = (from c in db.Cities where c.CityID == flightPostType.OriginID select c.CityName).FirstOrDefault();

                string getCityDestinationType = (from c in db.Cities where c.CityID == flightPostType.DestinationID select c.CityName).FirstOrDefault();

                string infoType = $"Flight Number {flightPostType.FlightID} from {getCityOriginType } to {getCityDestinationType } - {flightPostType.DepartureTime}";

                ViewBag.info = infoType;

                ViewBag.seats = flightPostType.FlightTotalSeats;

                List<Ticket> TicketsListType = (from t in db.Tickets where t.FlightID == flightPostType.FlightID select t).ToList();

                List<string> TicketSeatType = (from ts in TicketsListType select ts.Seat).ToList();

                ViewBag.tickets = TicketSeatType;

                ViewBag.Type = new SelectList(db.TicketTypes, "TicketTypeID", "TicketTypeName");
                ViewBag.Class = new SelectList(db.TicketClasses, "TicketClassID", "TicketClassName");
                ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "FirstName");
                ViewBag.TicketPriceID = new SelectList(db.TicketPrices, "TicketPriceID", "TicketPriceName");

                ModelState.AddModelError("Type", "You have to choose a Type");
                return View();
            }

            var name = Request["Name"];

            var dobCheck = Request["DOB"];

            if (string.IsNullOrEmpty(dobCheck))
            {
                Flight flightPostDOB = (from f in db.Flights where f.FlightID == ticketCheck.FlightID select f).FirstOrDefault();

                string getCityOriginDOB = (from c in db.Cities where c.CityID == flightPostDOB.OriginID select c.CityName).FirstOrDefault();

                string getCityDestinationDOB = (from c in db.Cities where c.CityID == flightPostDOB.DestinationID select c.CityName).FirstOrDefault();

                string infoDOB = $"Flight Number {flightPostDOB.FlightID} from {getCityOriginDOB} to {getCityDestinationDOB} - {flightPostDOB.DepartureTime}";

                ViewBag.info = infoDOB;

                ViewBag.seats = flightPostDOB.FlightTotalSeats;

                List<Ticket> TicketsListDOB = (from t in db.Tickets where t.FlightID == flightPostDOB.FlightID select t).ToList();

                List<string> TicketSeatDOB = (from ts in TicketsListDOB select ts.Seat).ToList();

                ViewBag.tickets = TicketSeatDOB;

                ViewBag.Type = new SelectList(db.TicketTypes, "TicketTypeID", "TicketTypeName");
                ViewBag.Class = new SelectList(db.TicketClasses, "TicketClassID", "TicketClassName");
                ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "FirstName");
                ViewBag.TicketPriceID = new SelectList(db.TicketPrices, "TicketPriceID", "TicketPriceName");

                ModelState.AddModelError("DOB", "You have to choose a Date of Birth");
                return View();
            }

            var dob = DateTime.Parse(Request["DOB"]);

            var seat = Request["Seat"];

            var ticketID = (from f in db.Tickets where f.TicketID == id select f).FirstOrDefault();

            int ticketc = int.Parse(Request["Class"]);

            string ticketclass = (from c in db.TicketClasses where c.TicketClassID == ticketc select c.TicketClassName).FirstOrDefault();

            int tickett = int.Parse(Request["Type"]);

            string tickettype = (from c in db.TicketTypes where c.TicketTypeID == tickett select c.TicketTypeName).FirstOrDefault();

            DateTime checkDate = DateTime.Today.Date.AddYears(-2);

            if (tickettype == "Infant" && dateCheck < checkDate)
            {
                Flight flightPostClass = (from f in db.Flights where f.FlightID == id select f).FirstOrDefault();

                string getCityOriginClass = (from c in db.Cities where c.CityID == flight.OriginID select c.CityName).FirstOrDefault();

                string getCityDestinationClass = (from c in db.Cities where c.CityID == flight.DestinationID select c.CityName).FirstOrDefault();

                string infoClass = $"Flight Number {flight.FlightID} from {getCityOriginClass } to {getCityDestinationClass } - {flight.DepartureTime}";

                ViewBag.info = infoClass;

                ViewBag.seats = flight.FlightTotalSeats;

                List<Ticket> TicketsListClass = (from t in db.Tickets where t.FlightID == flight.FlightID select t).ToList();

                List<string> TicketSeatClass = (from ts in TicketsListClass select ts.Seat).ToList();

                ViewBag.tickets = TicketSeatClass;

                ViewBag.Type = new SelectList(db.TicketTypes, "TicketTypeID", "TicketTypeName");
                ViewBag.Class = new SelectList(db.TicketClasses, "TicketClassID", "TicketClassName");
                ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "FirstName");
                ViewBag.TicketPriceID = new SelectList(db.TicketPrices, "TicketPriceID", "TicketPriceName");

                ModelState.AddModelError("DOB", "The Date you introduced is not valid for an Infant");
                return View();
            }

            DateTime checkDateChild = DateTime.Today.Date.AddYears(-18);

            if (tickettype == "Child" && dateCheck > checkDate)
            {
                Flight flightPostClass = (from f in db.Flights where f.FlightID == id select f).FirstOrDefault();

                string getCityOriginClass = (from c in db.Cities where c.CityID == flight.OriginID select c.CityName).FirstOrDefault();

                string getCityDestinationClass = (from c in db.Cities where c.CityID == flight.DestinationID select c.CityName).FirstOrDefault();

                string infoClass = $"Flight Number {flight.FlightID} from {getCityOriginClass } to {getCityDestinationClass } - {flight.DepartureTime}";

                ViewBag.info = infoClass;

                ViewBag.seats = flight.FlightTotalSeats;

                List<Ticket> TicketsListClass = (from t in db.Tickets where t.FlightID == flight.FlightID select t).ToList();

                List<string> TicketSeatClass = (from ts in TicketsListClass select ts.Seat).ToList();

                ViewBag.tickets = TicketSeatClass;

                ViewBag.Type = new SelectList(db.TicketTypes, "TicketTypeID", "TicketTypeName");
                ViewBag.Class = new SelectList(db.TicketClasses, "TicketClassID", "TicketClassName");
                ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "FirstName");
                ViewBag.TicketPriceID = new SelectList(db.TicketPrices, "TicketPriceID", "TicketPriceName");

                ModelState.AddModelError("DOB", "The Date you introduced is not valid for a Child");
                return View();
            }

            if (tickettype == "Child" && dateCheck < checkDateChild)
            {
                Flight flightPostClass = (from f in db.Flights where f.FlightID == id select f).FirstOrDefault();

                string getCityOriginClass = (from c in db.Cities where c.CityID == flight.OriginID select c.CityName).FirstOrDefault();

                string getCityDestinationClass = (from c in db.Cities where c.CityID == flight.DestinationID select c.CityName).FirstOrDefault();

                string infoClass = $"Flight Number {flight.FlightID} from {getCityOriginClass } to {getCityDestinationClass } - {flight.DepartureTime}";

                ViewBag.info = infoClass;

                ViewBag.seats = flight.FlightTotalSeats;

                List<Ticket> TicketsListClass = (from t in db.Tickets where t.FlightID == flight.FlightID select t).ToList();

                List<string> TicketSeatClass = (from ts in TicketsListClass select ts.Seat).ToList();

                ViewBag.tickets = TicketSeatClass;

                ViewBag.Type = new SelectList(db.TicketTypes, "TicketTypeID", "TicketTypeName");
                ViewBag.Class = new SelectList(db.TicketClasses, "TicketClassID", "TicketClassName");
                ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "FirstName");
                ViewBag.TicketPriceID = new SelectList(db.TicketPrices, "TicketPriceID", "TicketPriceName");

                ModelState.AddModelError("DOB", "The Date you introduced is not valid for a Child");
                return View();
            }

            if (tickettype == "Adult" && dateCheck > checkDateChild)
            {
                Flight flightPostClass = (from f in db.Flights where f.FlightID == id select f).FirstOrDefault();

                string getCityOriginClass = (from c in db.Cities where c.CityID == flight.OriginID select c.CityName).FirstOrDefault();

                string getCityDestinationClass = (from c in db.Cities where c.CityID == flight.DestinationID select c.CityName).FirstOrDefault();

                string infoClass = $"Flight Number {flight.FlightID} from {getCityOriginClass } to {getCityDestinationClass } - {flight.DepartureTime}";

                ViewBag.info = infoClass;

                ViewBag.seats = flight.FlightTotalSeats;

                List<Ticket> TicketsListClass = (from t in db.Tickets where t.FlightID == flight.FlightID select t).ToList();

                List<string> TicketSeatClass = (from ts in TicketsListClass select ts.Seat).ToList();

                ViewBag.tickets = TicketSeatClass;

                ViewBag.Type = new SelectList(db.TicketTypes, "TicketTypeID", "TicketTypeName");
                ViewBag.Class = new SelectList(db.TicketClasses, "TicketClassID", "TicketClassName");
                ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "FirstName");
                ViewBag.TicketPriceID = new SelectList(db.TicketPrices, "TicketPriceID", "TicketPriceName");

                ModelState.AddModelError("DOB", "The Date you introduced is not valid for an Adult");
                return View();
            }

            ticket.TicketPriceID = (from tp in db.TicketPrices where tp.TicketClassID == ticketc && tp.TicketTypeID == tickett select tp.TicketPriceID).FirstOrDefault();

            if (ModelState.IsValid)
            {
                ticketID.Name = name;
                ticketID.DOB = dob;
                ticketID.Class = ticketclass;
                ticketID.Type = tickettype;
                ticketID.FlightID = flight.FlightID;
                ticketID.Seat = seat;
                db.Entry(ticketID).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            Flight flightPost = (from f in db.Flights where f.FlightID == ticket.FlightID select f).FirstOrDefault();

            string getCityOrigin = (from c in db.Cities where c.CityID == flightPost.OriginID select c.CityName).FirstOrDefault();

            string getCityDestination = (from c in db.Cities where c.CityID == flightPost.DestinationID select c.CityName).FirstOrDefault();

            string info = $"Flight Number {flightPost.FlightID} from {getCityOrigin} to {getCityDestination} - {flightPost.DepartureTime}";

            ViewBag.info = info;

            ViewBag.seats = flightPost.FlightTotalSeats;

            List<Ticket> TicketsList = (from t in db.Tickets where t.FlightID == flightPost.FlightID select t).ToList();

            List<string> TicketSeat = (from ts in TicketsList select ts.Seat).ToList();

            ViewBag.tickets = TicketSeat;

            ViewBag.Type = new SelectList(db.TicketTypes, "TicketTypeID", "TicketTypeName");
            ViewBag.Class = new SelectList(db.TicketClasses, "TicketClassID", "TicketClassName");
            ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "FirstName");
            ViewBag.TicketPriceID = new SelectList(db.TicketPrices, "TicketPriceID", "TicketPriceName");
            return View(ticket);
        }

        [Authorize(Roles = "EmployeeEdit")]
        // GET: Tickets/Edit/5
        public ActionResult EditEmployee(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("BadRequest", "Requests");
            }
            Ticket ticket = db.Tickets.Find(id);
            if (ticket == null)
            {
                return RedirectToAction("PageNotFound", "Requests");
            }

            Flight flight = (from f in db.Flights where f.FlightID == ticket.FlightID select f).FirstOrDefault();

            string getCityOrigin = (from c in db.Cities where c.CityID == flight.OriginID select c.CityName).FirstOrDefault();

            string getCityDestination = (from c in db.Cities where c.CityID == flight.DestinationID select c.CityName).FirstOrDefault();

            string info = $"Flight Number {flight.FlightID} from {getCityOrigin} to {getCityDestination} - {flight.DepartureTime}";

            ViewBag.info = info;

            ViewBag.seats = flight.FlightTotalSeats;

            List<Ticket> TicketsList = (from t in db.Tickets where t.FlightID == flight.FlightID select t).ToList();

            List<string> TicketSeat = (from ts in TicketsList select ts.Seat).ToList();

            ViewBag.tickets = TicketSeat;

            ViewBag.Type = new SelectList(db.TicketTypes, "TicketTypeID", "TicketTypeName");
            ViewBag.Class = new SelectList(db.TicketClasses, "TicketClassID", "TicketClassName");
            ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "FirstName");
            ViewBag.TicketPriceID = new SelectList(db.TicketPrices, "TicketPriceID", "TicketPriceValue");
            return View(ticket);
        }

        // POST: Tickets/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditEmployee([Bind(Include = "TicketID,Name,DOB,Class,Type,FlightID,Seat,TicketPriceID,ClientID")] Ticket ticket, int? id)
        {
            if (id == null)
            {
                return RedirectToAction("BadRequest", "Requests");
            }
            Ticket ticketCheck = db.Tickets.Find(id);
            if (ticket == null)
            {
                return RedirectToAction("PageNotFound", "Requests");
            }

            Flight flight = (from f in db.Flights where f.FlightID == ticketCheck.FlightID select f).FirstOrDefault();

            var dateDOB = Request["DOB"];

            DateTime dateCheck;

            if (dateDOB == "")
            {
                Flight flightPostClass = (from f in db.Flights where f.FlightID == id select f).FirstOrDefault();

                string getCityOriginClass = (from c in db.Cities where c.CityID == flight.OriginID select c.CityName).FirstOrDefault();

                string getCityDestinationClass = (from c in db.Cities where c.CityID == flight.DestinationID select c.CityName).FirstOrDefault();

                string infoClass = $"Flight Number {flight.FlightID} from {getCityOriginClass } to {getCityDestinationClass } - {flight.DepartureTime}";

                ViewBag.info = infoClass;

                ViewBag.seats = flight.FlightTotalSeats;

                List<Ticket> TicketsListClass = (from t in db.Tickets where t.FlightID == flight.FlightID select t).ToList();

                List<string> TicketSeatClass = (from ts in TicketsListClass select ts.Seat).ToList();

                ViewBag.tickets = TicketSeatClass;

                ViewBag.Type = new SelectList(db.TicketTypes, "TicketTypeID", "TicketTypeName");
                ViewBag.Class = new SelectList(db.TicketClasses, "TicketClassID", "TicketClassName");
                ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "FirstName");
                ViewBag.TicketPriceID = new SelectList(db.TicketPrices, "TicketPriceID", "TicketPriceName");

                ModelState.AddModelError("DOB", "You have to choose a valid Date of Birth");
                return View();
            }
            else
            {
                dateCheck = DateTime.Parse(Request["DOB"]);

                if (dateCheck >= DateTime.Now.Date)
                {
                    Flight flightPostClass = (from f in db.Flights where f.FlightID == id select f).FirstOrDefault();

                    string getCityOriginClass = (from c in db.Cities where c.CityID == flight.OriginID select c.CityName).FirstOrDefault();

                    string getCityDestinationClass = (from c in db.Cities where c.CityID == flight.DestinationID select c.CityName).FirstOrDefault();

                    string infoClass = $"Flight Number {flight.FlightID} from {getCityOriginClass } to {getCityDestinationClass } - {flight.DepartureTime}";

                    ViewBag.info = infoClass;

                    ViewBag.seats = flight.FlightTotalSeats;

                    List<Ticket> TicketsListClass = (from t in db.Tickets where t.FlightID == flight.FlightID select t).ToList();

                    List<string> TicketSeatClass = (from ts in TicketsListClass select ts.Seat).ToList();

                    ViewBag.tickets = TicketSeatClass;

                    ViewBag.Type = new SelectList(db.TicketTypes, "TicketTypeID", "TicketTypeName");
                    ViewBag.Class = new SelectList(db.TicketClasses, "TicketClassID", "TicketClassName");
                    ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "FirstName");
                    ViewBag.TicketPriceID = new SelectList(db.TicketPrices, "TicketPriceID", "TicketPriceName");

                    ModelState.AddModelError("DOB", "You have to choose a valid Date of Birth");
                    return View();
                }
            }
            var classCheck = Request["Class"];

            if (string.IsNullOrEmpty(classCheck))
            {
                Flight flightPostClass = (from f in db.Flights where f.FlightID == ticketCheck.FlightID select f).FirstOrDefault();

                string getCityOriginClass = (from c in db.Cities where c.CityID == flightPostClass.OriginID select c.CityName).FirstOrDefault();

                string getCityDestinationClass = (from c in db.Cities where c.CityID == flightPostClass.DestinationID select c.CityName).FirstOrDefault();

                string infoClass = $"Flight Number {flightPostClass.FlightID} from {getCityOriginClass } to {getCityDestinationClass } - {flightPostClass.DepartureTime}";

                ViewBag.info = infoClass;

                ViewBag.seats = flightPostClass.FlightTotalSeats;

                List<Ticket> TicketsListClass = (from t in db.Tickets where t.FlightID == flightPostClass.FlightID select t).ToList();

                List<string> TicketSeatClass = (from ts in TicketsListClass select ts.Seat).ToList();

                ViewBag.tickets = TicketSeatClass;

                ViewBag.Type = new SelectList(db.TicketTypes, "TicketTypeID", "TicketTypeName");
                ViewBag.Class = new SelectList(db.TicketClasses, "TicketClassID", "TicketClassName");
                ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "FirstName");
                ViewBag.TicketPriceID = new SelectList(db.TicketPrices, "TicketPriceID", "TicketPriceName");

                ModelState.AddModelError("Class", "You have to choose a Class");
                return View();
            }

            var typeCheck = Request["Type"];

            if (string.IsNullOrEmpty(typeCheck))
            {
                Flight flightPostType = (from f in db.Flights where f.FlightID == ticketCheck.FlightID select f).FirstOrDefault();

                string getCityOriginType = (from c in db.Cities where c.CityID == flightPostType.OriginID select c.CityName).FirstOrDefault();

                string getCityDestinationType = (from c in db.Cities where c.CityID == flightPostType.DestinationID select c.CityName).FirstOrDefault();

                string infoType = $"Flight Number {flightPostType.FlightID} from {getCityOriginType } to {getCityDestinationType } - {flightPostType.DepartureTime}";

                ViewBag.info = infoType;

                ViewBag.seats = flightPostType.FlightTotalSeats;

                List<Ticket> TicketsListType = (from t in db.Tickets where t.FlightID == flightPostType.FlightID select t).ToList();

                List<string> TicketSeatType = (from ts in TicketsListType select ts.Seat).ToList();

                ViewBag.tickets = TicketSeatType;

                ViewBag.Type = new SelectList(db.TicketTypes, "TicketTypeID", "TicketTypeName");
                ViewBag.Class = new SelectList(db.TicketClasses, "TicketClassID", "TicketClassName");
                ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "FirstName");
                ViewBag.TicketPriceID = new SelectList(db.TicketPrices, "TicketPriceID", "TicketPriceName");

                ModelState.AddModelError("Type", "You have to choose a Type");
                return View();
            }

            var name = Request["Name"];

            var dobCheck = Request["DOB"];

            if (string.IsNullOrEmpty(dobCheck))
            {
                Flight flightPostDOB = (from f in db.Flights where f.FlightID == ticketCheck.FlightID select f).FirstOrDefault();

                string getCityOriginDOB = (from c in db.Cities where c.CityID == flightPostDOB.OriginID select c.CityName).FirstOrDefault();

                string getCityDestinationDOB = (from c in db.Cities where c.CityID == flightPostDOB.DestinationID select c.CityName).FirstOrDefault();

                string infoDOB = $"Flight Number {flightPostDOB.FlightID} from {getCityOriginDOB} to {getCityDestinationDOB} - {flightPostDOB.DepartureTime}";

                ViewBag.info = infoDOB;

                ViewBag.seats = flightPostDOB.FlightTotalSeats;

                List<Ticket> TicketsListDOB = (from t in db.Tickets where t.FlightID == flightPostDOB.FlightID select t).ToList();

                List<string> TicketSeatDOB = (from ts in TicketsListDOB select ts.Seat).ToList();

                ViewBag.tickets = TicketSeatDOB;

                ViewBag.Type = new SelectList(db.TicketTypes, "TicketTypeID", "TicketTypeName");
                ViewBag.Class = new SelectList(db.TicketClasses, "TicketClassID", "TicketClassName");
                ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "FirstName");
                ViewBag.TicketPriceID = new SelectList(db.TicketPrices, "TicketPriceID", "TicketPriceName");

                ModelState.AddModelError("DOB", "You have to choose a Date of Birth");
                return View();
            }

            var dob = DateTime.Parse(Request["DOB"]);

            var seat = Request["Seat"];

            var ticketID = (from f in db.Tickets where f.TicketID == id select f).FirstOrDefault();

            int ticketc = int.Parse(Request["Class"]);

            string ticketclass = (from c in db.TicketClasses where c.TicketClassID == ticketc select c.TicketClassName).FirstOrDefault();

            int tickett = int.Parse(Request["Type"]);

            string tickettype = (from c in db.TicketTypes where c.TicketTypeID == tickett select c.TicketTypeName).FirstOrDefault();

            DateTime checkDate = DateTime.Today.Date.AddYears(-2);

            if (tickettype == "Infant" && dateCheck < checkDate)
            {
                Flight flightPostClass = (from f in db.Flights where f.FlightID == id select f).FirstOrDefault();

                string getCityOriginClass = (from c in db.Cities where c.CityID == flight.OriginID select c.CityName).FirstOrDefault();

                string getCityDestinationClass = (from c in db.Cities where c.CityID == flight.DestinationID select c.CityName).FirstOrDefault();

                string infoClass = $"Flight Number {flight.FlightID} from {getCityOriginClass } to {getCityDestinationClass } - {flight.DepartureTime}";

                ViewBag.info = infoClass;

                ViewBag.seats = flight.FlightTotalSeats;

                List<Ticket> TicketsListClass = (from t in db.Tickets where t.FlightID == flight.FlightID select t).ToList();

                List<string> TicketSeatClass = (from ts in TicketsListClass select ts.Seat).ToList();

                ViewBag.tickets = TicketSeatClass;

                ViewBag.Type = new SelectList(db.TicketTypes, "TicketTypeID", "TicketTypeName");
                ViewBag.Class = new SelectList(db.TicketClasses, "TicketClassID", "TicketClassName");
                ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "FirstName");
                ViewBag.TicketPriceID = new SelectList(db.TicketPrices, "TicketPriceID", "TicketPriceName");

                ModelState.AddModelError("DOB", "The Date you introduced is not valid for an Infant");
                return View();
            }

            DateTime checkDateChild = DateTime.Today.Date.AddYears(-18);

            if (tickettype == "Child" && dateCheck > checkDate)
            {
                Flight flightPostClass = (from f in db.Flights where f.FlightID == id select f).FirstOrDefault();

                string getCityOriginClass = (from c in db.Cities where c.CityID == flight.OriginID select c.CityName).FirstOrDefault();

                string getCityDestinationClass = (from c in db.Cities where c.CityID == flight.DestinationID select c.CityName).FirstOrDefault();

                string infoClass = $"Flight Number {flight.FlightID} from {getCityOriginClass } to {getCityDestinationClass } - {flight.DepartureTime}";

                ViewBag.info = infoClass;

                ViewBag.seats = flight.FlightTotalSeats;

                List<Ticket> TicketsListClass = (from t in db.Tickets where t.FlightID == flight.FlightID select t).ToList();

                List<string> TicketSeatClass = (from ts in TicketsListClass select ts.Seat).ToList();

                ViewBag.tickets = TicketSeatClass;

                ViewBag.Type = new SelectList(db.TicketTypes, "TicketTypeID", "TicketTypeName");
                ViewBag.Class = new SelectList(db.TicketClasses, "TicketClassID", "TicketClassName");
                ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "FirstName");
                ViewBag.TicketPriceID = new SelectList(db.TicketPrices, "TicketPriceID", "TicketPriceName");

                ModelState.AddModelError("DOB", "The Date you introduced is not valid for a Child");
                return View();
            }

            if (tickettype == "Child" && dateCheck < checkDateChild)
            {
                Flight flightPostClass = (from f in db.Flights where f.FlightID == id select f).FirstOrDefault();

                string getCityOriginClass = (from c in db.Cities where c.CityID == flight.OriginID select c.CityName).FirstOrDefault();

                string getCityDestinationClass = (from c in db.Cities where c.CityID == flight.DestinationID select c.CityName).FirstOrDefault();

                string infoClass = $"Flight Number {flight.FlightID} from {getCityOriginClass } to {getCityDestinationClass } - {flight.DepartureTime}";

                ViewBag.info = infoClass;

                ViewBag.seats = flight.FlightTotalSeats;

                List<Ticket> TicketsListClass = (from t in db.Tickets where t.FlightID == flight.FlightID select t).ToList();

                List<string> TicketSeatClass = (from ts in TicketsListClass select ts.Seat).ToList();

                ViewBag.tickets = TicketSeatClass;

                ViewBag.Type = new SelectList(db.TicketTypes, "TicketTypeID", "TicketTypeName");
                ViewBag.Class = new SelectList(db.TicketClasses, "TicketClassID", "TicketClassName");
                ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "FirstName");
                ViewBag.TicketPriceID = new SelectList(db.TicketPrices, "TicketPriceID", "TicketPriceName");

                ModelState.AddModelError("DOB", "The Date you introduced is not valid for a Child");
                return View();
            }

            if (tickettype == "Adult" && dateCheck > checkDateChild)
            {
                Flight flightPostClass = (from f in db.Flights where f.FlightID == id select f).FirstOrDefault();

                string getCityOriginClass = (from c in db.Cities where c.CityID == flight.OriginID select c.CityName).FirstOrDefault();

                string getCityDestinationClass = (from c in db.Cities where c.CityID == flight.DestinationID select c.CityName).FirstOrDefault();

                string infoClass = $"Flight Number {flight.FlightID} from {getCityOriginClass } to {getCityDestinationClass } - {flight.DepartureTime}";

                ViewBag.info = infoClass;

                ViewBag.seats = flight.FlightTotalSeats;

                List<Ticket> TicketsListClass = (from t in db.Tickets where t.FlightID == flight.FlightID select t).ToList();

                List<string> TicketSeatClass = (from ts in TicketsListClass select ts.Seat).ToList();

                ViewBag.tickets = TicketSeatClass;

                ViewBag.Type = new SelectList(db.TicketTypes, "TicketTypeID", "TicketTypeName");
                ViewBag.Class = new SelectList(db.TicketClasses, "TicketClassID", "TicketClassName");
                ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "FirstName");
                ViewBag.TicketPriceID = new SelectList(db.TicketPrices, "TicketPriceID", "TicketPriceName");

                ModelState.AddModelError("DOB", "The Date you introduced is not valid for an Adult");
                return View();
            }

            ticket.TicketPriceID = (from tp in db.TicketPrices where tp.TicketClassID == ticketc && tp.TicketTypeID == tickett select tp.TicketPriceID).FirstOrDefault();

            if (ModelState.IsValid)
            {
                ticketID.Name = name;
                ticketID.DOB = dob;
                ticketID.Class = ticketclass;
                ticketID.Type = tickettype;
                ticketID.FlightID = flight.FlightID;
                ticketID.Seat = seat;
                db.Entry(ticketID).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("IndexTicketsEmployee");
            }

            Flight flightPost = (from f in db.Flights where f.FlightID == ticket.FlightID select f).FirstOrDefault();

            string getCityOrigin = (from c in db.Cities where c.CityID == flightPost.OriginID select c.CityName).FirstOrDefault();

            string getCityDestination = (from c in db.Cities where c.CityID == flightPost.DestinationID select c.CityName).FirstOrDefault();

            string info = $"Flight Number {flightPost.FlightID} from {getCityOrigin} to {getCityDestination} - {flightPost.DepartureTime}";

            ViewBag.info = info;

            ViewBag.seats = flightPost.FlightTotalSeats;

            List<Ticket> TicketsList = (from t in db.Tickets where t.FlightID == flightPost.FlightID select t).ToList();

            List<string> TicketSeat = (from ts in TicketsList select ts.Seat).ToList();

            ViewBag.tickets = TicketSeat;

            ViewBag.Type = new SelectList(db.TicketTypes, "TicketTypeID", "TicketTypeName");
            ViewBag.Class = new SelectList(db.TicketClasses, "TicketClassID", "TicketClassName");
            ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "FirstName");
            ViewBag.TicketPriceID = new SelectList(db.TicketPrices, "TicketPriceID", "TicketPriceName");
            return View(ticket);
        }

        [Authorize(Roles = "Delete")]
        // GET: Tickets/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("BadRequest", "Requests");
            }
            Ticket ticket = db.Tickets.Find(id);
            if (ticket == null)
            {
                return RedirectToAction("PageNotFound", "Requests");
            }
            return View(ticket);
        }

        // POST: Tickets/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Ticket ticket = db.Tickets.Find(id);
            db.Tickets.Remove(ticket);
            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                RedirectToAction("NotPossible", "Requests");
            }
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "EmployeeDelete")]
        // GET: Tickets/Delete/5
        public ActionResult DeleteEmployee(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("BadRequest", "Requests");
            }
            Ticket ticket = db.Tickets.Find(id);
            if (ticket == null)
            {
                return RedirectToAction("PageNotFound", "Requests");
            }
            return View(ticket);
        }

        // POST: Tickets/Delete/5
        [HttpPost, ActionName("DeleteEmployee")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteEmployeeConfirmed(int id)
        {
            Ticket ticket = db.Tickets.Find(id);
            db.Tickets.Remove(ticket);
            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                RedirectToAction("NotPossible", "Requests");
            }
            return RedirectToAction("IndexTicketsEmployee");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
