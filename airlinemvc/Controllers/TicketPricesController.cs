﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using airlinemvc.Models;

namespace airlinemvc.Controllers
{
    [Authorize(Users = "antonio.franco.frazao@formandos.cinel.pt")]
    public class TicketPricesController : Controller
    {
        private AirlineMVCContext db = new AirlineMVCContext();

        // GET: TicketPrices
        public ActionResult Index()
        {
            var ticketPrices = db.TicketPrices.Include(t => t.TicketClass).Include(t => t.TicketType);
            return View(ticketPrices.ToList());
        }

        // GET: TicketPrices/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("BadRequest", "Requests");
            }
            TicketPrice ticketPrice = db.TicketPrices.Find(id);
            if (ticketPrice == null)
            {
                return RedirectToAction("PageNotFound", "Requests");
            }
            return View(ticketPrice);
        }

        // GET: TicketPrices/Create
        public ActionResult Create()
        {
            ViewBag.TicketClassID = new SelectList(db.TicketClasses, "TicketClassID", "TicketClassName");
            ViewBag.TicketTypeID = new SelectList(db.TicketTypes, "TicketTypeID", "TicketTypeName");
            return View();
        }

        // POST: TicketPrices/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "TicketPriceID,TicketPriceName,TicketPriceValue,TicketClassID,TicketTypeID")] TicketPrice ticketPrice)
        {
            if (ModelState.IsValid)
            {
                db.TicketPrices.Add(ticketPrice);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.TicketClassID = new SelectList(db.TicketClasses, "TicketClassID", "TicketClassName", ticketPrice.TicketClassID);
            ViewBag.TicketTypeID = new SelectList(db.TicketTypes, "TicketTypeID", "TicketTypeName", ticketPrice.TicketTypeID);
            return View(ticketPrice);
        }

        // GET: TicketPrices/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("BadRequest", "Requests");
            }
            TicketPrice ticketPrice = db.TicketPrices.Find(id);
            if (ticketPrice == null)
            {
                return RedirectToAction("PageNotFound", "Requests");
            }
            ViewBag.TicketClassID = new SelectList(db.TicketClasses, "TicketClassID", "TicketClassName", ticketPrice.TicketClassID);
            ViewBag.TicketTypeID = new SelectList(db.TicketTypes, "TicketTypeID", "TicketTypeName", ticketPrice.TicketTypeID);
            return View(ticketPrice);
        }

        // POST: TicketPrices/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "TicketPriceID,TicketPriceName,TicketPriceValue,TicketClassID,TicketTypeID")] TicketPrice ticketPrice)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ticketPrice).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.TicketClassID = new SelectList(db.TicketClasses, "TicketClassID", "TicketClassName", ticketPrice.TicketClassID);
            ViewBag.TicketTypeID = new SelectList(db.TicketTypes, "TicketTypeID", "TicketTypeName", ticketPrice.TicketTypeID);
            return View(ticketPrice);
        }

        // GET: TicketPrices/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("BadRequest", "Requests");
            }
            TicketPrice ticketPrice = db.TicketPrices.Find(id);
            if (ticketPrice == null)
            {
                return RedirectToAction("PageNotFound", "Requests");
            }
            return View(ticketPrice);
        }

        // POST: TicketPrices/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TicketPrice ticketPrice = db.TicketPrices.Find(id);
            db.TicketPrices.Remove(ticketPrice);
            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                RedirectToAction("NotPossible", "Requests");
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
