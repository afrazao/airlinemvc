﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using airlinemvc.Models;

namespace airlinemvc.Controllers
{
    public class AirplanesController : Controller
    {
        private AirlineMVCContext db = new AirlineMVCContext();

        [Authorize(Roles = "View")]//
        // GET: Airplanes
        public ActionResult Index()
        {
            var airplanes = db.Airplanes.Include(a => a.AirplaneRange);
            return View(airplanes.ToList());
        }

        [AllowAnonymous]
        // GET: Airplanes
        public ActionResult IndexAnonymous()
        {
            var airplanes = db.Airplanes.Include(a => a.AirplaneRange);
            return View(airplanes.ToList());
        }

        [Authorize(Roles = "EmployeeView")]
        // GET: Airplanes
        public ActionResult IndexEmployee()
        {
            var airplanes = db.Airplanes.Include(a => a.AirplaneRange);
            return View(airplanes.ToList());
        }

        [Authorize(Roles = "antonio.franco.frazao@formandos.cinel.pt")]
        // GET: Airplanes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("BadRequest", "Requests");
            }
            Airplane airplane = db.Airplanes.Find(id);
            if (airplane == null)
            {
                return RedirectToAction("PageNotFound", "Requests");
            }
            return View(airplane);
        }

        [Authorize(Roles = "Create")]
        // GET: Airplanes/Create
        public ActionResult Create()
        {
            ViewBag.RangeID = new SelectList(db.AirplaneRanges, "RangeID", "RangeDistance");
            return View();
        }

        // POST: Airplanes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "AirplaneID,Brand,Model,RangeID,CruiseSpeed,Seats")] Airplane airplane)
        {
            var checkCruiseSpeed = Request["CruiseSpeed"];

            var checkSeats = Request["Seats"];

            var rangeCheck = Request["Seats"];

            if (string.IsNullOrEmpty(checkCruiseSpeed) || string.IsNullOrEmpty(checkSeats) || string.IsNullOrEmpty(rangeCheck))
            {
                ViewBag.RangeID = new SelectList(db.AirplaneRanges, "RangeID", "RangeDistance");
                ModelState.AddModelError(string.Empty, "All fields are required");
                return View();
                
            }
            if (ModelState.IsValid)
            {
                db.Airplanes.Add(airplane);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.RangeID = new SelectList(db.AirplaneRanges, "RangeID", "RangeDistance", airplane.RangeID);
            return View(airplane);
        }

        [Authorize(Roles = "EmployeeCreate")]
        // GET: Airplanes/Create
        public ActionResult CreateEmployee()
        {
            ViewBag.RangeID = new SelectList(db.AirplaneRanges, "RangeID", "RangeDistance");
            return View();
        }

        // POST: Airplanes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateEmployee([Bind(Include = "AirplaneID,Brand,Model,RangeID,CruiseSpeed,Seats")] Airplane airplane)
        {
            var checkCruiseSpeed = Request["CruiseSpeed"];

            var checkSeats = Request["Seats"];

            var rangeCheck = Request["Seats"];

            if (string.IsNullOrEmpty(checkCruiseSpeed) || string.IsNullOrEmpty(checkSeats) || string.IsNullOrEmpty(rangeCheck))
            {
                ViewBag.RangeID = new SelectList(db.AirplaneRanges, "RangeID", "RangeDistance");
                ModelState.AddModelError(string.Empty, "All fields are required");
                return View();

            }

            if (ModelState.IsValid)
            {
                db.Airplanes.Add(airplane);
                db.SaveChanges();
                return RedirectToAction("IndexEmployee");
            }

            ViewBag.RangeID = new SelectList(db.AirplaneRanges, "RangeID", "RangeDistance", airplane.RangeID);
            return View(airplane);
        }

        [Authorize(Roles ="Edit")]
        // GET: Airplanes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("BadRequest", "Requests");
            }
            Airplane airplane = db.Airplanes.Find(id);
            if (airplane == null)
            {
                return RedirectToAction("PageNotFound", "Requests");
            }
            ViewBag.RangeID = new SelectList(db.AirplaneRanges, "RangeID", "RangeDistance", airplane.RangeID);
            return View(airplane);
        }

        // POST: Airplanes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "AirplaneID,Brand,Model,RangeID,CruiseSpeed,Seats")] Airplane airplane)
        {
            var checkCruiseSpeed = Request["CruiseSpeed"];

            var checkSeats = Request["Seats"];

            var rangeCheck = Request["Seats"];

            if (string.IsNullOrEmpty(checkCruiseSpeed) || string.IsNullOrEmpty(checkSeats) || string.IsNullOrEmpty(rangeCheck))
            {
                ViewBag.RangeID = new SelectList(db.AirplaneRanges, "RangeID", "RangeDistance");
                ModelState.AddModelError(string.Empty, "All fields are required");
                return View();

            }

            if (ModelState.IsValid)
            {
                db.Entry(airplane).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.RangeID = new SelectList(db.AirplaneRanges, "RangeID", "RangeDistance", airplane.RangeID);
            return View(airplane);
        }

        [Authorize(Roles = "EmployeeEdit")]
        // GET: Airplanes/Edit/5
        public ActionResult EditEmployee(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("BadRequest", "Requests");
            }
            Airplane airplane = db.Airplanes.Find(id);
            if (airplane == null)
            {
                return RedirectToAction("PageNotFound", "Requests");
            }
            ViewBag.RangeID = new SelectList(db.AirplaneRanges, "RangeID", "RangeDistance", airplane.RangeID);
            return View(airplane);
        }

        // POST: Airplanes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditEmployee([Bind(Include = "AirplaneID,Brand,Model,RangeID,CruiseSpeed,Seats")] Airplane airplane)
        {
            var checkCruiseSpeed = Request["CruiseSpeed"];

            var checkSeats = Request["Seats"];

            var rangeCheck = Request["Seats"];

            if (string.IsNullOrEmpty(checkCruiseSpeed) || string.IsNullOrEmpty(checkSeats) || string.IsNullOrEmpty(rangeCheck))
            {
                ViewBag.RangeID = new SelectList(db.AirplaneRanges, "RangeID", "RangeDistance");
                ModelState.AddModelError(string.Empty, "All fields are required");
                return View();

            }
            if (ModelState.IsValid)
            {
                db.Entry(airplane).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("IndexEmployee");
            }
            ViewBag.RangeID = new SelectList(db.AirplaneRanges, "RangeID", "RangeDistance", airplane.RangeID);
            return View(airplane);
        }

        [Authorize(Roles = "Delete")]
        // GET: Airplanes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("BadRequest", "Requests");
            }
            Airplane airplane = db.Airplanes.Find(id);
            if (airplane == null)
            {
                return RedirectToAction("PageNotFound", "Requests");
            }
            return View(airplane);
        }

        // POST: Airplanes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Airplane airplane = db.Airplanes.Find(id);
            db.Airplanes.Remove(airplane);
            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                RedirectToAction("NotPossible", "Requests");
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
