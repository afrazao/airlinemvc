﻿using airlinemvc.Models;
using airlinemvc.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace airlinemvc.Controllers
{
    [Authorize(Users = "antonio.franco.frazao@formandos.cinel.pt")]
    public class UsersController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Users
        public ActionResult Index() //AQUI VAMOS BUSCAR A LISTA DE USERS PARA MOSTRAR NA VIEWMODEL
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
            var users = userManager.Users.ToList();
            var usersView = new List<UserView>();

            foreach(var user in users)
            {
                var userView = new UserView
                {
                    UserID = user.Id,             
                    Name = user.UserName,
                    Email = user.Email
                };

                usersView.Add(userView);
            }

            return View(usersView);
        }

        //Get: Roles
        public ActionResult Roles(string userID)
        {
            if (string.IsNullOrEmpty(userID))
            {
                return RedirectToAction("BadRequest", "Requests");
            }

            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));

            var users = userManager.Users.ToList();
            var roles = roleManager.Roles.ToList();
            var user = users.Find(u => u.Id == userID);

            if(user == null)
            {
                return RedirectToAction("PageNotFound", "Requests");
            }

            var rolesView = new List<RoleView>();
            foreach(var item in user.Roles)
            {
                var role = roles.Find(r => r.Id == item.RoleId);
                var roleView = new RoleView
                {
                    RoleID = role.Id,
                    NameRole = role.Name
                };

                rolesView.Add(roleView);
            }

            var userView = new UserView
            {
                UserID = user.Id,
                Name = user.UserName,
                Email = user.Email,
                Roles = rolesView
            };

            return View(userView);
        }

        public ActionResult AddRole(string userID)
        {
            if (string.IsNullOrEmpty(userID))
            {
                return RedirectToAction("BadRequest", "Requests");
            }

            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
            var users = userManager.Users.ToList();
            var user = users.Find(u => u.Id == userID);

            if (user == null)
            {
                return RedirectToAction("PageNotFound", "Requests");
            }

            var userView = new UserView
            {
                UserID = user.Id,
                Name = user.UserName,
                Email = user.Email
            };

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
            var list = roleManager.Roles.ToList();
            list.Add(new IdentityRole { Id = "", Name = "{Select a Permission...}" });
            list = list.OrderBy(r => r.Name).ToList();
            ViewBag.RoleID = new SelectList(list, "Id", "Name");

            return View(userView);
        }

        //Post:
        [HttpPost]

        public ActionResult AddRole(string userID, FormCollection form)
        {
            var roleId = Request["RoleID"];

            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
            var users = userManager.Users.ToList();
            var user = users.Find(u => u.Id == userID);

            var userView = new UserView
            {
                UserID = user.Id,
                Name = user.UserName,
                Email = user.Email
            };

            if (string.IsNullOrEmpty(roleId))
            {
                ViewBag.Error = "You have to select a Permission to add";

                var rolesManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
                var list = rolesManager.Roles.ToList();
                list.Add(new IdentityRole { Id = "", Name = "{Select a Permission...}" });
                list = list.OrderBy(r => r.Name).ToList();
                ViewBag.RoleID = new SelectList(list, "Id", "Name");
            }

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
            var roles = roleManager.Roles.ToList();
            var role = roles.Find(r => r.Id == roleId);

            if(!userManager.IsInRole(userID, role.Name))
            {
                userManager.AddToRole(userID, role.Name);
            }

            var rolesView = new List<RoleView>();

            foreach (var item in user.Roles)
            {
                role = roles.Find(r => r.Id == item.RoleId);
                var roleView = new RoleView
                {
                    NameRole = role.Name,
                    RoleID = role.Id
                };

                rolesView.Add(roleView);
            }

            userView = new UserView
            {
                UserID = user.Id,
                Name = user.UserName,
                Email = user.Email,
                Roles = rolesView,
            };

            return View("Roles", userView);
        }

        public ActionResult Delete(string userID, string roleID)
        {
            if (string.IsNullOrEmpty(userID) || string.IsNullOrEmpty(roleID))
            {
                return RedirectToAction("BadRequest", "Requests");
            }

            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));

            var user = userManager.Users.ToList().Find(u => u.Id == userID);
            var role = roleManager.Roles.ToList().Find(r => r.Id == roleID);

            //Apagar o User deste Role.
            if (userManager.IsInRole(user.Id, role.Name))
            {
                userManager.RemoveFromRole(user.Id, role.Name);
            }

            //Preparar a View
            var users = userManager.Users.ToList();
            var roles = roleManager.Roles.ToList();
            var rolesView = new List<RoleView>();

            foreach(var item in user.Roles)
            {
                role = roles.Find(r => r.Id == item.RoleId);
                var roleView = new RoleView
                {
                    NameRole = role.Name,
                    RoleID = role.Id
                };

                rolesView.Add(roleView);
            }

            var userView = new UserView
            {
                UserID = user.Id,
                Name = user.UserName,
                Email = user.Email,
                Roles = rolesView
            };

            return View("Roles", userView);

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}