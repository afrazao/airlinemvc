﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace airlinemvc.Controllers
{
    [Authorize(Users = "antonio.franco.frazao@formandos.cinel.pt")]
    public class RequestsController : Controller
    {
        // GET: Requests
        public ActionResult PageNotFound()
        {
            return View();
        }

        // GET: Requests
        public ActionResult BadRequest()
        {
            return View();
        }

        // GET: Requests
        public ActionResult NotPossible()
        {
            return View();
        }
    }
}