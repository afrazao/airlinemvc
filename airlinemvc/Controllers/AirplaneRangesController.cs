﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using airlinemvc.Models;

namespace airlinemvc.Controllers
{
    [Authorize(Users = "antonio.franco.frazao@formandos.cinel.pt")]
    public class AirplaneRangesController : Controller
    {
        private AirlineMVCContext db = new AirlineMVCContext();

        // GET: AirplaneRanges
        public ActionResult Index()
        {
            return View(db.AirplaneRanges.ToList());
        }

        // GET: AirplaneRanges/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("BadRequest", "Requests");
            }
            AirplaneRange airplaneRange = db.AirplaneRanges.Find(id);
            if (airplaneRange == null)
            {
                return RedirectToAction("PageNotFound", "Requests");
            }
            return View(airplaneRange);
        }

        // GET: AirplaneRanges/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: AirplaneRanges/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "RangeID,RangeDistance")] AirplaneRange airplaneRange)
        {
            if (ModelState.IsValid)
            {
                db.AirplaneRanges.Add(airplaneRange);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(airplaneRange);
        }

        // GET: AirplaneRanges/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("BadRequest", "Requests");
            }
            AirplaneRange airplaneRange = db.AirplaneRanges.Find(id);
            if (airplaneRange == null)
            {
                return RedirectToAction("PageNotFound", "Requests");
            }
            return View(airplaneRange);
        }

        // POST: AirplaneRanges/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "RangeID,RangeDistance")] AirplaneRange airplaneRange)
        {
            if (ModelState.IsValid)
            {
                db.Entry(airplaneRange).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(airplaneRange);
        }

        // GET: AirplaneRanges/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("BadRequest", "Requests");
            }
            AirplaneRange airplaneRange = db.AirplaneRanges.Find(id);
            if (airplaneRange == null)
            {
                return RedirectToAction("PageNotFound", "Requests");
            }
            return View(airplaneRange);
        }

        // POST: AirplaneRanges/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            AirplaneRange airplaneRange = db.AirplaneRanges.Find(id);

            db.AirplaneRanges.Remove(airplaneRange);

            try
            {
                db.SaveChanges();
            }
            catch(Exception e)
            {
                RedirectToAction("NotPossible", "Requests");
            }

            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
