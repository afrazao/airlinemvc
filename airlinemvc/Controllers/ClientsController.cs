﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using airlinemvc.Models;

namespace airlinemvc.Controllers
{
    public class ClientsController : Controller
    {
        private AirlineMVCContext db = new AirlineMVCContext();
        private ApplicationDbContext dbApp = new ApplicationDbContext();

        [Authorize(Roles = "View")]
        // GET: Clients
        public ActionResult Index()
        {
            return View(db.Clients.ToList());
        }

        [Authorize(Roles = "ClientView")]
        // GET: Clients
        public ActionResult IndexClient()
        {
            Client client = new Client();

            var idUser = (from u in dbApp.Users where u.Email == User.Identity.Name select u.Id);

            int id = (from c in db.Clients where c.Email == User.Identity.Name select c.ClientID).FirstOrDefault();

            if (id == 0)
            {

                return RedirectToAction("CreateClient");
            }

            client = db.Clients.Find(id);
            return View(client);
        }

        [Authorize(Roles = "EmployeeView")]
        // GET: Clients
        public ActionResult IndexEmployee()
        {
            return View(db.Clients.ToList());
        }

        [Authorize(Users = "antonio.franco.frazao@formandos.cinel.pt")]
        // GET: Clients/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("BadRequest", "Requests");
            }
            Client client = db.Clients.Find(id);
            if (client == null)
            {
                return RedirectToAction("PageNotFound", "Requests");
            }
            return View(client);
        }

        [Authorize(Users = "antonio.franco.frazao@formandos.cinel.pt")]
        // GET: Clients/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Clients/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ClientID,FirstName,LastName,DateOfBirth,TIN,Email")] Client client)
        {
            var emailCheck = Request["Email"];

            if (string.IsNullOrEmpty(emailCheck))
            {
                ModelState.AddModelError("Email", "You have to Register first.");
            }

            var check = Request["DateOfBirth"];

            if (check == "")
            {
                ModelState.AddModelError("DateOfBirth", "The Date is Invalid");
                return View(client);
            }
            else
            {
                DateTime dateCheck = DateTime.Parse(Request["DateOfBirth"]);

                if (dateCheck >= DateTime.Now.Date)
                {
                    ModelState.AddModelError("DateOfBirth", "The Date of Birth cannot be greater than Today");
                    return View(client);
                }
            }

            if (ModelState.IsValid)
            {
                db.Clients.Add(client);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(client);
        }

        [AllowAnonymous]
        // GET: Clients/Create
        public ActionResult CreateClient(string username)
        {
            var userEmail = User.Identity.Name;
            var email = dbApp.Users.Where(i => i.Email == userEmail).First();
            ViewBag.Email = email.Email;
            return View();
        }

        // POST: Clients/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateClient([Bind(Include = "ClientID,FirstName,LastName,DateOfBirth,TIN,Email")] Client client, string username)
        {
            var userEmail = User.Identity.Name;
            var email = dbApp.Users.Where(i => i.Email == userEmail).First();
            ViewBag.Email = email.Email;

            var emailCheck = Request["Email"];

            if (string.IsNullOrEmpty(emailCheck))
            {
                ModelState.AddModelError("Email", "You have to Register first.");
            }

            var check = Request["DateOfBirth"];

            if (check == "")
            {
                ViewBag.user = username;
                userEmail = User.Identity.Name;
                email = dbApp.Users.Where(i => i.Email == userEmail).First();
                ViewBag.Email = email.Email;
                ModelState.AddModelError("DateOfBirth", "The Date is Invalid");
                return View();
            }
            else
            {
                DateTime dateCheck = DateTime.Parse(Request["DateOfBirth"]);

                if (dateCheck >= DateTime.Now.Date)
                {
                    ViewBag.user = username;
                    userEmail = User.Identity.Name;
                    email = dbApp.Users.Where(i => i.Email == userEmail).First();
                    ViewBag.Email = email.Email;
                    ModelState.AddModelError("DateOfBirth", "The Date of Birth cannot be greater than Today");
                    return View();
                }
            }

            if (ModelState.IsValid)
            {
                db.Clients.Add(client);
                db.SaveChanges();
                return RedirectToAction("IndexClient");
            }

            return View(client);
        }

        [Authorize(Roles = "EmployeeCreate")]
        // GET: Clients/Create
        public ActionResult CreateEmployee()
        {
            return View();
        }

        // POST: Clients/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateEmployee([Bind(Include = "ClientID,FirstName,LastName,DateOfBirth,TIN,Email")] Client client)
        {

            var emailCheck = Request["Email"];

            if (string.IsNullOrEmpty(emailCheck))
            {
                ModelState.AddModelError("Email", "You have to Register first.");
            }

            var check = Request["DateOfBirth"];

            if (check == "")
            {
                ModelState.AddModelError("DateOfBirth", "The Date is Invalid");
                return View();
            }
            else
            {
                DateTime dateCheck = DateTime.Parse(Request["DateOfBirth"]);

                if (dateCheck >= DateTime.Now.Date)
                {
                    ModelState.AddModelError("DateOfBirth", "The Date of Birth cannot be greater than Today");
                    return View();
                }
            }

            if (ModelState.IsValid)
            {
                db.Clients.Add(client);
                db.SaveChanges();
                return RedirectToAction("IndexEmployee");
            }

            return View(client);
        }

        [Authorize(Roles = "Edit")]
        // GET: Clients/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("BadRequest", "Requests");
            }
            Client client = db.Clients.Find(id);
            if (client == null)
            {
                return RedirectToAction("PageNotFound", "Requests");
            }
            return View(client);
        }

        // POST: Clients/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ClientID,FirstName,LastName,DateOfBirth,TIN,Email")] Client client)
        {
            var check = Request["DateOfBirth"];

            if (check == "")
            {
                ModelState.AddModelError("DateOfBirth", "The Date is Invalid");
                return View(client);
            }
            else
            {
                DateTime dateCheck = DateTime.Parse(Request["DateOfBirth"]);

                if (dateCheck >= DateTime.Now.Date)
                {
                    ModelState.AddModelError("DateOfBirth", "The Date of Birth cannot be greater than Today");
                    return View(client);
                }
            }

            if (ModelState.IsValid)
            {
                db.Entry(client).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(client);
        }

        [Authorize(Roles = "ClientEdit")]
        // GET: Clients/Edit/5
        public ActionResult EditClient(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("BadRequest", "Requests");
            }
            Client client = db.Clients.Find(id);
            if (client == null)
            {
                return RedirectToAction("PageNotFound", "Requests");
            }
            return View(client);
        }

        // POST: Clients/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditClient([Bind(Include = "ClientID,FirstName,LastName,DateOfBirth,TIN,Email")] Client client)
        {
            var check = Request["DateOfBirth"];

            if (check == "")
            {
                ModelState.AddModelError("DateOfBirth", "The Date is Invalid");
                return View(client);
            }
            else
            {
                DateTime dateCheck = DateTime.Parse(Request["DateOfBirth"]);

                if (dateCheck >= DateTime.Now.Date)
                {
                    ModelState.AddModelError("DateOfBirth", "The Date of Birth cannot be greater than Today");
                    return View(client);
                }
            }
            if (ModelState.IsValid)
            {
                db.Entry(client).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("IndexClient");
            }
            return View(client);
        }

        [Authorize(Roles = "EmployeeEdit")]
        // GET: Clients/Edit/5
        public ActionResult EditEmployee(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("BadRequest", "Requests");
            }
            Client client = db.Clients.Find(id);
            if (client == null)
            {
                return RedirectToAction("PageNotFound", "Requests");
            }
            return View(client);
        }

        // POST: Clients/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditEmployee([Bind(Include = "ClientID,FirstName,LastName,DateOfBirth,TIN,Email")] Client client)
        {
            var check = Request["DateOfBirth"];

            if (check == "")
            {
                ModelState.AddModelError("DateOfBirth", "The Date is Invalid");
                return View(client);
            }
            else
            {
                DateTime dateCheck = DateTime.Parse(Request["DateOfBirth"]);

                if (dateCheck >= DateTime.Now.Date)
                {
                    ModelState.AddModelError("DateOfBirth", "The Date of Birth cannot be greater than Today");
                    return View(client);
                }
            }
            if (ModelState.IsValid)
            {
                db.Entry(client).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("IndexEmployee");
            }
            return View(client);
        }

        [Authorize(Roles = "Delete")]
        // GET: Clients/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("BadRequest", "Requests");
            }
            Client client = db.Clients.Find(id);
            if (client == null)
            {
                return RedirectToAction("PageNotFound", "Requests");
            }
            return View(client);
        }

        // POST: Clients/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Client client = db.Clients.Find(id);
            db.Clients.Remove(client);
            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                RedirectToAction("NotPossible", "Requests");
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
