﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using airlinemvc.Models;

namespace airlinemvc.Controllers
{
    [Authorize(Users = "antonio.franco.frazao@formandos.cinel.pt")]
    public class TicketClassesController : Controller
    {
        private AirlineMVCContext db = new AirlineMVCContext();

        // GET: TicketClasses
        public ActionResult Index()
        {
            return View(db.TicketClasses.ToList());
        }

        // GET: TicketClasses/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("BadRequest", "Requests");
            }
            TicketClass ticketClass = db.TicketClasses.Find(id);
            if (ticketClass == null)
            {
                return RedirectToAction("PageNotFound", "Requests");
            }
            return View(ticketClass);
        }

        // GET: TicketClasses/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TicketClasses/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "TicketClassID,TicketClassName")] TicketClass ticketClass)
        {
            if (ModelState.IsValid)
            {
                db.TicketClasses.Add(ticketClass);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(ticketClass);
        }

        // GET: TicketClasses/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("BadRequest", "Requests");
            }
            TicketClass ticketClass = db.TicketClasses.Find(id);
            if (ticketClass == null)
            {
                return RedirectToAction("PageNotFound", "Requests");
            }
            return View(ticketClass);
        }

        // POST: TicketClasses/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "TicketClassID,TicketClassName")] TicketClass ticketClass)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ticketClass).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(ticketClass);
        }

        // GET: TicketClasses/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("BadRequest", "Requests");
            }
            TicketClass ticketClass = db.TicketClasses.Find(id);
            if (ticketClass == null)
            {
                return RedirectToAction("PageNotFound", "Requests");
            }
            return View(ticketClass);
        }

        // POST: TicketClasses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TicketClass ticketClass = db.TicketClasses.Find(id);
            db.TicketClasses.Remove(ticketClass);
            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                RedirectToAction("NotPossible", "Requests");
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
