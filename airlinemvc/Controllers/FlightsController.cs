﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using airlinemvc.Helpers;
using airlinemvc.Models;

namespace airlinemvc.Controllers
{
    public class FlightsController : Controller
    {
        private AirlineMVCContext db = new AirlineMVCContext();

        [Authorize(Roles = "View")]
        // GET: Flights
        public ActionResult Index()
        {
            var flights = db.Flights.Include(f => f.Airplane).Include(f => f.CountryDestination).Include(f => f.CountryOrigin).Include(f => f.Destination).Include(f => f.Origin);
            return View(flights.ToList());
        }

        [AllowAnonymous]
        // GET: Flights
        public ActionResult IndexAnonymous()
        {
            var flights = db.Flights.Include(f => f.Airplane).Include(f => f.CountryDestination).Include(f => f.CountryOrigin).Include(f => f.Destination).Include(f => f.Origin).Where(f => f.DepartureTime >= DateTime.Today).OrderBy(f => f.DepartureTime);

            ViewBag.Countries = new SelectList(db.Countries, "CountryID", "CountryName");
            ViewBag.Cities = new SelectList(db.Cities, "CityID", "CityName");

            return View(flights.ToList());
        }

        [Authorize(Roles = "ClientView")]
        // GET: Flights
        public ActionResult IndexClient()
        {
            var flights = db.Flights.Include(f => f.Airplane).Include(f => f.CountryDestination).Include(f => f.CountryOrigin).Include(f => f.Destination).Include(f => f.Origin).Where(f => f.DepartureTime >= DateTime.Today).OrderBy(f => f.DepartureTime);

            ViewBag.Countries = new SelectList(db.Countries, "CountryID", "CountryName");
            ViewBag.Cities = new SelectList(db.Cities, "CityID", "CityName");

            return View(flights.ToList());
        }

        [Authorize(Roles = "EmployeeView")]
        // GET: Flights
        public ActionResult IndexEmployee()
        {
            var flights = db.Flights.Include(f => f.Airplane).Include(f => f.CountryDestination).Include(f => f.CountryOrigin).Include(f => f.Destination).Include(f => f.Origin);

            ViewBag.Countries = new SelectList(db.Countries, "CountryID", "CountryName");
            ViewBag.Cities = new SelectList(db.Cities, "CityID", "CityName");

            return View(flights.ToList());
        }


        //Get: FlightNumber
        public ActionResult SendFlight(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("BadRequest", "Requests");
            }
            Flight flight = db.Flights.Find(id);
            if (flight == null)
            {
                return RedirectToAction("PageNotFound", "Requests");
            }
            return RedirectToAction("CreateClient", "Tickets", new { id = id});
        }

        //Get: FlightNumber
        public ActionResult SendFlightEmployee(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("BadRequest", "Requests");
            }
            Flight flight = db.Flights.Find(id);
            if (flight == null)
            {
                return RedirectToAction("PageNotFound", "Requests");
            }
            return RedirectToAction("IndexEmployee", "Tickets", new { id = id });
        }

        //Get: FlightNumber
        public ActionResult SendFlightCreateEmployee(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("BadRequest", "Requests");
            }
            Flight flight = db.Flights.Find(id);
            if (flight == null)
            {
                return RedirectToAction("PageNotFound", "Requests");
            }
            return RedirectToAction("CreateEmployee", "Tickets", new { id = id });
        }

        //Get: FlightNumber
        public ActionResult SendFlightAdmin(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("BadRequest", "Requests");
            }
            Flight flight = db.Flights.Find(id);
            if (flight == null)
            {
                return RedirectToAction("PageNotFound", "Requests");
            }
            return RedirectToAction("IndexAdmin", "Tickets", new { id = id });
        }

        //Get: FlightNumber
        public ActionResult SendFlightCreateAdmin(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("BadRequest", "Requests");
            }
            Flight flight = db.Flights.Find(id);
            if (flight == null)
            {
                return RedirectToAction("PageNotFound", "Requests");
            }
            return RedirectToAction("Create", "Tickets", new { id = id });
        }

        [Authorize(Users ="antonio.franco.frazao@formandos.cinel.pt")]
        // GET: Flights/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("BadRequest", "Requests");
            }
            Flight flight = db.Flights.Find(id);
            if (flight == null)
            {
                return RedirectToAction("PageNotFound", "Requests");
            }
            return View(flight);
        }

        //JSON RESULT
        [HttpPost]
        public ActionResult PostCities(int CountryID)
        {
            var cities = db.Cities.Where(c => c.CountryID == CountryID).Select(p => new
            {
                Value = p.CityID,
                Text = p.CityName
            });

            return Json(cities, JsonRequestBehavior.AllowGet);
        }

        //JSON RESULT
        [HttpPost]
        public ActionResult PostSeats(int AirplaneID)
        {
            var seats = db.Airplanes.Where(a => a.AirplaneID == AirplaneID).Select(p => p).FirstOrDefault();

            int seatsNumber = seats.Seats;

            return Json(seatsNumber, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "Create")]
        // GET: Flights/Create  //FAZER UMA TEXTBOX ONDE APARECEM OS LUGARES POR AVIAO COM AJAX E FAZER O METODO GET COORDINATES E MISTURAR COM O LONG RANGE ETC
        public ActionResult Create()
        {
            var PT = (from country in db.Countries where country.CountryName == "Portugal" select country.CountryID).FirstOrDefault();

            List<Country> ListPT = new List<Country>();

            var countryNew = new Country
            {
                CountryID = PT,
                CountryName = "Portugal"
            };

            ListPT.Add(countryNew);

            ViewBag.CountryOriginID = new SelectList(ListPT, "CountryID", "CountryName");

            var LIS = (from city in db.Cities where city.CityName == "Lisbon" select city.CityID).FirstOrDefault();

            List<City> ListLIS = new List<City>();

            var cityNew = new City
            {
                CityID = LIS,
                CityName = "Lisbon"
            };

            ListLIS.Add(cityNew);

            ViewBag.OriginID = new SelectList(ListLIS, "CityID", "CityName");

            ViewBag.AirplaneID = new SelectList(db.Airplanes, "AirplaneID", "AirplaneBrandModel");
            ViewBag.CountryDestinationID = new SelectList(db.Countries, "CountryID", "CountryName");
            ViewBag.DestinationID = new SelectList(db.Cities, "CityID", "CityName");
            return View();
        }

        // POST: Flights/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "FlightID,CountryOriginID,OriginID,CountryDestinationID,DestinationID,AirplaneID,DepartureTime,FlightTotalSeats")] Flight flight)
        {
            var checkCountry = Request["CountryDestinationID"];

            if (checkCountry != "")
            {
                var checkC = int.Parse(Request["CountryDestinationID"]);
            }
            else
            {

                var PT = (from country in db.Countries where country.CountryName == "Portugal" select country.CountryID).FirstOrDefault();

                List<Country> ListPT = new List<Country>();

                var countryNew = new Country
                {
                    CountryID = PT,
                    CountryName = "Portugal"
                };

                ListPT.Add(countryNew);

                ViewBag.CountryOriginID = new SelectList(ListPT, "CountryID", "CountryName");

                var LIS = (from city in db.Cities where city.CityName == "Lisbon" select city.CityID).FirstOrDefault();

                List<City> ListLIS = new List<City>();

                var cityNew = new City
                {
                    CityID = LIS,
                    CityName = "Lisbon"
                };

                ListLIS.Add(cityNew);

                ViewBag.OriginID = new SelectList(ListLIS, "CityID", "CityName");

                ViewBag.AirplaneID = new SelectList(db.Airplanes, "AirplaneID", "AirplaneBrandModel");
                ViewBag.CountryDestinationID = new SelectList(db.Countries, "CountryID", "CountryName");
                ViewBag.DestinationID = new SelectList(db.Cities, "CityID", "CityName");
                ModelState.AddModelError(string.Empty, "You have to choose a Country");
                return View();
            }

            var citycheck = Request["DestinationID"];

            if (citycheck == "" || int.Parse(citycheck) == 0)
            {
                var PT = (from country in db.Countries where country.CountryName == "Portugal" select country.CountryID).FirstOrDefault();

                List<Country> ListPT = new List<Country>();

                var countryNew = new Country
                {
                    CountryID = PT,
                    CountryName = "Portugal"
                };

                ListPT.Add(countryNew);

                ViewBag.CountryOriginID = new SelectList(ListPT, "CountryID", "CountryName");

                var LIS = (from city in db.Cities where city.CityName == "Lisbon" select city.CityID).FirstOrDefault();

                List<City> ListLIS = new List<City>();

                var cityNew = new City
                {
                    CityID = LIS,
                    CityName = "Lisbon"
                };

                ListLIS.Add(cityNew);

                ViewBag.OriginID = new SelectList(ListLIS, "CityID", "CityName");

                ViewBag.AirplaneID = new SelectList(db.Airplanes, "AirplaneID", "AirplaneBrandModel");
                ViewBag.CountryDestinationID = new SelectList(db.Countries, "CountryID", "CountryName");
                ViewBag.DestinationID = new SelectList(db.Cities, "CityID", "CityName");
                ModelState.AddModelError(string.Empty, "You have to choose a City");
                return View();
            }

            if (citycheck == "Lisbon")
            {
                var PT = (from country in db.Countries where country.CountryName == "Portugal" select country.CountryID).FirstOrDefault();

                List<Country> ListPT = new List<Country>();

                var countryNew = new Country
                {
                    CountryID = PT,
                    CountryName = "Portugal"
                };

                ListPT.Add(countryNew);

                ViewBag.CountryOriginID = new SelectList(ListPT, "CountryID", "CountryName");

                var LIS = (from city in db.Cities where city.CityName == "Lisbon" select city.CityID).FirstOrDefault();

                List<City> ListLIS = new List<City>();

                var cityNew = new City
                {
                    CityID = LIS,
                    CityName = "Lisbon"
                };

                ListLIS.Add(cityNew);

                ViewBag.OriginID = new SelectList(ListLIS, "CityID", "CityName");

                ViewBag.AirplaneID = new SelectList(db.Airplanes, "AirplaneID", "AirplaneBrandModel");
                ViewBag.CountryDestinationID = new SelectList(db.Countries, "CountryID", "CountryName");
                ViewBag.DestinationID = new SelectList(db.Cities, "CityID", "CityName");
                ModelState.AddModelError("Date", "You cannot choose Lisbon as a Destination");
                return View(flight);
            }

            var dtime = flight.DepartureTime;

            if (dtime <= DateTime.Now.Date)
            {
                var PT = (from country in db.Countries where country.CountryName == "Portugal" select country.CountryID).FirstOrDefault();

                List<Country> ListPT = new List<Country>();

                var countryNew = new Country
                {
                    CountryID = PT,
                    CountryName = "Portugal"
                };

                ListPT.Add(countryNew);

                ViewBag.CountryOriginID = new SelectList(ListPT, "CountryID", "CountryName");

                var LIS = (from city in db.Cities where city.CityName == "Lisbon" select city.CityID).FirstOrDefault();

                List<City> ListLIS = new List<City>();

                var cityNew = new City
                {
                    CityID = LIS,
                    CityName = "Lisbon"
                };

                ListLIS.Add(cityNew);

                ViewBag.OriginID = new SelectList(ListLIS, "CityID", "CityName");

                ViewBag.AirplaneID = new SelectList(db.Airplanes, "AirplaneID", "AirplaneBrandModel");
                ViewBag.CountryDestinationID = new SelectList(db.Countries, "CountryID", "CountryName");
                ViewBag.DestinationID = new SelectList(db.Cities, "CityID", "CityName");
                ModelState.AddModelError("Date", "The Date cannot be less than actual date!");
                return View(flight);
            }

            var Id = int.Parse(Request["AirplaneID"]);
            var id = db.Airplanes.Find(Id);
            var seats = id.Seats;
            double speed = (from a in db.Airplanes where a.AirplaneID == id.AirplaneID select a.CruiseSpeed).FirstOrDefault();

            double distance;
            var OrigId = "Lisbon";
            var DestiId = int.Parse(Request["DestinationID"]);
            string getOlat = (from c in db.Cities where c.CityName == OrigId select c.Lat).FirstOrDefault();
            string getOlong = (from c in db.Cities where c.CityName == OrigId select c.Long).FirstOrDefault();
            string getAlat = (from c in db.Cities where c.CityID == DestiId select c.Lat).FirstOrDefault();
            string getAlong = (from c in db.Cities where c.CityID == DestiId select c.Long).FirstOrDefault();

            if (getOlat != null && getOlong != null && getAlat != null && getAlong != null)
            {
                var latb = getOlat/*.Replace('.', ',')*/;
                var longb = getOlong/*.Replace('.', ',')*/;
                var lata = getAlat/*.Replace('.', ',')*/;
                var longa = getAlong/*.Replace('.', ',')*/;
                Double.TryParse(latb, out double latitudeB);
                Double.TryParse(longb, out double longitudeB);
                Double.TryParse(lata, out double latitudeA);
                Double.TryParse(longa, out double longitudeA);
                distance = DistanceHelper.calcDistance(latitudeA, longitudeA, latitudeB, longitudeB);
                distance = Math.Round(distance, 2);
            }
            else
            {
                distance = 0;
            }

            var timedistance = distance / speed;

            var date = DateTime.Parse(Request["DepartureTime"]);
            var dayofYear = date.DayOfYear;
            var Time = date.AddHours(timedistance);

            if (Time.DayOfYear != dayofYear)
            {
                flight.ArrivalTime = date.AddHours(timedistance);
            }
            else
            {
                flight.ArrivalTime = date.AddHours(timedistance);
            }

            var returnFlight = new Flight
            {
                CountryOriginID = int.Parse(Request["CountryDestinationID"]),
                OriginID = int.Parse(Request["DestinationID"]),
                CountryDestinationID = int.Parse(Request["CountryOriginID"]),
                DestinationID = int.Parse(Request["OriginID"]),
                AirplaneID = int.Parse(Request["AirplaneID"]),
                DepartureTime = flight.DepartureTime.AddDays(3),
                ArrivalTime = flight.ArrivalTime.AddDays(3),
                FlightTotalSeats = seats,
                FlightAvailableSeats = seats
            };

            if (ModelState.IsValid)
            {
                flight.FlightAvailableSeats = seats;
                db.Flights.Add(flight);
                db.Flights.Add(returnFlight);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.AirplaneID = new SelectList(db.Airplanes, "AirplaneID", "Brand", flight.AirplaneID);
            ViewBag.CountryDestinationID = new SelectList(db.Countries, "CountryID", "CountryName", flight.CountryDestinationID);
            ViewBag.CountryOriginID = new SelectList(db.Countries, "CountryID", "CountryName", flight.CountryOriginID);
            ViewBag.DestinationID = new SelectList(db.Cities, "CityID", "CityName", flight.DestinationID);
            ViewBag.OriginID = new SelectList(db.Cities, "CityID", "CityName", flight.OriginID);
            return View(flight);
        }

        [Authorize(Roles = "EmployeeCreate")]
        // GET: Flights/Create  //FAZER UMA TEXTBOX ONDE APARECEM OS LUGARES POR AVIAO COM AJAX E FAZER O METODO GET COORDINATES E MISTURAR COM O LONG RANGE ETC
        public ActionResult CreateEmployee()
        {
            var PT = (from country in db.Countries where country.CountryName == "Portugal" select country.CountryID).FirstOrDefault();

            List<Country> ListPT = new List<Country>();

            var countryNew = new Country
            {
                CountryID = PT,
                CountryName = "Portugal"
            };

            ListPT.Add(countryNew);

            ViewBag.CountryOriginID = new SelectList(ListPT, "CountryID", "CountryName");

            var LIS = (from city in db.Cities where city.CityName == "Lisbon" select city.CityID).FirstOrDefault();

            List<City> ListLIS = new List<City>();

            var cityNew = new City
            {
                CityID = LIS,
                CityName = "Lisbon"
            };

            ListLIS.Add(cityNew);

            ViewBag.OriginID = new SelectList(ListLIS, "CityID", "CityName");

            ViewBag.AirplaneID = new SelectList(db.Airplanes, "AirplaneID", "AirplaneBrandModel");
            ViewBag.CountryDestinationID = new SelectList(db.Countries, "CountryID", "CountryName");
            ViewBag.DestinationID = new SelectList(db.Cities, "CityID", "CityName");
            return View();
        }

        // POST: Flights/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateEmployee([Bind(Include = "FlightID,CountryOriginID,OriginID,CountryDestinationID,DestinationID,AirplaneID,DepartureTime,FlightTotalSeats")] Flight flight)
        {
            var checkCountry = Request["CountryDestinationID"];

            if (checkCountry != "")
            {
                var checkC = int.Parse(Request["CountryDestinationID"]);
            }
            else
            {

                var PT = (from country in db.Countries where country.CountryName == "Portugal" select country.CountryID).FirstOrDefault();

                List<Country> ListPT = new List<Country>();

                var countryNew = new Country
                {
                    CountryID = PT,
                    CountryName = "Portugal"
                };

                ListPT.Add(countryNew);

                ViewBag.CountryOriginID = new SelectList(ListPT, "CountryID", "CountryName");

                var LIS = (from city in db.Cities where city.CityName == "Lisbon" select city.CityID).FirstOrDefault();

                List<City> ListLIS = new List<City>();

                var cityNew = new City
                {
                    CityID = LIS,
                    CityName = "Lisbon"
                };

                ListLIS.Add(cityNew);

                ViewBag.OriginID = new SelectList(ListLIS, "CityID", "CityName");

                ViewBag.AirplaneID = new SelectList(db.Airplanes, "AirplaneID", "AirplaneBrandModel");
                ViewBag.CountryDestinationID = new SelectList(db.Countries, "CountryID", "CountryName");
                ViewBag.DestinationID = new SelectList(db.Cities, "CityID", "CityName");
                ModelState.AddModelError(string.Empty, "You have to choose a Country");
                return View();
            }

            var citycheck = Request["DestinationID"];

            if (citycheck == "" || int.Parse(citycheck) == 0)
            {
                var PT = (from country in db.Countries where country.CountryName == "Portugal" select country.CountryID).FirstOrDefault();

                List<Country> ListPT = new List<Country>();

                var countryNew = new Country
                {
                    CountryID = PT,
                    CountryName = "Portugal"
                };

                ListPT.Add(countryNew);

                ViewBag.CountryOriginID = new SelectList(ListPT, "CountryID", "CountryName");

                var LIS = (from city in db.Cities where city.CityName == "Lisbon" select city.CityID).FirstOrDefault();

                List<City> ListLIS = new List<City>();

                var cityNew = new City
                {
                    CityID = LIS,
                    CityName = "Lisbon"
                };

                ListLIS.Add(cityNew);

                ViewBag.OriginID = new SelectList(ListLIS, "CityID", "CityName");

                ViewBag.AirplaneID = new SelectList(db.Airplanes, "AirplaneID", "AirplaneBrandModel");
                ViewBag.CountryDestinationID = new SelectList(db.Countries, "CountryID", "CountryName");
                ViewBag.DestinationID = new SelectList(db.Cities, "CityID", "CityName");
                ModelState.AddModelError(string.Empty, "You have to choose a City");
                return View();
            }

            if (citycheck == "Lisbon")
            {
                var PT = (from country in db.Countries where country.CountryName == "Portugal" select country.CountryID).FirstOrDefault();

                List<Country> ListPT = new List<Country>();

                var countryNew = new Country
                {
                    CountryID = PT,
                    CountryName = "Portugal"
                };

                ListPT.Add(countryNew);

                ViewBag.CountryOriginID = new SelectList(ListPT, "CountryID", "CountryName");

                var LIS = (from city in db.Cities where city.CityName == "Lisbon" select city.CityID).FirstOrDefault();

                List<City> ListLIS = new List<City>();

                var cityNew = new City
                {
                    CityID = LIS,
                    CityName = "Lisbon"
                };

                ListLIS.Add(cityNew);

                ViewBag.OriginID = new SelectList(ListLIS, "CityID", "CityName");

                ViewBag.AirplaneID = new SelectList(db.Airplanes, "AirplaneID", "AirplaneBrandModel");
                ViewBag.CountryDestinationID = new SelectList(db.Countries, "CountryID", "CountryName");
                ViewBag.DestinationID = new SelectList(db.Cities, "CityID", "CityName");
                ModelState.AddModelError("Date", "You cannot choose Lisbon as a Destination");
                return View(flight);
            }

            var dtime = flight.DepartureTime;

            if (dtime <= DateTime.Now.Date)
            {
                var PT = (from country in db.Countries where country.CountryName == "Portugal" select country.CountryID).FirstOrDefault();

                List<Country> ListPT = new List<Country>();

                var countryNew = new Country
                {
                    CountryID = PT,
                    CountryName = "Portugal"
                };

                ListPT.Add(countryNew);

                ViewBag.CountryOriginID = new SelectList(ListPT, "CountryID", "CountryName");

                var LIS = (from city in db.Cities where city.CityName == "Lisbon" select city.CityID).FirstOrDefault();

                List<City> ListLIS = new List<City>();

                var cityNew = new City
                {
                    CityID = LIS,
                    CityName = "Lisbon"
                };

                ListLIS.Add(cityNew);

                ViewBag.OriginID = new SelectList(ListLIS, "CityID", "CityName");

                ViewBag.AirplaneID = new SelectList(db.Airplanes, "AirplaneID", "AirplaneBrandModel");
                ViewBag.CountryDestinationID = new SelectList(db.Countries, "CountryID", "CountryName");
                ViewBag.DestinationID = new SelectList(db.Cities, "CityID", "CityName");
                ModelState.AddModelError("Date", "The Date cannot be less than actual date!");
                return View(flight);
            }

            var Id = int.Parse(Request["AirplaneID"]);
            var id = db.Airplanes.Find(Id);
            var seats = id.Seats;
            double speed = (from a in db.Airplanes where a.AirplaneID == id.AirplaneID select a.CruiseSpeed).FirstOrDefault();

            double distance;
            var OrigId = "Lisbon";
            var DestiId = int.Parse(Request["DestinationID"]);
            string getOlat = (from c in db.Cities where c.CityName == OrigId select c.Lat).FirstOrDefault();
            string getOlong = (from c in db.Cities where c.CityName == OrigId select c.Long).FirstOrDefault();
            string getAlat = (from c in db.Cities where c.CityID == DestiId select c.Lat).FirstOrDefault();
            string getAlong = (from c in db.Cities where c.CityID == DestiId select c.Long).FirstOrDefault();

            if (getOlat != null && getOlong != null && getAlat != null && getAlong != null)
            {
                var latb = getOlat/*.Replace('.', ',')*/;
                var longb = getOlong/*.Replace('.', ',')*/;
                var lata = getAlat/*.Replace('.', ',')*/;
                var longa = getAlong/*.Replace('.', ',')*/;
                Double.TryParse(latb, out double latitudeB);
                Double.TryParse(longb, out double longitudeB);
                Double.TryParse(lata, out double latitudeA);
                Double.TryParse(longa, out double longitudeA);
                distance = DistanceHelper.calcDistance(latitudeA, longitudeA, latitudeB, longitudeB);
                distance = Math.Round(distance, 2);
            }
            else
            {
                distance = 0;
            }

            var timedistance = distance / speed;

            var date = DateTime.Parse(Request["DepartureTime"]);
            var dayofYear = date.DayOfYear;
            var Time = date.AddHours(timedistance);

            if (Time.DayOfYear != dayofYear)
            {
                flight.ArrivalTime = date.AddHours(timedistance);
            }
            else
            {
                flight.ArrivalTime = date.AddHours(timedistance);
            }

            var returnFlight = new Flight
            {
                CountryOriginID = int.Parse(Request["CountryDestinationID"]),
                OriginID = int.Parse(Request["DestinationID"]),
                CountryDestinationID = int.Parse(Request["CountryOriginID"]),
                DestinationID = int.Parse(Request["OriginID"]),
                AirplaneID = int.Parse(Request["AirplaneID"]),
                DepartureTime = flight.DepartureTime.AddDays(3),
                ArrivalTime = flight.ArrivalTime.AddDays(3),
                FlightTotalSeats = seats,
                FlightAvailableSeats = seats
            };

            if (ModelState.IsValid)
            {
                flight.FlightAvailableSeats = seats;
                db.Flights.Add(flight);
                db.Flights.Add(returnFlight);
                db.SaveChanges();
                return RedirectToAction("IndexEmployee");
            }

            ViewBag.AirplaneID = new SelectList(db.Airplanes, "AirplaneID", "Brand", flight.AirplaneID);
            ViewBag.CountryDestinationID = new SelectList(db.Countries, "CountryID", "CountryName", flight.CountryDestinationID);
            ViewBag.CountryOriginID = new SelectList(db.Countries, "CountryID", "CountryName", flight.CountryOriginID);
            ViewBag.DestinationID = new SelectList(db.Cities, "CityID", "CityName", flight.DestinationID);
            ViewBag.OriginID = new SelectList(db.Cities, "CityID", "CityName", flight.OriginID);
            return View(flight);
        }

        [Authorize(Roles = "Edit")]
        // GET: Flights/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("BadRequest", "Requests");
            }
            Flight flight = db.Flights.Find(id);
            if (flight == null)
            {
                return RedirectToAction("PageNotFound", "Requests");
            }

            if (id % 2 == 0)
            {
                int PlaneId = flight.AirplaneID;

                var PT = (from country in db.Countries where country.CountryName == "Portugal" select country.CountryID).FirstOrDefault();

                List<Country> ListPT = new List<Country>();

                var countryNew = new Country
                {
                    CountryID = PT,
                    CountryName = "Portugal"
                };

                ListPT.Add(countryNew);

                ViewBag.CountryDestinationID = new SelectList(ListPT, "CountryID", "CountryName");

                var LIS = (from city in db.Cities where city.CityName == "Lisbon" select city.CityID).FirstOrDefault();

                List<City> ListLIS = new List<City>();

                var cityNew = new City
                {
                    CityID = LIS,
                    CityName = "Lisbon"
                };

                ListLIS.Add(cityNew);

                ViewBag.DestinationID = new SelectList(ListLIS, "CityID", "CityName");

                var DestCountry = (from country in db.Countries where country.CountryID == flight.CountryOriginID select country).FirstOrDefault();

                List<Country> ListCountryDest = new List<Country>();

                var countryDestNew = new Country
                {
                    CountryID = DestCountry.CountryID,
                    CountryName = DestCountry.CountryName
                };

                ListCountryDest.Add(countryDestNew);

                ViewBag.CountryOriginID = new SelectList(ListCountryDest, "CountryID", "CountryName");

                var DesCity = (from city in db.Cities where city.CityID == flight.OriginID select city).FirstOrDefault();

                List<City> ListDestCity = new List<City>();

                var cityDestNew = new City
                {
                    CityID = DesCity.CityID,
                    CityName = DesCity.CityName
                };

                ListDestCity.Add(cityDestNew);

                ViewBag.OriginID = new SelectList(ListDestCity, "CityID", "CityName");


                var PID = (from p in db.Airplanes where p.AirplaneID == flight.AirplaneID select p).FirstOrDefault();

                List<Airplane> ListPID = new List<Airplane>();

                var planeNew = new Airplane
                {
                    AirplaneID = PID.AirplaneID,
                    Brand = PID.AirplaneBrandModel
                };

                ListPID.Add(planeNew);

                ViewBag.AirplaneID = new SelectList(ListPID, "AirplaneID", "Brand");

                return View(flight);
            }
            else
            {
                int PlaneId = flight.AirplaneID;

                var PT = (from country in db.Countries where country.CountryName == "Portugal" select country.CountryID).FirstOrDefault();

                List<Country> ListPT = new List<Country>();

                var countryNew = new Country
                {
                    CountryID = PT,
                    CountryName = "Portugal"
                };

                ListPT.Add(countryNew);

                ViewBag.CountryOriginID = new SelectList(ListPT, "CountryID", "CountryName");

                var LIS = (from city in db.Cities where city.CityName == "Lisbon" select city.CityID).FirstOrDefault();

                List<City> ListLIS = new List<City>();

                var cityNew = new City
                {
                    CityID = LIS,
                    CityName = "Lisbon"
                };

                ListLIS.Add(cityNew);

                ViewBag.OriginID = new SelectList(ListLIS, "CityID", "CityName");

                var DestCountry = (from country in db.Countries where country.CountryID == flight.CountryDestinationID select country).FirstOrDefault();

                List<Country> ListCountryDest = new List<Country>();

                var countryDestNew = new Country
                {
                    CountryID = DestCountry.CountryID,
                    CountryName = DestCountry.CountryName
                };

                ListCountryDest.Add(countryDestNew);

                ViewBag.CountryDestinationID = new SelectList(ListCountryDest, "CountryID", "CountryName");

                var DesCity = (from city in db.Cities where city.CityID == flight.DestinationID select city).FirstOrDefault();

                List<City> ListDestCity = new List<City>();

                var cityDestNew = new City
                {
                    CityID = DesCity.CityID,
                    CityName = DesCity.CityName
                };

                ListDestCity.Add(cityDestNew);

                ViewBag.DestinationID = new SelectList(ListDestCity, "CityID", "CityName");


                var PID = (from p in db.Airplanes where p.AirplaneID == flight.AirplaneID select p).FirstOrDefault();

                List<Airplane> ListPID = new List<Airplane>();

                var planeNew = new Airplane
                {
                    AirplaneID = PID.AirplaneID,
                    Brand = PID.AirplaneBrandModel
                };

                ListPID.Add(planeNew);

                ViewBag.AirplaneID = new SelectList(ListPID, "AirplaneID", "Brand");

                return View(flight);
            }
            
        }

        // POST: Flights/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "FlightID,CountryOriginID,OriginID,CountryDestinationID,DestinationID,AirplaneID,DepartureTime,ArrivalTime,FlightTotalSeats,FlightAvailableSeats")] Flight flight)
        {

            var Id = int.Parse(Request["AirplaneID"]);
            var idP = db.Airplanes.Find(Id);
            var seats = idP.Seats;
            double speed = (from a in db.Airplanes where a.AirplaneID == idP.AirplaneID select a.CruiseSpeed).FirstOrDefault();

            double distance;
            string OrigId = (from c in db.Cities where c.CityID == flight.OriginID select c.CityName).FirstOrDefault();
            string DestiId = (from c in db.Cities where c.CityID == flight.DestinationID select c.CityName).FirstOrDefault();
            string getOlat = (from c in db.Cities where c.CityName == OrigId select c.Lat).FirstOrDefault();
            string getOlong = (from c in db.Cities where c.CityName == OrigId select c.Long).FirstOrDefault();
            string getAlat = (from c in db.Cities where c.CityName == DestiId select c.Lat).FirstOrDefault();
            string getAlong = (from c in db.Cities where c.CityName == DestiId select c.Long).FirstOrDefault();

            if (getOlat != null && getOlong != null && getAlat != null && getAlong != null)
            {
                var latb = getOlat/*.Replace('.', ',')*/;
                var longb = getOlong/*.Replace('.', ',')*/;
                var lata = getAlat/*.Replace('.', ',')*/;
                var longa = getAlong/*.Replace('.', ',')*/;
                Double.TryParse(latb, out double latitudeB);
                Double.TryParse(longb, out double longitudeB);
                Double.TryParse(lata, out double latitudeA);
                Double.TryParse(longa, out double longitudeA);
                distance = DistanceHelper.calcDistance(latitudeA, longitudeA, latitudeB, longitudeB);
                distance = Math.Round(distance, 2);
            }
            else
            {
                distance = 0;
            }

            var timedistance = distance / speed;

            var dateCheck = Request["DepartureTime"];

            if (dateCheck != "")
            {
                var date = DateTime.Parse(Request["DepartureTime"]);

                if (date <= DateTime.Today.Date)
                {
                    if (flight.FlightID % 2 == 0)
                    {
                        int PlaneId = flight.AirplaneID;

                        var PT = (from country in db.Countries where country.CountryName == "Portugal" select country.CountryID).FirstOrDefault();

                        List<Country> ListPT = new List<Country>();

                        var countryNew = new Country
                        {
                            CountryID = PT,
                            CountryName = "Portugal"
                        };

                        ListPT.Add(countryNew);

                        ViewBag.CountryDestinationID = new SelectList(ListPT, "CountryID", "CountryName");

                        var LIS = (from city in db.Cities where city.CityName == "Lisbon" select city.CityID).FirstOrDefault();

                        List<City> ListLIS = new List<City>();

                        var cityNew = new City
                        {
                            CityID = LIS,
                            CityName = "Lisbon"
                        };

                        ListLIS.Add(cityNew);

                        ViewBag.DestinationID = new SelectList(ListLIS, "CityID", "CityName");

                        var DestCountry = (from country in db.Countries where country.CountryID == flight.CountryOriginID select country).FirstOrDefault();

                        List<Country> ListCountryDest = new List<Country>();

                        var countryDestNew = new Country
                        {
                            CountryID = DestCountry.CountryID,
                            CountryName = DestCountry.CountryName
                        };

                        ListCountryDest.Add(countryDestNew);

                        ViewBag.CountryOriginID = new SelectList(ListCountryDest, "CountryID", "CountryName");

                        var DesCity = (from city in db.Cities where city.CityID == flight.OriginID select city).FirstOrDefault();

                        List<City> ListDestCity = new List<City>();

                        var cityDestNew = new City
                        {
                            CityID = DesCity.CityID,
                            CityName = DesCity.CityName
                        };

                        ListDestCity.Add(cityDestNew);

                        ViewBag.OriginID = new SelectList(ListDestCity, "CityID", "CityName");


                        var PID = (from p in db.Airplanes where p.AirplaneID == flight.AirplaneID select p).FirstOrDefault();

                        List<Airplane> ListPID = new List<Airplane>();

                        var planeNew = new Airplane
                        {
                            AirplaneID = PID.AirplaneID,
                            Brand = PID.AirplaneBrandModel
                        };

                        ListPID.Add(planeNew);

                        ViewBag.AirplaneID = new SelectList(ListPID, "AirplaneID", "Brand");

                        ModelState.AddModelError("Departure Time", "The Date has to be greater than today");
                        return View(flight);
                    }
                    else
                    {
                        int PlaneId = flight.AirplaneID;

                        var PT = (from country in db.Countries where country.CountryName == "Portugal" select country.CountryID).FirstOrDefault();

                        List<Country> ListPT = new List<Country>();

                        var countryNew = new Country
                        {
                            CountryID = PT,
                            CountryName = "Portugal"
                        };

                        ListPT.Add(countryNew);

                        ViewBag.CountryOriginID = new SelectList(ListPT, "CountryID", "CountryName");

                        var LIS = (from city in db.Cities where city.CityName == "Lisbon" select city.CityID).FirstOrDefault();

                        List<City> ListLIS = new List<City>();

                        var cityNew = new City
                        {
                            CityID = LIS,
                            CityName = "Lisbon"
                        };

                        ListLIS.Add(cityNew);

                        ViewBag.OriginID = new SelectList(ListLIS, "CityID", "CityName");

                        var DestCountry = (from country in db.Countries where country.CountryID == flight.CountryDestinationID select country).FirstOrDefault();

                        List<Country> ListCountryDest = new List<Country>();

                        var countryDestNew = new Country
                        {
                            CountryID = DestCountry.CountryID,
                            CountryName = DestCountry.CountryName
                        };

                        ListCountryDest.Add(countryDestNew);

                        ViewBag.CountryDestinationID = new SelectList(ListCountryDest, "CountryID", "CountryName");

                        var DesCity = (from city in db.Cities where city.CityID == flight.DestinationID select city).FirstOrDefault();

                        List<City> ListDestCity = new List<City>();

                        var cityDestNew = new City
                        {
                            CityID = DesCity.CityID,
                            CityName = DesCity.CityName
                        };

                        ListDestCity.Add(cityDestNew);

                        ViewBag.DestinationID = new SelectList(ListDestCity, "CityID", "CityName");


                        var PID = (from p in db.Airplanes where p.AirplaneID == flight.AirplaneID select p).FirstOrDefault();

                        List<Airplane> ListPID = new List<Airplane>();

                        var planeNew = new Airplane
                        {
                            AirplaneID = PID.AirplaneID,
                            Brand = PID.AirplaneBrandModel
                        };
                        
                        ListPID.Add(planeNew);

                        ViewBag.AirplaneID = new SelectList(ListPID, "AirplaneID", "Brand");

                        ModelState.AddModelError("Departure Time", "The Date has to be greater than today");
                        return View(flight);
                    }
                    
                }
                else
                {
                    var dayofYear = date.DayOfYear;
                    var Time = date.AddHours(timedistance);

                    if (Time.DayOfYear != dayofYear)
                    {
                        flight.ArrivalTime = date.AddHours(timedistance);
                    }
                    else
                    {
                        flight.ArrivalTime = date.AddHours(timedistance);
                    }
                }
            }
            else
            {
                if (flight.FlightID % 2 == 0)
                {
                    int PlaneId = flight.AirplaneID;

                    var PT = (from country in db.Countries where country.CountryName == "Portugal" select country.CountryID).FirstOrDefault();

                    List<Country> ListPT = new List<Country>();

                    var countryNew = new Country
                    {
                        CountryID = PT,
                        CountryName = "Portugal"
                    };

                    ListPT.Add(countryNew);

                    ViewBag.CountryDestinationID = new SelectList(ListPT, "CountryID", "CountryName");

                    var LIS = (from city in db.Cities where city.CityName == "Lisbon" select city.CityID).FirstOrDefault();

                    List<City> ListLIS = new List<City>();

                    var cityNew = new City
                    {
                        CityID = LIS,
                        CityName = "Lisbon"
                    };

                    ListLIS.Add(cityNew);

                    ViewBag.DestinationID = new SelectList(ListLIS, "CityID", "CityName");

                    var DestCountry = (from country in db.Countries where country.CountryID == flight.CountryOriginID select country).FirstOrDefault();

                    List<Country> ListCountryDest = new List<Country>();

                    var countryDestNew = new Country
                    {
                        CountryID = DestCountry.CountryID,
                        CountryName = DestCountry.CountryName
                    };

                    ListCountryDest.Add(countryDestNew);

                    ViewBag.CountryOriginID = new SelectList(ListCountryDest, "CountryID", "CountryName");

                    var DesCity = (from city in db.Cities where city.CityID == flight.OriginID select city).FirstOrDefault();

                    List<City> ListDestCity = new List<City>();

                    var cityDestNew = new City
                    {
                        CityID = DesCity.CityID,
                        CityName = DesCity.CityName
                    };

                    ListDestCity.Add(cityDestNew);

                    ViewBag.OriginID = new SelectList(ListDestCity, "CityID", "CityName");


                    var PID = (from p in db.Airplanes where p.AirplaneID == flight.AirplaneID select p).FirstOrDefault();

                    List<Airplane> ListPID = new List<Airplane>();

                    var planeNew = new Airplane
                    {
                        AirplaneID = PID.AirplaneID,
                        Brand = PID.AirplaneBrandModel
                    };

                    ListPID.Add(planeNew);

                    ViewBag.AirplaneID = new SelectList(ListPID, "AirplaneID", "Brand");

                    return View(flight);
                }
                else
                {
                    int PlaneId = flight.AirplaneID;

                    var PT = (from country in db.Countries where country.CountryName == "Portugal" select country.CountryID).FirstOrDefault();

                    List<Country> ListPT = new List<Country>();

                    var countryNew = new Country
                    {
                        CountryID = PT,
                        CountryName = "Portugal"
                    };

                    ListPT.Add(countryNew);

                    ViewBag.CountryOriginID = new SelectList(ListPT, "CountryID", "CountryName");

                    var LIS = (from city in db.Cities where city.CityName == "Lisbon" select city.CityID).FirstOrDefault();

                    List<City> ListLIS = new List<City>();

                    var cityNew = new City
                    {
                        CityID = LIS,
                        CityName = "Lisbon"
                    };

                    ListLIS.Add(cityNew);

                    ViewBag.OriginID = new SelectList(ListLIS, "CityID", "CityName");

                    var DestCountry = (from country in db.Countries where country.CountryID == flight.CountryDestinationID select country).FirstOrDefault();

                    List<Country> ListCountryDest = new List<Country>();

                    var countryDestNew = new Country
                    {
                        CountryID = DestCountry.CountryID,
                        CountryName = DestCountry.CountryName
                    };

                    ListCountryDest.Add(countryDestNew);

                    ViewBag.CountryDestinationID = new SelectList(ListCountryDest, "CountryID", "CountryName");

                    var DesCity = (from city in db.Cities where city.CityID == flight.DestinationID select city).FirstOrDefault();

                    List<City> ListDestCity = new List<City>();

                    var cityDestNew = new City
                    {
                        CityID = DesCity.CityID,
                        CityName = DesCity.CityName
                    };

                    ListDestCity.Add(cityDestNew);

                    ViewBag.DestinationID = new SelectList(ListDestCity, "CityID", "CityName");


                    var PID = (from p in db.Airplanes where p.AirplaneID == flight.AirplaneID select p).FirstOrDefault();

                    List<Airplane> ListPID = new List<Airplane>();

                    var planeNew = new Airplane
                    {
                        AirplaneID = PID.AirplaneID,
                        Brand = PID.AirplaneBrandModel
                    };

                    ListPID.Add(planeNew);

                    ViewBag.AirplaneID = new SelectList(ListPID, "AirplaneID", "Brand");

                }

                ModelState.AddModelError("DepartureTime", "You have to choose a valid Date");
                return View(flight);
            }

            if (ModelState.IsValid)
            {
                db.Flights.Attach(flight);
                flight.DepartureTime = DateTime.Parse(dateCheck);
                flight.ArrivalTime = flight.ArrivalTime;
                int count = (from t in db.Tickets where t.FlightID == flight.FlightID select t).Count();
                flight.FlightAvailableSeats = flight.FlightTotalSeats - count;
                db.Entry(flight).State = EntityState.Modified;
                db.SaveChanges();

                if (flight.FlightID % 2 == 0)
                {
                    Flight otherFlight = db.Flights.Find(flight.FlightID - 1);
                    db.Flights.Attach(otherFlight);
                    DateTime backD = flight.DepartureTime - TimeSpan.FromDays(3);
                    otherFlight.DepartureTime = backD;
                    DateTime backA = flight.ArrivalTime - TimeSpan.FromDays(3);
                    otherFlight.ArrivalTime = backA;
                    int countOtherEven = (from t in db.Tickets where t.FlightID == otherFlight.FlightID select t).Count();
                    otherFlight.FlightAvailableSeats = otherFlight.FlightTotalSeats - countOtherEven;
                    db.Entry(otherFlight).State = EntityState.Modified;
                    db.SaveChanges();
                }
                else
                {
                    Flight otherFlight = db.Flights.Find(flight.FlightID + 1);
                    db.Flights.Attach(otherFlight);
                    otherFlight.DepartureTime = flight.DepartureTime.AddDays(3);
                    otherFlight.ArrivalTime = flight.ArrivalTime.AddDays(3);
                    int countOtherOdd = (from t in db.Tickets where t.FlightID == otherFlight.FlightID select t).Count();
                    otherFlight.FlightAvailableSeats = otherFlight.FlightTotalSeats - countOtherOdd;
                    db.Entry(otherFlight).State = EntityState.Modified;
                    db.SaveChanges();
                }
                return RedirectToAction("Index");
            }
            if (flight.FlightID % 2 == 0)
            {
                int PlaneId = flight.AirplaneID;

                var PT = (from country in db.Countries where country.CountryName == "Portugal" select country.CountryID).FirstOrDefault();

                List<Country> ListPT = new List<Country>();

                var countryNew = new Country
                {
                    CountryID = PT,
                    CountryName = "Portugal"
                };

                ListPT.Add(countryNew);

                ViewBag.CountryDestinationID = new SelectList(ListPT, "CountryID", "CountryName");

                var LIS = (from city in db.Cities where city.CityName == "Lisbon" select city.CityID).FirstOrDefault();

                List<City> ListLIS = new List<City>();

                var cityNew = new City
                {
                    CityID = LIS,
                    CityName = "Lisbon"
                };

                ListLIS.Add(cityNew);

                ViewBag.DestinationID = new SelectList(ListLIS, "CityID", "CityName");

                var DestCountry = (from country in db.Countries where country.CountryID == flight.CountryOriginID select country).FirstOrDefault();

                List<Country> ListCountryDest = new List<Country>();

                var countryDestNew = new Country
                {
                    CountryID = DestCountry.CountryID,
                    CountryName = DestCountry.CountryName
                };

                ListCountryDest.Add(countryDestNew);

                ViewBag.CountryOriginID = new SelectList(ListCountryDest, "CountryID", "CountryName");

                var DesCity = (from city in db.Cities where city.CityID == flight.OriginID select city).FirstOrDefault();

                List<City> ListDestCity = new List<City>();

                var cityDestNew = new City
                {
                    CityID = DesCity.CityID,
                    CityName = DesCity.CityName
                };

                ListDestCity.Add(cityDestNew);

                ViewBag.OriginID = new SelectList(ListDestCity, "CityID", "CityName");


                var PID = (from p in db.Airplanes where p.AirplaneID == flight.AirplaneID select p).FirstOrDefault();

                List<Airplane> ListPID = new List<Airplane>();

                var planeNew = new Airplane
                {
                    AirplaneID = PID.AirplaneID,
                    Brand = PID.AirplaneBrandModel
                };

                ListPID.Add(planeNew);

                ViewBag.AirplaneID = new SelectList(ListPID, "AirplaneID", "Brand");

                return View(flight);
            }
            else
            {
                int PlaneId = flight.AirplaneID;

                var PT = (from country in db.Countries where country.CountryName == "Portugal" select country.CountryID).FirstOrDefault();

                List<Country> ListPT = new List<Country>();

                var countryNew = new Country
                {
                    CountryID = PT,
                    CountryName = "Portugal"
                };

                ListPT.Add(countryNew);

                ViewBag.CountryOriginID = new SelectList(ListPT, "CountryID", "CountryName");

                var LIS = (from city in db.Cities where city.CityName == "Lisbon" select city.CityID).FirstOrDefault();

                List<City> ListLIS = new List<City>();

                var cityNew = new City
                {
                    CityID = LIS,
                    CityName = "Lisbon"
                };

                ListLIS.Add(cityNew);

                ViewBag.OriginID = new SelectList(ListLIS, "CityID", "CityName");

                var DestCountry = (from country in db.Countries where country.CountryID == flight.CountryDestinationID select country).FirstOrDefault();

                List<Country> ListCountryDest = new List<Country>();

                var countryDestNew = new Country
                {
                    CountryID = DestCountry.CountryID,
                    CountryName = DestCountry.CountryName
                };

                ListCountryDest.Add(countryDestNew);

                ViewBag.CountryDestinationID = new SelectList(ListCountryDest, "CountryID", "CountryName");

                var DesCity = (from city in db.Cities where city.CityID == flight.DestinationID select city).FirstOrDefault();

                List<City> ListDestCity = new List<City>();

                var cityDestNew = new City
                {
                    CityID = DesCity.CityID,
                    CityName = DesCity.CityName
                };

                ListDestCity.Add(cityDestNew);

                ViewBag.DestinationID = new SelectList(ListDestCity, "CityID", "CityName");


                var PID = (from p in db.Airplanes where p.AirplaneID == flight.AirplaneID select p).FirstOrDefault();

                List<Airplane> ListPID = new List<Airplane>();

                var planeNew = new Airplane
                {
                    AirplaneID = PID.AirplaneID,
                    Brand = PID.AirplaneBrandModel
                };

                ListPID.Add(planeNew);

                ViewBag.AirplaneID = new SelectList(ListPID, "AirplaneID", "Brand");

                return View(flight);
            }
        }

        [Authorize(Roles = "EmployeeEdit")]
        // GET: Flights/Edit/5
        public ActionResult EditEmployee(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("BadRequest", "Requests");
            }
            Flight flight = db.Flights.Find(id);
            if (flight == null)
            {
                return RedirectToAction("PageNotFound", "Requests");
            }

            if (id % 2 == 0)
            {
                int PlaneId = flight.AirplaneID;

                var PT = (from country in db.Countries where country.CountryName == "Portugal" select country.CountryID).FirstOrDefault();

                List<Country> ListPT = new List<Country>();

                var countryNew = new Country
                {
                    CountryID = PT,
                    CountryName = "Portugal"
                };

                ListPT.Add(countryNew);

                ViewBag.CountryDestinationID = new SelectList(ListPT, "CountryID", "CountryName");

                var LIS = (from city in db.Cities where city.CityName == "Lisbon" select city.CityID).FirstOrDefault();

                List<City> ListLIS = new List<City>();

                var cityNew = new City
                {
                    CityID = LIS,
                    CityName = "Lisbon"
                };

                ListLIS.Add(cityNew);

                ViewBag.DestinationID = new SelectList(ListLIS, "CityID", "CityName");

                var DestCountry = (from country in db.Countries where country.CountryID == flight.CountryOriginID select country).FirstOrDefault();

                List<Country> ListCountryDest = new List<Country>();

                var countryDestNew = new Country
                {
                    CountryID = DestCountry.CountryID,
                    CountryName = DestCountry.CountryName
                };

                ListCountryDest.Add(countryDestNew);

                ViewBag.CountryOriginID = new SelectList(ListCountryDest, "CountryID", "CountryName");

                var DesCity = (from city in db.Cities where city.CityID == flight.OriginID select city).FirstOrDefault();

                List<City> ListDestCity = new List<City>();

                var cityDestNew = new City
                {
                    CityID = DesCity.CityID,
                    CityName = DesCity.CityName
                };

                ListDestCity.Add(cityDestNew);

                ViewBag.OriginID = new SelectList(ListDestCity, "CityID", "CityName");


                var PID = (from p in db.Airplanes where p.AirplaneID == flight.AirplaneID select p).FirstOrDefault();

                List<Airplane> ListPID = new List<Airplane>();

                var planeNew = new Airplane
                {
                    AirplaneID = PID.AirplaneID,
                    Brand = PID.AirplaneBrandModel
                };

                ListPID.Add(planeNew);

                ViewBag.AirplaneID = new SelectList(ListPID, "AirplaneID", "Brand");

                return View(flight);
            }
            else
            {
                int PlaneId = flight.AirplaneID;

                var PT = (from country in db.Countries where country.CountryName == "Portugal" select country.CountryID).FirstOrDefault();

                List<Country> ListPT = new List<Country>();

                var countryNew = new Country
                {
                    CountryID = PT,
                    CountryName = "Portugal"
                };

                ListPT.Add(countryNew);

                ViewBag.CountryOriginID = new SelectList(ListPT, "CountryID", "CountryName");

                var LIS = (from city in db.Cities where city.CityName == "Lisbon" select city.CityID).FirstOrDefault();

                List<City> ListLIS = new List<City>();

                var cityNew = new City
                {
                    CityID = LIS,
                    CityName = "Lisbon"
                };

                ListLIS.Add(cityNew);

                ViewBag.OriginID = new SelectList(ListLIS, "CityID", "CityName");

                var DestCountry = (from country in db.Countries where country.CountryID == flight.CountryDestinationID select country).FirstOrDefault();

                List<Country> ListCountryDest = new List<Country>();

                var countryDestNew = new Country
                {
                    CountryID = DestCountry.CountryID,
                    CountryName = DestCountry.CountryName
                };

                ListCountryDest.Add(countryDestNew);

                ViewBag.CountryDestinationID = new SelectList(ListCountryDest, "CountryID", "CountryName");

                var DesCity = (from city in db.Cities where city.CityID == flight.DestinationID select city).FirstOrDefault();

                List<City> ListDestCity = new List<City>();

                var cityDestNew = new City
                {
                    CityID = DesCity.CityID,
                    CityName = DesCity.CityName
                };

                ListDestCity.Add(cityDestNew);

                ViewBag.DestinationID = new SelectList(ListDestCity, "CityID", "CityName");


                var PID = (from p in db.Airplanes where p.AirplaneID == flight.AirplaneID select p).FirstOrDefault();

                List<Airplane> ListPID = new List<Airplane>();

                var planeNew = new Airplane
                {
                    AirplaneID = PID.AirplaneID,
                    Brand = PID.AirplaneBrandModel
                };

                ListPID.Add(planeNew);

                ViewBag.AirplaneID = new SelectList(ListPID, "AirplaneID", "Brand");

                return View(flight);
            }
        }

        // POST: Flights/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditEmployee([Bind(Include = "FlightID,CountryOriginID,OriginID,CountryDestinationID,DestinationID,AirplaneID,DepartureTime,ArrivalTime,FlightTotalSeats,FlightAvailableSeats")] Flight flight)
        {
            var Id = int.Parse(Request["AirplaneID"]);
            var idP = db.Airplanes.Find(Id);
            var seats = idP.Seats;
            double speed = (from a in db.Airplanes where a.AirplaneID == idP.AirplaneID select a.CruiseSpeed).FirstOrDefault();

            double distance;
            string OrigId = (from c in db.Cities where c.CityID == flight.OriginID select c.CityName).FirstOrDefault();
            string DestiId = (from c in db.Cities where c.CityID == flight.DestinationID select c.CityName).FirstOrDefault();
            string getOlat = (from c in db.Cities where c.CityName == OrigId select c.Lat).FirstOrDefault();
            string getOlong = (from c in db.Cities where c.CityName == OrigId select c.Long).FirstOrDefault();
            string getAlat = (from c in db.Cities where c.CityName == DestiId select c.Lat).FirstOrDefault();
            string getAlong = (from c in db.Cities where c.CityName == DestiId select c.Long).FirstOrDefault();

            if (getOlat != null && getOlong != null && getAlat != null && getAlong != null)
            {
                var latb = getOlat/*.Replace('.', ',')*/;
                var longb = getOlong/*.Replace('.', ',')*/;
                var lata = getAlat/*.Replace('.', ',')*/;
                var longa = getAlong/*.Replace('.', ',')*/;
                Double.TryParse(latb, out double latitudeB);
                Double.TryParse(longb, out double longitudeB);
                Double.TryParse(lata, out double latitudeA);
                Double.TryParse(longa, out double longitudeA);
                distance = DistanceHelper.calcDistance(latitudeA, longitudeA, latitudeB, longitudeB);
                distance = Math.Round(distance, 2);
            }
            else
            {
                distance = 0;
            }

            var timedistance = distance / speed;

            var dateCheck = Request["DepartureTime"];

            if (dateCheck != "")
            {
                var date = DateTime.Parse(Request["DepartureTime"]);

                if (date <= DateTime.Today.Date)
                {
                    if (flight.FlightID % 2 == 0)
                    {
                        int PlaneId = flight.AirplaneID;

                        var PT = (from country in db.Countries where country.CountryName == "Portugal" select country.CountryID).FirstOrDefault();

                        List<Country> ListPT = new List<Country>();

                        var countryNew = new Country
                        {
                            CountryID = PT,
                            CountryName = "Portugal"
                        };

                        ListPT.Add(countryNew);

                        ViewBag.CountryDestinationID = new SelectList(ListPT, "CountryID", "CountryName");

                        var LIS = (from city in db.Cities where city.CityName == "Lisbon" select city.CityID).FirstOrDefault();

                        List<City> ListLIS = new List<City>();

                        var cityNew = new City
                        {
                            CityID = LIS,
                            CityName = "Lisbon"
                        };

                        ListLIS.Add(cityNew);

                        ViewBag.DestinationID = new SelectList(ListLIS, "CityID", "CityName");

                        var DestCountry = (from country in db.Countries where country.CountryID == flight.CountryOriginID select country).FirstOrDefault();

                        List<Country> ListCountryDest = new List<Country>();

                        var countryDestNew = new Country
                        {
                            CountryID = DestCountry.CountryID,
                            CountryName = DestCountry.CountryName
                        };

                        ListCountryDest.Add(countryDestNew);

                        ViewBag.CountryOriginID = new SelectList(ListCountryDest, "CountryID", "CountryName");

                        var DesCity = (from city in db.Cities where city.CityID == flight.OriginID select city).FirstOrDefault();

                        List<City> ListDestCity = new List<City>();

                        var cityDestNew = new City
                        {
                            CityID = DesCity.CityID,
                            CityName = DesCity.CityName
                        };

                        ListDestCity.Add(cityDestNew);

                        ViewBag.OriginID = new SelectList(ListDestCity, "CityID", "CityName");


                        var PID = (from p in db.Airplanes where p.AirplaneID == flight.AirplaneID select p).FirstOrDefault();

                        List<Airplane> ListPID = new List<Airplane>();

                        var planeNew = new Airplane
                        {
                            AirplaneID = PID.AirplaneID,
                            Brand = PID.AirplaneBrandModel
                        };

                        ListPID.Add(planeNew);

                        ViewBag.AirplaneID = new SelectList(ListPID, "AirplaneID", "Brand");

                        ModelState.AddModelError("Departure Time", "The Date has to be greater than today");
                        return View(flight);
                    }
                    else
                    {
                        int PlaneId = flight.AirplaneID;

                        var PT = (from country in db.Countries where country.CountryName == "Portugal" select country.CountryID).FirstOrDefault();

                        List<Country> ListPT = new List<Country>();

                        var countryNew = new Country
                        {
                            CountryID = PT,
                            CountryName = "Portugal"
                        };

                        ListPT.Add(countryNew);

                        ViewBag.CountryOriginID = new SelectList(ListPT, "CountryID", "CountryName");

                        var LIS = (from city in db.Cities where city.CityName == "Lisbon" select city.CityID).FirstOrDefault();

                        List<City> ListLIS = new List<City>();

                        var cityNew = new City
                        {
                            CityID = LIS,
                            CityName = "Lisbon"
                        };

                        ListLIS.Add(cityNew);

                        ViewBag.OriginID = new SelectList(ListLIS, "CityID", "CityName");

                        var DestCountry = (from country in db.Countries where country.CountryID == flight.CountryDestinationID select country).FirstOrDefault();

                        List<Country> ListCountryDest = new List<Country>();

                        var countryDestNew = new Country
                        {
                            CountryID = DestCountry.CountryID,
                            CountryName = DestCountry.CountryName
                        };

                        ListCountryDest.Add(countryDestNew);

                        ViewBag.CountryDestinationID = new SelectList(ListCountryDest, "CountryID", "CountryName");

                        var DesCity = (from city in db.Cities where city.CityID == flight.DestinationID select city).FirstOrDefault();

                        List<City> ListDestCity = new List<City>();

                        var cityDestNew = new City
                        {
                            CityID = DesCity.CityID,
                            CityName = DesCity.CityName
                        };

                        ListDestCity.Add(cityDestNew);

                        ViewBag.DestinationID = new SelectList(ListDestCity, "CityID", "CityName");


                        var PID = (from p in db.Airplanes where p.AirplaneID == flight.AirplaneID select p).FirstOrDefault();

                        List<Airplane> ListPID = new List<Airplane>();

                        var planeNew = new Airplane
                        {
                            AirplaneID = PID.AirplaneID,
                            Brand = PID.AirplaneBrandModel
                        };

                        ListPID.Add(planeNew);

                        ViewBag.AirplaneID = new SelectList(ListPID, "AirplaneID", "Brand");

                        ModelState.AddModelError("Departure Time", "The Date has to be greater than today");
                        return View(flight);
                    }

                }
                else
                {
                    var dayofYear = date.DayOfYear;
                    var Time = date.AddHours(timedistance);

                    if (Time.DayOfYear != dayofYear)
                    {
                        flight.ArrivalTime = date.AddHours(timedistance);
                    }
                    else
                    {
                        flight.ArrivalTime = date.AddHours(timedistance);
                    }
                }
            }
            else
            {
                if (flight.FlightID % 2 == 0)
                {
                    int PlaneId = flight.AirplaneID;

                    var PT = (from country in db.Countries where country.CountryName == "Portugal" select country.CountryID).FirstOrDefault();

                    List<Country> ListPT = new List<Country>();

                    var countryNew = new Country
                    {
                        CountryID = PT,
                        CountryName = "Portugal"
                    };

                    ListPT.Add(countryNew);

                    ViewBag.CountryDestinationID = new SelectList(ListPT, "CountryID", "CountryName");

                    var LIS = (from city in db.Cities where city.CityName == "Lisbon" select city.CityID).FirstOrDefault();

                    List<City> ListLIS = new List<City>();

                    var cityNew = new City
                    {
                        CityID = LIS,
                        CityName = "Lisbon"
                    };

                    ListLIS.Add(cityNew);

                    ViewBag.DestinationID = new SelectList(ListLIS, "CityID", "CityName");

                    var DestCountry = (from country in db.Countries where country.CountryID == flight.CountryOriginID select country).FirstOrDefault();

                    List<Country> ListCountryDest = new List<Country>();

                    var countryDestNew = new Country
                    {
                        CountryID = DestCountry.CountryID,
                        CountryName = DestCountry.CountryName
                    };

                    ListCountryDest.Add(countryDestNew);

                    ViewBag.CountryOriginID = new SelectList(ListCountryDest, "CountryID", "CountryName");

                    var DesCity = (from city in db.Cities where city.CityID == flight.OriginID select city).FirstOrDefault();

                    List<City> ListDestCity = new List<City>();

                    var cityDestNew = new City
                    {
                        CityID = DesCity.CityID,
                        CityName = DesCity.CityName
                    };

                    ListDestCity.Add(cityDestNew);

                    ViewBag.OriginID = new SelectList(ListDestCity, "CityID", "CityName");


                    var PID = (from p in db.Airplanes where p.AirplaneID == flight.AirplaneID select p).FirstOrDefault();

                    List<Airplane> ListPID = new List<Airplane>();

                    var planeNew = new Airplane
                    {
                        AirplaneID = PID.AirplaneID,
                        Brand = PID.AirplaneBrandModel
                    };

                    ListPID.Add(planeNew);

                    ViewBag.AirplaneID = new SelectList(ListPID, "AirplaneID", "Brand");

                    return View(flight);
                }
                else
                {
                    int PlaneId = flight.AirplaneID;

                    var PT = (from country in db.Countries where country.CountryName == "Portugal" select country.CountryID).FirstOrDefault();

                    List<Country> ListPT = new List<Country>();

                    var countryNew = new Country
                    {
                        CountryID = PT,
                        CountryName = "Portugal"
                    };

                    ListPT.Add(countryNew);

                    ViewBag.CountryOriginID = new SelectList(ListPT, "CountryID", "CountryName");

                    var LIS = (from city in db.Cities where city.CityName == "Lisbon" select city.CityID).FirstOrDefault();

                    List<City> ListLIS = new List<City>();

                    var cityNew = new City
                    {
                        CityID = LIS,
                        CityName = "Lisbon"
                    };

                    ListLIS.Add(cityNew);

                    ViewBag.OriginID = new SelectList(ListLIS, "CityID", "CityName");

                    var DestCountry = (from country in db.Countries where country.CountryID == flight.CountryDestinationID select country).FirstOrDefault();

                    List<Country> ListCountryDest = new List<Country>();

                    var countryDestNew = new Country
                    {
                        CountryID = DestCountry.CountryID,
                        CountryName = DestCountry.CountryName
                    };

                    ListCountryDest.Add(countryDestNew);

                    ViewBag.CountryDestinationID = new SelectList(ListCountryDest, "CountryID", "CountryName");

                    var DesCity = (from city in db.Cities where city.CityID == flight.DestinationID select city).FirstOrDefault();

                    List<City> ListDestCity = new List<City>();

                    var cityDestNew = new City
                    {
                        CityID = DesCity.CityID,
                        CityName = DesCity.CityName
                    };

                    ListDestCity.Add(cityDestNew);

                    ViewBag.DestinationID = new SelectList(ListDestCity, "CityID", "CityName");


                    var PID = (from p in db.Airplanes where p.AirplaneID == flight.AirplaneID select p).FirstOrDefault();

                    List<Airplane> ListPID = new List<Airplane>();

                    var planeNew = new Airplane
                    {
                        AirplaneID = PID.AirplaneID,
                        Brand = PID.AirplaneBrandModel
                    };

                    ListPID.Add(planeNew);

                    ViewBag.AirplaneID = new SelectList(ListPID, "AirplaneID", "Brand");

                }

                ModelState.AddModelError("DepartureTime", "You have to choose a valid Date");
                return View(flight);
            }

            if (ModelState.IsValid)
            {
                db.Flights.Attach(flight);
                flight.DepartureTime = DateTime.Parse(dateCheck);
                flight.ArrivalTime = flight.ArrivalTime;
                int count = (from t in db.Tickets where t.FlightID == flight.FlightID select t).Count();
                flight.FlightAvailableSeats = flight.FlightTotalSeats - count;
                db.Entry(flight).State = EntityState.Modified;
                db.SaveChanges();

                if (flight.FlightID % 2 == 0)
                {
                    Flight otherFlight = db.Flights.Find(flight.FlightID - 1);
                    db.Flights.Attach(otherFlight);
                    DateTime backD = flight.DepartureTime - TimeSpan.FromDays(3);
                    otherFlight.DepartureTime = backD;
                    DateTime backA = flight.ArrivalTime - TimeSpan.FromDays(3);
                    otherFlight.ArrivalTime = backA;
                    int countOtherEven = (from t in db.Tickets where t.FlightID == otherFlight.FlightID select t).Count();
                    otherFlight.FlightAvailableSeats = otherFlight.FlightTotalSeats - countOtherEven;
                    db.Entry(otherFlight).State = EntityState.Modified;
                    db.SaveChanges();
                }
                else
                {
                    Flight otherFlight = db.Flights.Find(flight.FlightID + 1);
                    db.Flights.Attach(otherFlight);
                    otherFlight.DepartureTime = flight.DepartureTime.AddDays(3);
                    otherFlight.ArrivalTime = flight.ArrivalTime.AddDays(3);
                    int countOtherOdd = (from t in db.Tickets where t.FlightID == otherFlight.FlightID select t).Count();
                    otherFlight.FlightAvailableSeats = otherFlight.FlightTotalSeats - countOtherOdd;
                    db.Entry(otherFlight).State = EntityState.Modified;
                    db.SaveChanges();
                }
                return RedirectToAction("IndexEmployee");
            }
            if (flight.FlightID % 2 == 0)
            {
                int PlaneId = flight.AirplaneID;

                var PT = (from country in db.Countries where country.CountryName == "Portugal" select country.CountryID).FirstOrDefault();

                List<Country> ListPT = new List<Country>();

                var countryNew = new Country
                {
                    CountryID = PT,
                    CountryName = "Portugal"
                };

                ListPT.Add(countryNew);

                ViewBag.CountryDestinationID = new SelectList(ListPT, "CountryID", "CountryName");

                var LIS = (from city in db.Cities where city.CityName == "Lisbon" select city.CityID).FirstOrDefault();

                List<City> ListLIS = new List<City>();

                var cityNew = new City
                {
                    CityID = LIS,
                    CityName = "Lisbon"
                };

                ListLIS.Add(cityNew);

                ViewBag.DestinationID = new SelectList(ListLIS, "CityID", "CityName");

                var DestCountry = (from country in db.Countries where country.CountryID == flight.CountryOriginID select country).FirstOrDefault();

                List<Country> ListCountryDest = new List<Country>();

                var countryDestNew = new Country
                {
                    CountryID = DestCountry.CountryID,
                    CountryName = DestCountry.CountryName
                };

                ListCountryDest.Add(countryDestNew);

                ViewBag.CountryOriginID = new SelectList(ListCountryDest, "CountryID", "CountryName");

                var DesCity = (from city in db.Cities where city.CityID == flight.OriginID select city).FirstOrDefault();

                List<City> ListDestCity = new List<City>();

                var cityDestNew = new City
                {
                    CityID = DesCity.CityID,
                    CityName = DesCity.CityName
                };

                ListDestCity.Add(cityDestNew);

                ViewBag.OriginID = new SelectList(ListDestCity, "CityID", "CityName");


                var PID = (from p in db.Airplanes where p.AirplaneID == flight.AirplaneID select p).FirstOrDefault();

                List<Airplane> ListPID = new List<Airplane>();

                var planeNew = new Airplane
                {
                    AirplaneID = PID.AirplaneID,
                    Brand = PID.AirplaneBrandModel
                };

                ListPID.Add(planeNew);

                ViewBag.AirplaneID = new SelectList(ListPID, "AirplaneID", "Brand");

                return View(flight);
            }
            else
            {
                int PlaneId = flight.AirplaneID;

                var PT = (from country in db.Countries where country.CountryName == "Portugal" select country.CountryID).FirstOrDefault();

                List<Country> ListPT = new List<Country>();

                var countryNew = new Country
                {
                    CountryID = PT,
                    CountryName = "Portugal"
                };

                ListPT.Add(countryNew);

                ViewBag.CountryOriginID = new SelectList(ListPT, "CountryID", "CountryName");

                var LIS = (from city in db.Cities where city.CityName == "Lisbon" select city.CityID).FirstOrDefault();

                List<City> ListLIS = new List<City>();

                var cityNew = new City
                {
                    CityID = LIS,
                    CityName = "Lisbon"
                };

                ListLIS.Add(cityNew);

                ViewBag.OriginID = new SelectList(ListLIS, "CityID", "CityName");

                var DestCountry = (from country in db.Countries where country.CountryID == flight.CountryDestinationID select country).FirstOrDefault();

                List<Country> ListCountryDest = new List<Country>();

                var countryDestNew = new Country
                {
                    CountryID = DestCountry.CountryID,
                    CountryName = DestCountry.CountryName
                };

                ListCountryDest.Add(countryDestNew);

                ViewBag.CountryDestinationID = new SelectList(ListCountryDest, "CountryID", "CountryName");

                var DesCity = (from city in db.Cities where city.CityID == flight.DestinationID select city).FirstOrDefault();

                List<City> ListDestCity = new List<City>();

                var cityDestNew = new City
                {
                    CityID = DesCity.CityID,
                    CityName = DesCity.CityName
                };

                ListDestCity.Add(cityDestNew);

                ViewBag.DestinationID = new SelectList(ListDestCity, "CityID", "CityName");


                var PID = (from p in db.Airplanes where p.AirplaneID == flight.AirplaneID select p).FirstOrDefault();

                List<Airplane> ListPID = new List<Airplane>();

                var planeNew = new Airplane
                {
                    AirplaneID = PID.AirplaneID,
                    Brand = PID.AirplaneBrandModel
                };

                ListPID.Add(planeNew);

                ViewBag.AirplaneID = new SelectList(ListPID, "AirplaneID", "Brand");

                return View(flight);
            }
        }

        [Authorize(Roles = "Delete")]
        // GET: Flights/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("BadRequest", "Requests");
            }
            Flight flight = db.Flights.Find(id);
            if (flight == null)
            {
                return RedirectToAction("PageNotFound", "Requests");
            }
            return View(flight);
        }

        // POST: Flights/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Flight otherFlight;
            if (id % 2 == 0)
            {
                otherFlight = db.Flights.Find(id - 1);
            }
            else
            {
                otherFlight = db.Flights.Find(id + 1);
            }

            Flight flight = db.Flights.Find(id);
            db.Flights.Remove(flight);
            db.Flights.Remove(otherFlight);
            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                return RedirectToAction("NotPossible", "Requests");
            }
            
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
