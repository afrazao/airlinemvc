﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace airlinemvc.ViewModels
{
    public class RoleView
    {
        public string RoleID { get; set; }
        
        public string NameRole { get; set; }
    }
}