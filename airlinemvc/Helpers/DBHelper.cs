﻿using airlinemvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace airlinemvc.Helpers
{
    public class DBHelper : IDisposable
    {
        private static AirlineMVCContext db = new AirlineMVCContext();
        private static ApplicationDbContext dbApp = new ApplicationDbContext();

        public static List<City> GetCities()
        {
            List<City> ListCities = new List<City>();

            var CitiesList = db.Cities.ToList();

            foreach (var city in CitiesList)
            {
                if ((GetCountries(city) != "Antarctica") || (GetCountries(city) != "Bonaire, Saint Eustatius and Saba") || (GetCountries(city) != "Heard Island and McDonald Islands")
                || (GetCountries(city) != "Tokelau") || (GetCountries(city) != "United States Minor Outlying Islands") || (GetCountries(city) != "Andorra")
                 || (GetCountries(city) != "Anguilla") || (GetCountries(city) != "Aruba") || (GetCountries(city) != "Benin") || (GetCountries(city) != "Bouvet Island")
                  || (GetCountries(city) != "Cocos Islands") || (GetCountries(city) != "Cook Islands") || (GetCountries(city) != "Curaçao") || (GetCountries(city) != "Christmas Island") || (GetCountries(city) != "Western Sahara")
                   || (GetCountries(city) != "Falkland Islands") || (GetCountries(city) != "Micronesia") || (GetCountries(city) != "Faroe Islands") || (GetCountries(city) != "Grenada") || (GetCountries(city) != "Guernsey")
                    || (GetCountries(city) != "Guadeloupe") || (GetCountries(city) != "South Georgia and the South Sandwich Islands") || (GetCountries(city) != "Haiti") || (GetCountries(city) != "Isle of Man") || (GetCountries(city) != "British Indian Ocean Territory")
                     || (GetCountries(city) != "Jersey") || (GetCountries(city) != "Liechtenstein") || (GetCountries(city) != "Libya") || (GetCountries(city) != "Monaco")
                      || (GetCountries(city) != "Saint Martin") || (GetCountries(city) != "Mongolia") || (GetCountries(city) != "Macao") || (GetCountries(city) != "Martinique") || (GetCountries(city) != "Malta")
                       || (GetCountries(city) != "Mauritius") || (GetCountries(city) != "Nauru") || (GetCountries(city) != "Niue") || (GetCountries(city) != "Saint Pierre and Miquelon") || (GetCountries(city) != "Pitcairn")
                        || (GetCountries(city) != "Palestinian Territory") || (GetCountries(city) != "Palau") || (GetCountries(city) != "Reunion") || (GetCountries(city) != "Singapore") || (GetCountries(city) != "San Marino")
                         || (GetCountries(city) != "Sao Tome and Principe") || (GetCountries(city) != "Swaziland") || (GetCountries(city) != "Turks and Caicos Islands") || (GetCountries(city) != "French Southern Territories")
                          || (GetCountries(city) != "Trinidad and Tobago") || (GetCountries(city) != "Tuvalu") || (GetCountries(city) != "Uganda") || (GetCountries(city) != "Vatican") || (GetCountries(city) != "Saint Vincent and the Grenadines") || (GetCountries(city) != "British Virgin Islands")
                           || (GetCountries(city) != "Wallis and Futuna") || (GetCountries(city) != "Mayotte"))
                {
                    ListCities.Add(city);
                }
            }

            return ListCities;
        }

        public static string GetCountries(City city)
        {
            string getName = (from c in db.Countries where c.CountryID == city.CountryID select c.CountryName).FirstOrDefault();

            return getName;
        }

        //        DocumentTypes.Add(new DocumentType
        //    {
        //        DocumentTypeID = 0,
        //        Description = "{Seleccione um tipo de documento...}"
        //    });

        //    return DocumentTypes.OrderBy(d => d.Description).ToList();
        //}

        //public static List<Customer> GetCustomersNames()
        //{
        //    var Customers = db.Customers.ToList();
        //    Customers.Add(new Customer
        //    {
        //        CustomerID = 0,
        //        FirstName = "{Seleccione um cliente}"
        //    });

        //    return Customers.OrderBy(c => c.Name).ToList();
        //}

        //public static List<Product> GetProducts()
        //{
        //    var Products = db.Products.ToList();
        //    Products.Add(new Product
        //    {
        //        ProductID = 0,
        //        Description = "{Seleccione um Produto...}"
        //    });

        //    return Products.OrderBy(p => p.Description).ToList();
        //}

        //public static List<IdentityRole> GetRoles()
        //{
        //    var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(da));
        //    var list = roleManager.Roles.ToList();
        //    list.Add(new IdentityRole { Id = "", Name = "{Seleccione uma Permissão...}" });

        //    return list.OrderBy(r => r.Name).ToList();
        //}

        public void Dispose()
        {
            db.Dispose();
        }
    }
}