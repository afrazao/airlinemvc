﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Web;

namespace airlinemvc.Helpers
{
    public static class DistanceHelper
    {

        public static double LatA { get; set; }

        public static double LatB { get; set; }

        public static double LongA { get; set; }

        public static double LongB { get; set; }

        public static double calcDistance(double latA, double longA, double latB, double longB)
        {
            LatA = latA;
            LatB = latB;
            LongA = longA;
            LongB = longB;

            var locA = new GeoCoordinate(LatA, LongA);
            var locB = new GeoCoordinate(LatB, LongB);
            double distance = locA.GetDistanceTo(locB); // meters

            return distance / 1000; //kms
        }
    }
}

//public static double GetArrival(string lato, string longo, string latd, string longd)
//{
//    if (lato != null && longo != null && latd != null && longd != null)
//    {

//        var latg = lato.Replace('.', ',');
//        var longg = longo.Replace('.', ',');
//        var lata = latd.Replace('.', ',');
//        var longa = longd.Replace('.', ',');
//        Double.TryParse(latg, out double latitudeG);
//        Double.TryParse(longg, out double longitudeG);
//        Double.TryParse(lata, out double latitudeA);
//        Double.TryParse(longa, out double longitudeA);

//        var sCoord = new GeoCoordinate(latitudeG, longitudeG);
//        var eCoord = new GeoCoordinate(latitudeA, longitudeA);

//        return sCoord.GetDistanceTo(eCoord);


//        //var distance = DistancesHelper.calcDistancia(latitudeA, longitudeA, latitudeB, longitudeB);
//        //    _distanceFromPointOfDeparture = Math.Round(distance, 2);
//        //    return _distanceFromPointOfDeparture;
//        //    //}

//    }

//    }

//    else
//    {
//        _distanceFromPointOfDeparture = 0;
//    }


//    return _distanceFromPointOfDeparture;



//}

//    //var latitude = double.Parse(lato);

//    //var sCoord = new GeoCoordinate(sLatitude, sLongitude);
//    //var eCoord = new GeoCoordinate(eLatitude, eLongitude);

//    //return sCoord.GetDistanceTo(eCoord);

//        //var latitudeOrigin = Origin.latitudeCity;
//        //var longitudeOrigin = Origin.longitudeCity;
//        //var latitudeDestination = Destination.latitudeCity;
//        //var longitudeDestination = Destination.longitudeCity;
//        //if (latitudeOrigin != null && longitudeOrigin != null && latitudeOrigin != null && latitudeDestination != null)
//        //{
//        //    //if (Origin.CityName == "Lisbon" || Destination.CityName == "Lisbon")
//        //    //{
//        //    //    return 0;
//        //    //}
//        //    //else
//        //    //{
